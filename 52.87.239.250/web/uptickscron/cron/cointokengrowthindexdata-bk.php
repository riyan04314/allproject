<?php
	include('../config/dbconnect.php');

	$x 	 = "";
	$sql = "select name from `growth_indexes`";
	$res = mysqli_query($con,$sql);
	foreach($res as $rowItem) 
	{
		$x .= '\''.$rowItem['name'].'\',';
	}
	$x = substr($x,0,-1);
	
	$sql_rank = '';
	if(date('h')<0){
		$sql_rank = ' and rank > 50 ';		
	}
	$sql1 = "SELECT ct_id, symbol, price_usd FROM coins_tokens_data where updated_time > '".date('Y-m-d',strtotime("-1 days"))."' ".$sql_rank." and name not in (".$x.")  order by rand() limit 1
	";
	//$sql1 = "SELECT ct_id, symbol, price_usd FROM coins_tokens_data order by rand() limit 1";

	if(!empty($_REQUEST['ct_id'])){
		$sql1 = "SELECT ct_id, symbol, price_usd FROM coins_tokens_data where updated_time > '".date('Y-m-d',strtotime('-10 day'))."' 
		and symbol in (  '".$_REQUEST['ct_id']."' ) 
		order by rand() limit 1";
	}	
	//echo $sql1;
	$run2 = mysqli_query($con,$sql1);
	
	//echo $sql1;
	
	foreach($run2 as $rowItem) 
	{
		$name 			= $rowItem['ct_id'];
		$ct_name_symbol	= $rowItem['ct_id'].'_'.$rowItem['symbol'];
		$current_price 	= $rowItem['price_usd'];
		echo '<br> Coin or Token Name : '.$name.'  :: ';//exit;
		/*** Start : Volatile Ratio ***/
		$volatile_today_high 		= 0;
		$volatile_today_low 		= 0;
		$volatile_yesterday_close	= 0;
		$sql = "select high,low from `historical_data` where name = '".$name."' and date like '".date('Y-m-d')."%'";
		//echo $sql.'<br><br>';exit;
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val){
			$volatile_today_high 	= $val['high'];
			$volatile_today_low 	= $val['low'];
		}
		
		$sql = "select close from `historical_data` where name = '".$name."' and date like '".date('Y-m-d',strtotime('-15 days'))."%'";
		//echo $sql.'<br><br>';
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val){
			$volatile_yesterday_close	= $val['close'];
		}
		
		$volatile_today_true_range = max($volatile_today_high,$volatile_yesterday_close)-min($volatile_today_low,$volatile_yesterday_close);
		//echo '<br>::: '.$volatile_today_true_range.' = max('.$volatile_today_high.','.$volatile_yesterday_close.')-min('.$volatile_today_low.','.$volatile_yesterday_close.');';
		
		$volatile_10days_high	= 0;
		$volatile_10days_low	= 0;
		$sql = "select max(high) as high, min(low) as low from `historical_data` where name = '".$name."' and date > '".date('Y-m-d',strtotime('-10 days'))."'";
		//echo $sql.'<br><br>';
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val){
			$volatile_10days_high	= $val['high'];
			$volatile_10days_low	= $val['low'];
		}
		$volatile_10days_true_range = $volatile_10days_high-$volatile_10days_low;
		if($volatile_10days_true_range>0){
			$volatility_ratio_10_days	= $volatile_today_true_range/$volatile_10days_true_range;
		}
		
		echo ' :: '.$volatile_10days_true_range.' = '.$volatile_10days_high.' -- '.$volatile_10days_low;
		//echo '<br>***'.$volatile_ratio.'	= '.$volatile_today_true_range.'/'.$volatile_10days_true_range;
		//echo $volatile_ratio;exit;
		//sleep(5);
		//echo 'reload';
		//header('location:http://coinmarketindex.org/cron_job_for_growth_index.php?ct_id=bitcoin');
		/*** End : Volatile Ratio ***/
		
		
		/***Start : Monthly Price & Volume index (current date - 30dates) ***/
		$thirtydays_avg_price = 0;
		$thirtydays_avg_volume = 0;			
		$sql 	= "select avg(close) avgprice,  avg(volum) avgvolume from `historical_data` where name = '".$name."' and date>'".date('Y-m-d',strtotime('-1 month'))."'";
		//$sql 	= "select avg(close) avgprice from `historical_data` where name = '".$name."' and date>'".date('Y-m-d')."'";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$thirtydays_avg_price		= $val['avgprice'];
			$thirtydays_avg_volume		= $val['avgvolume'];
		}
		/** End : Monthly Price  & Volume Index**/
		
		/***Start : Min-Max 7 days, 1 month ***/
		$ath_price = 0;
		$atl_price = 0;
		
		$sql 	= "select date,high from `historical_data` where name = '".$name."' and high = ( select max(high)  from `historical_data` where name = '".$name."' )";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$ath_price		= $val['high'];
			$ath_price_date	= $val['date'];
		}
		
		$sql 	= "select date,low from `historical_data` where name = '".$name."' and low = ( select min(low)  from `historical_data` where name = '".$name."' )";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$atl_price		= $val['low'];
			$atl_price_date	= $val['date'];
		}
		
		$minprice_7days		= 0;
		$maxprice_7days		= 0;
		$sql 	= "select min(low) minprice, max(high) maxprice from `historical_data` where date >= '".date('Y-m-d',strtotime('-7 days'))."' and name = '".$name."' order by date desc";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$minprice_7days		= $val['minprice'];
			$maxprice_7days		= $val['maxprice'];
		}
		
		$minprice_1months		= 0;
		$maxprice_1months		= 0;
		$sql 	= "select min(low) minprice, max(high) maxprice from `historical_data` where date >= '".date('Y-m-d',strtotime('-1 months'))."' and name = '".$name."' order by date desc";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$minprice_1months		= $val['minprice'];
			$maxprice_1months		= $val['maxprice'];
		}
		
		/***End : Min-Max 7 days, 1 month  ***/
		
		/***Start : YTD***/
		$ytd_price			= 0;
		$ytd_price_index	= 0;
		$sql_ytd 	= "SELECT name,open,date FROM historical_data where name = '".$name."' and date >= '".date('Y-01-01')."' order by date asc limit 1 ";
		$run_ytd 	= mysqli_query($con,$sql_ytd);
		foreach($run_ytd as $row_ytd) 
		{
			$ytd_price	= $row_ytd['open'];
		}
		if($ytd_price>0){				
			$ytd_price_index	= (($current_price-$ytd_price)/$ytd_price)*100;
			//echo ':::::: '.$ytd_price_index.' = (('.$current_price.'-'.$ytd_price.')/'.$ytd_price.')*.100';
		}
		//echo 'CP  '.$current_price.'ytd p ... '.$ytd_price.';;;;'.$ytd_price_index.'++++';
		/***End : YTD***/
		
		/***Start : ATD ***/
		$atd_price			= 0;
		$atd_price_index	= 0;
		$sql_atd 	= "SELECT name,open,date FROM historical_data where name = '".$name."'  order by date asc limit 1 ";
		$run_atd 	= mysqli_query($con,$sql_atd);
		foreach($run_atd as $row_atd) 
		{
			$atd_price	= $row_atd['open'];
		}
		if($atd_price>0){
			$atd_price_index	= (($current_price-$atd_price)/$atd_price)*100;
		}
		//echo 'CP  '.$current_price.'atd p ... '.$atd_price.';;;;'.$atd_price_index.'++++';
		/***End : ATD***/
		
		/** Start : Other Growth Index Calculation **/
		$gi_week_1 	= 0;
		$gi_week_2 	= 0;
		$gi_week_3 	= 0;
		$gi_week_4 	= 0;
		$gi_week_5 	= 0;
		$gi_week_6 	= 0;
		$gi_week_7 	= 0;
		$gi_week_8 	= 0;
		$gi_week_9 	= 0;
		$gi_week_10 	= 0;
		$gi_week_11 	= 0;
		$gi_week_12 	= 0;		
		
		$dtd_str  = '';
		$i = 0;
		$j = 0;
		while($i<12){
			$i++;
			$j = 7*$i;
			$dtd_str .= '\''.date('Y-m-d',strtotime('-'.$j.' days')).'\',';
		}
		$dtd_str 			= substr($dtd_str,0,-1);
		$i = 0;
		$sql_price_index 	= "select name,open,date FROM historical_data where name = '".$name."' and date in (".$dtd_str.") order by date desc limit 15 ";
		$run_price_index	= mysqli_query($con,$sql_price_index);
		foreach($run_price_index as $row_price_index) 
		{
			$i++;
			$price			= $row_price_index['open'];
			$price_index	= 0;
			if($price>0){
				$price_index	= (($current_price-$price)/$price)*100;
			}
			//$i = 99;
			switch($i){
				case 1:$gi_week_1 = $price_index;break;
				case 2:$gi_week_2 = $price_index;break;
				case 3:$gi_week_3 = $price_index;break;
				case 4:$gi_week_4 = $price_index;break;
				case 5:$gi_week_5 = $price_index;break;
				case 6:$gi_week_6 = $price_index;break;
				case 7:$gi_week_7 = $price_index;break;
				case 8:$gi_week_8 = $price_index;break;
				case 9:$gi_week_9 = $price_index;break;
				case 10:$gi_week_10 = $price_index;break;
				case 11:$gi_week_11 = $price_index;break;
				case 12:$gi_week_12 = $price_index;break;
			}
		}

		
		$gi_month_1 	= 0;
		$gi_month_2 	= 0;
		$gi_month_3 	= 0;
		$gi_month_4 	= 0;
		$gi_month_5 	= 0;
		$gi_month_6 	= 0;
		$gi_month_7 	= 0;
		$gi_month_8 	= 0;
		$gi_month_9 	= 0;
		$gi_month_10 	= 0;
		$gi_month_11 	= 0;
		$gi_month_12 	= 0;			
		
		
		$dtd_str  = '';
		$i = 0;
		$j = 0;
		while($i<12){
			$i++;
			$dtd_str .= '\''.date('Y-m-d',strtotime('-'.$i.' month')).'\',';
		}
		$dtd_str 			= substr($dtd_str,0,-1);
		$i = 0;
		$sql_price_index 	= "select name,open,date FROM historical_data where name = '".$name."' and date in (".$dtd_str.") order by date desc limit 15 ";		
		$run_price_index	= mysqli_query($con,$sql_price_index);
		foreach($run_price_index as $row_price_index) 
		{
			$i++;
			$price			= $row_price_index['open'];
			$price_index	= 0;
			if($price>0){
				$price_index	= (($current_price-$price)/$price)*100;
			}
			//$i = 99;
			switch($i){
				case 1:$gi_month_1 = $price_index;break;
				case 2:$gi_month_2 = $price_index;break;
				case 3:$gi_month_3 = $price_index;break;
				case 4:$gi_month_4 = $price_index;break;
				case 5:$gi_month_5 = $price_index;break;
				case 6:$gi_month_6 = $price_index;break;
				case 7:$gi_month_7 = $price_index;break;
				case 8:$gi_month_8 = $price_index;break;
				case 9:$gi_month_9 = $price_index;break;
				case 10:$gi_month_10 = $price_index;break;
				case 11:$gi_month_11 = $price_index;break;
				case 12:$gi_month_12 = $price_index;break;
			}
		}			
		
		$ins_upd = '';
		/** End : Other Growth Index Calculation **/
		$gi_uarter_1 	= 0;//$this->all_time_growth_index($name,$current_price, 90);
		$gi_uarter_2 	= 0;//$this->all_time_growth_index($name,$current_price, 180);
		$gi_uarter_3 	= 0;//$this->all_time_growth_index($name,$current_price, 270);
		$gi_uarter_4 	= 0;//$this->all_time_growth_index($name,$current_price, 365);
		
		$sql 	= "SELECT name FROM growth_indexes WHERE name = '".$name."'";
		$query 	= mysqli_query($con,$sql);
		if(mysqli_num_rows($query)>0)
		{
			$ins_upd = 'Inserted : ';
			$sql = "UPDATE growth_indexes SET 
							volatility_ratio_10_days	= '".$volatility_ratio_10_days."',
							atd					= '".$atd_price_index."',
							ytd					= '".$ytd_price_index."',
							ath_price			= '".$ath_price."',
							atl_price			= '".$atl_price."',
							ath_price_date		= '".$ath_price_date."',
							atl_price_date		= '".$atl_price_date."',
							30days_avg_price	= '".$thirtydays_avg_price."',
							30days_avg_volume	= '".$thirtydays_avg_volume."',
							minprice_7days		= '".$minprice_7days."',			
							maxprice_7days		= '".$maxprice_7days."',			
							minprice_1months	= '".$minprice_1months."',			
							maxprice_1months	= '".$maxprice_1months."',								
							gi_week_1	= '".$gi_week_1."',
							gi_week_2	= '".$gi_week_2."',
							gi_week_3	= '".$gi_week_3."',
							gi_week_4	= '".$gi_week_4."',
							gi_week_5	= '".$gi_week_5."',
							gi_week_6	= '".$gi_week_6."',
							gi_week_7	= '".$gi_week_7."',
							gi_week_8	= '".$gi_week_8."',
							gi_week_9	= '".$gi_week_9."',
							gi_week_10	= '".$gi_week_10."',
							gi_week_11	= '".$gi_week_11."',
							gi_week_12	= '".$gi_week_12."',
							gi_month_1	= '".$gi_month_1."',
							gi_month_2	= '".$gi_month_2."',
							gi_month_3	= '".$gi_month_3."',
							gi_month_4	= '".$gi_month_4."',
							gi_month_5	= '".$gi_month_5."',
							gi_month_6	= '".$gi_month_6."',
							gi_month_7	= '".$gi_month_7."',
							gi_month_8	= '".$gi_month_8."',
							gi_month_9	= '".$gi_month_9."',
							gi_month_10	= '".$gi_month_10."',
							gi_month_11	= '".$gi_month_11."',
							gi_month_12	= '".$gi_month_12."',
							gi_uarter_1	= '".$gi_uarter_1."',
							gi_uarter_2	= '".$gi_uarter_2."',
							gi_uarter_3	= '".$gi_uarter_3."',
							gi_uarter_4	= '".$gi_uarter_4."'
							WHERE name		= '".$name."'";			
			mysqli_query($con,$sql);
		}
		else
		{
			$ins_upd = 'Updated : ';
			$sql = "INSERT INTO growth_indexes SET 
						name						= '".$name."',
						ct_name_symbol				= '".$ct_name_symbol."',
						volatility_ratio_10_days	= '".$volatility_ratio_10_days."',
						atd							= '".$atd_price_index."',
						ytd							= '".$ytd_price_index."',
						ath_price					= '".$ath_price."',
						atl_price					= '".$atl_price."',
						ath_price_date				= '".$ath_price_date."',
						atl_price_date				= '".$atl_price_date."',
						30days_avg_price			= '".$thirtydays_avg_price."',
						30days_avg_volume			= '".$thirtydays_avg_volume."',
						minprice_7days				= '".$minprice_7days."',			
						maxprice_7days				= '".$maxprice_7days."',			
						minprice_1months			= '".$minprice_1months."',			
						maxprice_1months			= '".$maxprice_1months."',					
						gi_week_1					= '".$gi_week_1."',
						gi_week_2					= '".$gi_week_2."',
						gi_week_3					= '".$gi_week_3."',
						gi_week_4					= '".$gi_week_4."',
						gi_week_5					= '".$gi_week_5."',
						gi_week_6					= '".$gi_week_6."',
						gi_week_7					= '".$gi_week_7."',
						gi_week_8					= '".$gi_week_8."',
						gi_week_9					= '".$gi_week_9."',
						gi_week_10					= '".$gi_week_10."',
						gi_week_11					= '".$gi_week_11."',
						gi_week_12					= '".$gi_week_12."',
						gi_month_1	= '".$gi_month_1."',
						gi_month_2	= '".$gi_month_2."',
						gi_month_3	= '".$gi_month_3."',
						gi_month_4	= '".$gi_month_4."',
						gi_month_5	= '".$gi_month_5."',
						gi_month_6	= '".$gi_month_6."',
						gi_month_7	= '".$gi_month_7."',
						gi_month_8	= '".$gi_month_8."',
						gi_month_9	= '".$gi_month_9."',
						gi_month_10	= '".$gi_month_10."',
						gi_month_11	= '".$gi_month_11."',
						gi_month_12	= '".$gi_month_12."',
						gi_uarter_1	= '".$gi_uarter_1."',
						gi_uarter_2	= '".$gi_uarter_2."',
						gi_uarter_3	= '".$gi_uarter_3."',
						gi_uarter_4	= '".$gi_uarter_4."'";			
			mysqli_query($con,$sql);
		}
		
		echo '   '.$ins_upd.' '.$ct_name_symbol.'
		
';
echo '<br><br><br>'.$sql.'<br>

';
	}
?>