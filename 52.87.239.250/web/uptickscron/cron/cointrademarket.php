<?php
	include('../config/dbconnect.php');
	
	$coinname = '';
	$sql = "select ct_id from `coins_tokens_data` where updated_time > '".date('Y-m-d')."' limit 1";
	$res = mysqli_query($con,$sql);
	while($val = mysqli_fetch_array($res)){
		$coinname = $val['ct_id'];
	}

	if($coinname != ''){
		$marketdata = array();
		$fileurl	= "https://coinmarketcap.com/currencies/".$coinname."/#markets";
		$json_string 	= file_get_contents($fileurl);
		@$dom = new DOMDocument;
		@$dom->loadHTML( $json_string );
		foreach( $dom->getElementsByTagName( 'tr' ) as $tr ) 
		{
			$cells = array();
			foreach( $tr->getElementsByTagName( 'td' ) as $td ) {
				$cells[] = $td->nodeValue;
			}
			$marketdata[] = $cells;
		}
		$sl_no = 0;
		$slno = 0;
		$tblstr = '<table id="dataTbl4" class="mobileFormat display print_table" cellspacing="0" width="100%" ><thead>
		<tr><th>#</th><th>Source</th><th>Pair</th><th style="text-align:right;">Volume(24h)</th>
		<th style="text-align:right;">Price</th>
		<th style="text-align:right;">Volume(%)</th></tr></thead><tbody>';
		foreach($marketdata as $val){
			$sl_no++;
			if($sl_no == 1){
				continue;
			}
			$slno++;
			$tblstr .= '<tr><td>'.$slno.'</td><td>'.$val[1].'</td><td>'.$val[2].'</td><td style="text-align:right;">'.$val[3].'</td><td style="text-align:right;">'.$val[4].'</td><td style="text-align:right;">'.$val[5].'</td></tr>';
		}
		$tblstr .= '</tbody></table>';
		echo $tblstr;
		$fileurl = '../apidata/cointrademarket/cointrademarket_ethereum_'.date('d_m_Y').'.html';
		if($sl_no>2){
			file_put_contents($fileurl,$tblstr);
		}
	}
?>