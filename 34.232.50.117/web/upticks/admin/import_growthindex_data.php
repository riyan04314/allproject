<?php	
	include('../src/constant.php');
	$hostname	= "localhost";
	$dbuser 	= "root";
	$dbpass 	= "P@55word@123";
	$dbname 	= "coinmarketindexorg";
	
	//$con = mysqli_connect($hostname,$dbuser,$dbpass);
	$con = mysqli_connect(DBHOST,DBUSER,DBPASSWORD);
	if($con){
		//echo 'DB Connected<br>';
		if(mysqli_select_db($con,DBNAME)){
			//echo 'DB selected<br>';
		}else{
			echo 'DB not selected<br>';exit;
		}
	}else{
		echo 'DB Not Connected<br>';exit;
	}

	
	$tbl = 'growth_indexes';

	
	$values = '';
	$url 		= SERVER02_URL."apidata/cointokengrowthindexexportdata.json";
	echo $url.'<br><br>';
	$content	= file_get_contents($url);
	$content	= str_replace('30days_avg_price','thirtydays_avg_price',$content);
	$content	= str_replace('30days_avg_volume','thirtydays_avg_volume',$content);
	$content	= json_decode($content);
	$cnt = 0;

	if(count($content)<20){echo '+++Error'.count($content);exit;}
	$sql = "TRUNCATE TABLE ".$tbl;
	mysqli_query($con,$sql);
	
	foreach($content as $val)
	{
		$cnt++;		
		$values .= "(
			'".$val->ct_name_symbol."',
			'".$val->name."', 
			'".$val->status."', 
			'".$val->updated_time."', 
			'".$val->market_cap_usd."', 
			'".$val->volatility_ratio_10_days."', 
			'".$val->atd."', 
			'".$val->ytd."', 
			'".$val->ath_price."', 
			'".$val->atl_price."', 
			'".$val->ath_price_date."', 
			'".$val->atl_price_date."',
			'".$val->minprice_24hr."', 
			'".$val->maxprice_24hr."', 
			'".$val->minprice_7days."', 
			'".$val->maxprice_7days."', 
			'".$val->minprice_1months."', 
			'".$val->maxprice_1months."', 			
			'".$val->minprice_3months."', 
			'".$val->maxprice_3months."', 
			'".$val->minprice_ytd."', 
			'".$val->maxprice_ytd."', 			
			'".$val->sevenDays_avg_price."', 
			'".$val->sevenDays_avg_volume."', 
			'".$val->thirtydays_avg_price."', 
			'".$val->thirtydays_avg_volume."', 
			'".$val->gi_week_1."', 
			'".$val->gi_week_2."', 
			'".$val->gi_week_3."', 
			'".$val->gi_week_4."', 
			'".$val->gi_week_5."', 
			'".$val->gi_week_6."', 
			'".$val->gi_week_7."', 
			'".$val->gi_week_8."', 
			'".$val->gi_week_9."', 
			'".$val->gi_week_10."', 
			'".$val->gi_week_11."', 
			'".$val->gi_week_12."', 
			'".$val->gi_month_1."', 
			'".$val->gi_month_2."', 
			'".$val->gi_month_3."', 
			'".$val->gi_month_4."', 
			'".$val->gi_month_5."', 
			'".$val->gi_month_6."', 
			'".$val->gi_month_7."', 
			'".$val->gi_month_8."', 
			'".$val->gi_month_9."', 
			'".$val->gi_month_10."', 
			'".$val->gi_month_11."', 
			'".$val->gi_month_12."', 
			'".$val->gi_uarter_1."', 
			'".$val->gi_uarter_2."', 
			'".$val->gi_uarter_3."',
			'".$val->gi_uarter_4."',
			'".$val->listingdate."',
			'".$val->listingprice."',
			'".$val->listingprice_ytd."'			
		),";
				
		if($cnt>25)
		{
			$cnt 	= 0;
			if($values != '')
			{
				$values = substr($values,0,-1);
				$sql = "INSERT INTO ".$tbl." 	   
						(`ct_name_symbol`,
						`name`,
						`status`,
						`updated_time`,
						`market_cap_usd`,
						`volatility_ratio_10_days`,
						`atd`,
						`ytd`,
						`ath_price`,
						`atl_price`,
						`ath_price_date`,
						`atl_price_date`,						
						`minprice_24hr`,
						`maxprice_24hr`,						
						`minprice_7days`,
						`maxprice_7days`,
						`minprice_1months`,
						`maxprice_1months`,						
						`minprice_3months`,
						`maxprice_3months`,
						`minprice_ytd`,
						`maxprice_ytd`,						
						`sevenDays_avg_price`,
						`sevenDays_avg_volume`,
						`30days_avg_price`,
						`30days_avg_volume`,
						`gi_week_1`,
						`gi_week_2`,
						`gi_week_3`,
						`gi_week_4`,
						`gi_week_5`,
						`gi_week_6`,
						`gi_week_7`,
						`gi_week_8`,
						`gi_week_9`,
						`gi_week_10`,
						`gi_week_11`,
						`gi_week_12`,
						`gi_month_1`,
						`gi_month_2`,
						`gi_month_3`,
						`gi_month_4`,
						`gi_month_5`,
						`gi_month_6`,
						`gi_month_7`,
						`gi_month_8`,
						`gi_month_9`,
						`gi_month_10`,
						`gi_month_11`,
						`gi_month_12`,
						`gi_uarter_1`,
						`gi_uarter_2`,
						`gi_uarter_3`,
						`gi_uarter_4`,				
						`listingdate`,
						`listingprice`,
						`listingprice_ytd`				
						)
						values ".$values;
				echo $sql.'<br><br>';
				mysqli_query($con,$sql);	
			}			
			$values	= '';
		}
	}
	
	if($values != '')
	{
		$values = substr($values,0,-1);
		$sql = "INSERT INTO ".$tbl." 	   
				(`ct_name_symbol`,
				`name`,
				`status`,
				`updated_time`,
				`market_cap_usd`,
				`volatility_ratio_10_days`,
				`atd`,
				`ytd`,
				`ath_price`,
				`atl_price`,
				`ath_price_date`,
				`atl_price_date`,
				
				`minprice_24hr`,
				`maxprice_24hr`,
				
				`minprice_7days`,
				`maxprice_7days`,
				`minprice_1months`,
				`maxprice_1months`,
				
				`minprice_3months`,
				`maxprice_3months`,
				`minprice_ytd`,
				`maxprice_ytd`,
				
				`sevenDays_avg_price`,
				`sevenDays_avg_volume`,
				`30days_avg_price`,
				`30days_avg_volume`,
				`gi_week_1`,
				`gi_week_2`,
				`gi_week_3`,
				`gi_week_4`,
				`gi_week_5`,
				`gi_week_6`,
				`gi_week_7`,
				`gi_week_8`,
				`gi_week_9`,
				`gi_week_10`,
				`gi_week_11`,
				`gi_week_12`,
				`gi_month_1`,
				`gi_month_2`,
				`gi_month_3`,
				`gi_month_4`,
				`gi_month_5`,
				`gi_month_6`,
				`gi_month_7`,
				`gi_month_8`,
				`gi_month_9`,
				`gi_month_10`,
				`gi_month_11`,
				`gi_month_12`,
				`gi_uarter_1`,
				`gi_uarter_2`,
				`gi_uarter_3`,
				`gi_uarter_4`,				
				`listingdate`,
				`listingprice`,
				`listingprice_ytd`				
				)
				values ".$values;	
				 
		mysqli_query($con,$sql);	
	}
echo $sql.'<br><br>';

	echo 'Data Updated at :::::'.date('d-m-Y H:i:s').'<br><hr>';
	//echo $sql;
?>