<?php
include("../src/congif.php");
?>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title><?=COMPANY_NAME;?></title>
<?php include("../include/inc.links.php");?>
<?php include("../include/inc.header.php");?>
		<script type="text/javascript">
		$(document).ready( function () {
			$('#header_search_frm').hide();
			$('#table_inactivated_currencies').addClass( 'nowrap' ).dataTable( {responsive: false} );
			$('#table_inactivated_currencies_wrapper').css('width','100%');
		} );
		</script>

<div class="container-fluid" style="width:90%; margin:auto;">
	<div class="row print_table_container">
		<div class="col-xl-12 dahsboard-column">
			<div class="table_title">
				
				<?php
					if(!empty($_POST)){
						echo '<h4 style="color:#16B4FC;">Coin Name Updated Successfully !</h4>';
					}else{
						echo '<h4>Inactivated Coin List</h4>';
					}
				?>
				
			</div>
			<div class="table-responsive">
				<div class="container-fluid">				
					<div class="row rename_frm" style="display:none; margin:1%;">					
<form method="post" class="form-control">
<div class="col-md-12">
<div class="row" style="padding:1%;">
<div class="col-md-4">
<div class="form-group7 col-md-12">
	<label for="usr">Old Name :</label>
	<input type="text" name="old_coin" id="old_coin" readonly class="form-control">
</div>
</div>
<div class="col-md-4">
<div class="form-grou1p col-md-12">
	<label for="pwd">Select New Name :</label>
	<select id="coin_newname" name="coin_newname" class="form-control">
	<?php 
		$query 	= "select * from `coins_tokens` where updated_time >= '".date('Y-m-d',strtotime('-1 days'))."' order by updated_time desc limit 0,10";
		$row 	= $db->Execute($query);	
		foreach($row as $val){
			echo '<option val="'.$val['ct_id'].'">'.$val['name'].'</option>';
		}
	?>
	</select>
</div>
</div>
<div class="col-md-4">
<div class="form-group4 col-md-12">
	<label for="usr">&nbsp;</label>
	<input type="submit" name="sbmt" class="btn btn-submit" value="Update Coin Name" />
</div>	
</div>
	
</div></div>				
</form>			
				
					</div>
					<div class="row">
					<table id="table_inactivated_currencies" class="display print_table" cellspacing="0" style="width:100% !important;" >
						<thead>
						<tr><td>Sl.</td><td>Coin Name</td><td>Symbol</td><td>Rank</td><td>Price USD</td><td>Last Updated Date</td><td>Rename To</td></tr>
						</thead>
						<tbody>
							<?php 
								$i = 0;
								$allcoins = '';
								$query 	= "select * from `coins_tokens` where updated_time < '".date('Y-m-d',strtotime('-1 days'))."' order by updated_time desc limit 0,100";
								$row 	= $db->Execute($query);
								foreach($row as $val){
									$i++;
									echo '<tr><td>'.$i.'</td><td>'.$val['name'].'</td><td>'.$val['symbol'].'</td><td>'.$val['rank'].'</td><td>'.$foo->bd_nice_number($val['price_usd']).'</td><td>'.date('d-m-Y',strtotime($val['updated_time'])).'</td><td style="cursor:pointer;" onclick="open_frm(\''.$val['ct_id'].'\')">Edit</td></tr>';
								}
																								
								$query 	= "select * from `coins_tokens` order by updated_time asc limit 0,100";
								$row 	= $db->Execute($query);
								foreach($row as $val){
									$allcoins .= '\''.$val['ct_id'].'\',';
								}
								$allcoins = substr($allcoins,0,-1);
								
								
								$query 	= "select * from `coins_tokens_backup` where ct_id not in (".$allcoins.") and last_updated < 1520290000 order by last_updated desc limit 0,1000";
								$row 	= $db->Execute($query);
								foreach($row as $val){
									$i++;
									echo '<tr><td>'.$i.'</td><td>'.$val['name'].'</td><td>'.$val['symbol'].'</td><td>'.$val['rank'].'</td><td>'.$foo->bd_nice_number($val['price_usd']).'</td><td>'.date('d-m-Y',$val['last_updated']).'</td><td style="cursor:pointer;" onclick="open_frm(\''.$val['ct_id'].'\')">Edit</td></tr>';
								}																
							?>
						</tbody>
						</table>
					</div>
				</div> 
			</div> 
		</div> 
	</div> 
</div>
	<br>
<br>
<div style="height:50px;"></div>
<script>
open_frm = function(){
	var ct_id = arguments[0];
	$('#old_coin').val(ct_id);
	$('.rename_frm').show();
}
</script>

<?php include("include/inc.footer.php");?>
