<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Upticks.io - cryptocurrency analytics where they matter</title>
<link rel="shortcut icon" type="image/png" href="https://upticks.io/img/favicon0001.png" />
<link rel="icon" href="https://upticks.io/image/favicon.ico" type="image/ico" sizes="16x16">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113794290-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-116932664-1');
	</script>
<script src="https://upticks.io/js/jquery-2.1.4.min.js"></script>
<script src="https://upticks.io/js/bootstrap.bundle.min.js"></script>
<script src="https://upticks.io/js/bootstrap-datepicker.min.js"></script>
<script src="https://upticks.io/js/html2canvas.min.js"></script>
<script src="https://upticks.io/js/jspdf.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="https://upticks.io/css/bootstrap.min.css">
<link rel="stylesheet" href="https://upticks.io/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="https://upticks.io/css/main.css">
<link rel="stylesheet" href="https://upticks.io/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">





<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="https://upticks.io/css/dataTables.responsive.css">
<script type="text/javascript" src="https://upticks.io/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://upticks.io/js/dataTables.responsive.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://upticks.io/css/fixedColumns.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script> 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script> 


<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>





</head>
<body>
<h1>Hello Load Test <?php echo date('d-m-Y H:i:s');?></h1>
</body>
</html>