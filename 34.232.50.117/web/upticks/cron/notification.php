<?php
	include('../src/constant.php');
	$con = mysqli_connect(DBHOST,DBUSER,DBPASSWORD);
	if(!mysqli_select_db($con,DBNAME)){
		echo 'Error in DB connection';exit;
	}
	//include('../src/constant.php');
	include('../src/_functions.php');

	
	$selected_ctlist		= '';
	$userdata 				= array();
	//$currencydata 			= array();
	$cointokendata 			= array();
	$notificationdata 		= array();
	$sendnotificationdata 	= array();
	
	$compareto 		= array();
	$currencydata 	= file_get_contents('apidata/currencyconverter.json');
	$currencydata 	= json_decode($currencydata);
	$currencydata 	= $currencydata->quotes;
	/*foreach($currencyobj as $key=>$val){
		//$currencydata[$key] = $val;
	}*/
	//echo '<pre>';print_r($currencydata);exit;
	
	$sql = "select * from `users` order by id";
	$result 	= mysqli_query($con,$sql);			
	while($row = mysqli_fetch_assoc($result))
	{
		$userdata[$row['id']] = $row;
	}
	
	$sql = "select * from `notifications` where status = 1 order by user_id";
	$result 	= mysqli_query($con,$sql);			
	while($row = mysqli_fetch_assoc($result))
	{
		//echo $row['mailsentfrequentcy'].' == 1 && '.$row['mailsenttime'].'>0<br>';
		if($row['mailsentfrequentcy'] == 1 && $row['mailsenttime'] == 0){
			$selected_ctlist	.= "'".$row['ct_name_symbol']."',";
			$notificationdata[$row['user_id']][$row['ct_name_symbol']] = $row;
		}

		if($row['mailsentfrequentcy'] == 2 && $row['mailsenttime']+86400 < time()){
			$selected_ctlist	.= "'".$row['ct_name_symbol']."',";
			$notificationdata[$row['user_id']][$row['ct_name_symbol']] = $row;
		}
		
		if($row['mailsentfrequentcy'] == 3){
			$selected_ctlist	.= "'".$row['ct_name_symbol']."',";
			$notificationdata[$row['user_id']][$row['ct_name_symbol']] = $row;
		}
	}
	//$selected_ctlist = substr($selected_ctlist,0,-1);
	$selected_ctlist .= "'bitcoin_btc','ethereum_eth'";
	
	
	$sql = "select ct_name_symbol,price_usd,name ctname, symbol, ct_id, seourl from `coins_tokens_data` where ct_name_symbol in(".$selected_ctlist.") order by ids";
	$result 	= mysqli_query($con,$sql);			
	while($row = mysqli_fetch_assoc($result)) 
	{
		$cointokendata[$row['ct_name_symbol']] = $row;
	}
	//echo '<pre>';print_r($cointokendata);echo '</pre><hr><hr>';
	
	
	$i = -1;
	$mailsenttimeid = '';
	foreach($notificationdata as $key1=>$val1)
	{		
		//echo $key1.'+++++++++<pre>';print_r($val1);echo '</pre>';exit;
		foreach($val1 as $key2=>$val2)
		{		
			$ctprice 	= $cointokendata[$key2]['price_usd'];
			$ctname 	= $cointokendata[$key2]['ctname'];
			$ct_id 		= $cointokendata[$key2]['ct_id'];
			$ctsymbol 	= $cointokendata[$key2]['symbol'];
			$ctseourl 	= BASE_URL.$cointokendata[$key2]['seourl'];
			//echo '+++++++++<pre>';print_r($val2['upstick']);echo '</pre>';exit;//echo $ctprice;exit;
			
			$upstickcompareto = 'USD'.$val2['upstickcompareto'];
			$upstickcompareto = $currencydata->$upstickcompareto;
			
			$downstickcompareto = 'USD'.$val2['downstickcompareto'];
			//echo '...'.$val2['id'].'<br>';
			//echo '...'.$downstickcompareto.'<br>';
			$downstickcompareto = $currencydata->$downstickcompareto;
			//echo '...'.$downstickcompareto.'<br>';
			
			$upstick	= $val2['upstick']/$upstickcompareto;
			$downstick	= $val2['downstick']/$downstickcompareto;
			//echo 'ctprice...'.$ctprice.'<br>';
			//echo 'downstick...'.$downstick.'<hr><br>';
			
			
			//echo $val2['upstick'].'++++'.$val2['downstick'].'<br>';
			//echo $upstickcompareto.$val2['upstickcompareto'].'****'.$downstickcompareto.$val2['downstickcompareto'].'<br>';
			//echo $upstick.'.....'.$downstick.'<hr><hr>';//exit;
			
			if($upstick < $ctprice or $downstick > $ctprice)
			{
				$i++;
				$ctnamesymbol	= $val2['ct_name_symbol'];				
				$sendnotificationdata[$key1]['user_id'] 	= $userdata[$key1]['id'];
				$sendnotificationdata[$key1]['user_email'] 	= $userdata[$key1]['email'];
				$sendnotificationdata[$key1]['user_fname'] 	= $userdata[$key1]['fname'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['ct_name_symbol'] 	= $val2['ct_name_symbol'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['ctname'] 			= $cointokendata[$val2['ct_name_symbol']]['ctname'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['symbol'] 			= $cointokendata[$ctnamesymbol]['symbol'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['ct_id'] 			= $cointokendata[$val2['ct_name_symbol']]['ct_id'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['ctseourl'] 		= BASE_URL.$cointokendata[$val2['ct_name_symbol']]['seourl'];//$ctseourl;
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['price_usd'] 		= $cointokendata[$val2['ct_name_symbol']]['price_usd'];
				$mailsenttimeid 	.= $val2['id'].',';
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['upstick'] 			= $val2['upstick'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['downstick'] 		= $val2['downstick'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['upstickcompareto'] 			= $val2['upstickcompareto'];
				$sendnotificationdata[$key1]['coinlist'][$ctnamesymbol]['downstickcompareto'] 		= $val2['downstickcompareto'];
			}
		}
	}
	$mailsenttimeid = substr($mailsenttimeid,0,-1);
	$sql = "update notifications set mailsenttime = ".time()." where id in (".$mailsenttimeid.")";
	mysqli_query($con,$sql);
	
	//echo '<pre>';print_r($sendnotificationdata);exit;	
	foreach($sendnotificationdata as $key=>$val)
	{
		$maildata = array();;
		$maildata['to'] 		= $val['user_email'];//'support@69ideas.com';//
		$maildata['bcc']		= 'ritadas20@gmail.com';
		$maildata['subject'] 	= COMPANY_NAME.' Price Notification on '.date('d/m/Y H:i:s');
		$maildata['body'] 		= json_encode($val['coinlist']);
		
		//echo '<pre>';print_r($val['coinlist']);exit;
		
		$str  = 'Hi '.$val['user_fname'].',<br><br> Lorem Ipsum is also known as: Greeked text, blind text, placeholder text, dummy content, filler text, lipsum, and mock-content. Lorem Ipsum is also known as: Greeked text, blind text, placeholder text, dummy content, filler text, lipsum, and mock-content.<br><br>';
		$str .= '<table width="90%" cellpadding="8%" border="1" style="border:1px solid #000; border-collapse: collapse;">
		<tr style="background-color:#00A8FF;color:#fff;"><td><b>Coin or Token Name</b></td><td><b>Current Price</b></td><td><b>Upstick Price</b></td><td><b>Downstick</b></td></tr>';
		foreach($val['coinlist'] as $coindata)
		{
			$str .= '<tr><td><img src="'.BASE_URL.'img/coins/16x16/'.$coindata['ct_id'].'.png" /> &nbsp; <a href="'.$coindata['ctseourl'].'" style="color:#000; cursor:pointer; font-weight:bold;">'.$coindata['ctname'].' ( '.$coindata['symbol'].' )</a></td>';
			$str .= '<td>$'.$coindata['price_usd'].'</td>';
			$str .= '<td>'.$coindata['upstick'].' '.$coindata['upstickcompareto'].'</td>'; 
			$str .= '<td>'.$coindata['downstick'].' '.$coindata['downstickcompareto'].'</td></tr>';
		}
		$str .= '</table>'.MAILFROM;
		
		$maildata['body'] 		= $str;
		$foo->sendmail($maildata);
	
		echo '<br>Mail Sent to '.$val['user_email'].'<br>
		';
		//echo $str;
		//echo '<hr><hr>';
	}

	echo 'Notification Sent
	';
	//echo '+++++++++<pre>';print_r($sendnotificationdata);echo '</pre>';exit;
	
?>