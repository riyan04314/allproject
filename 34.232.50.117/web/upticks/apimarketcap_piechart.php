<?php
	include_once('src/congif.php');
	$returndata		= array();
	$coindata		= array();
	$coin 			= array();
	$total_volume	= 0;
	$volume			= 0;
	$i = 0;
	$color = array('#68e25a','#5a64e2','#e25aa5','#e2d85a','#e2865a','#75e25a','#5ae2c9','#5a8ae2','#c75ae2','#474fb5','#d4b680');

	if(!empty($_REQUEST['numberofday']) and in_array($_REQUEST['numberofday'],array('7D','30D','90D','6M','1Y','YTD','ALL')))
	{
		
		$numberofday	= '-7 days';
		$dtd			= date('Y-m-d',strtotime($numberofday));
		
		if(FALSE){
			$selected_dtd	= $_REQUEST['numberofday'].'D';
			$numberofday	= '-'.$_REQUEST['numberofday'].' days';
			$dtd			= date('Y-m-d',strtotime($numberofday));
		}else{
			$numberofday 	= $_REQUEST['numberofday'];
		if($numberofday == '7D'){
				$selected_dtd	= '7D';
				$dtd	= date('Y-m-d',strtotime('-7 days'));
			}
			elseif($numberofday == '30D'){
				$selected_dtd	= '30D';
				$dtd	= date('Y-m-d',strtotime('-30 days'));
			}
			elseif($numberofday == '1M'){
				$selected_dtd	= '1M';
				$dtd	= date('Y-m-d',strtotime('-30 days'));
			}
			elseif($numberofday == '90D'){
				$selected_dtd	= '90D';
				$dtd	= date('Y-m-d',strtotime('-90 days'));
			}
			elseif($numberofday == '3M'){
				$selected_dtd	= '3M';
				$dtd	= date('Y-m-d',strtotime('-90 days'));
			}
			elseif($numberofday == '6M'){
				$selected_dtd	= '6M';
				$dtd	= date('Y-m-d',strtotime('-6 months'));
			}			
			elseif($numberofday == '1Y'){
				$selected_dtd	= '1Y';
				$dtd	= date('Y-m-d',strtotime('-365 days'));
			}
			elseif($numberofday == 'YTD'){
				$selected_dtd	= 'YTD';
				$dtd	= date('Y').'-01-01';
			}
			elseif($numberofday == 'ALL'){
				$selected_dtd	= 'ALL';
				$dtd	= '2010-01-01';
			}
		}		
		
		
		
		$coindetails 	= array();
		$coinslist	 	= '';
		
		$sql = "select ct_name_symbol,sum(volum) total_volume from `historical_data` where date > '".$dtd."' group by ct_name_symbol order by total_volume  desc limit 10";
		//echo $sql.'<br>';
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$coinslist		.= "'".$val['ct_name_symbol']."',";
		}
		$coinslist	= substr($coinslist,0,-1);		
		
		$sql = "select ids,ct_id, ct_name_symbol, name, symbol, 24h_volume_usd from `coins_tokens_data` where ct_name_symbol in (".$coinslist.") limit 9";
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$coindetails[$val['ct_name_symbol']] 	= $val;
		}
		
		$sql = "select sum(market_cap) market_cap from `historical_data` where date > '".$dtd."'";
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$total_market_cap	= $val['market_cap'];
		}		
		
		$sql = "select ct_name_symbol,sum(market_cap) market_cap from `historical_data` where date > '".$dtd."' and ct_name_symbol in (".$coinslist.") group by ct_name_symbol order by market_cap desc";
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$i++;
			

			$coin['label']		= $coindetails[strtolower($val['ct_name_symbol'])]['name'];
			$coin['id']			= $coindetails[strtolower($val['ct_name_symbol'])]['symbol'];
			$coin['icon']		= BASE_URL_IMG.'img/coins/24x24/'.$coindetails[strtolower($val['ct_name_symbol'])]['ct_id'].'.png';			
			
			if(empty($coindetails[strtolower($val['ct_name_symbol'])]['name'])){
				$coin['label']		= 'Ethereum Classic';
				$coin['id']			= 'ETC';
				$coin['icon']		= BASE_URL_IMG.'img/coins/24x24/ethereumclassic.png';			
			
			}
			
			
			$coin['value']		= number_format(($val['market_cap']/$total_market_cap)*100,2);
			$coin['color']		= $color[$i];
			$coin['price']		= 100;
			$market_cap			+= $val['market_cap'];
			$coindata[] 		= $coin;
		}			
		
		$i++;
		$coin['label']	= 'Others';
		$coin['id']		= 'Others';
		$coin['icon']	= '';
		$coin['value']	= number_format((($total_market_cap-$market_cap)/$total_market_cap)*100,2);
		$coin['color']	=  '#9e9e9e';
		$coin['price']		= 100;
		
		$coindata[] 	= $coin;
		$returndata['selected_dtd']	= $selected_dtd;
		$returndata['total_market_cap_usd']	= $total_market_cap;	
		$returndata['coindata']		= $coindata;		
	}
	else
	{
		$sql = "select sum(market_cap_usd) market_cap_usd from `coins_tokens_data`";
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$market_cap_usd	= $val['market_cap_usd'];
		}
		
		$sql = "select ids,ct_id, ct_name_symbol, name, symbol, market_cap_usd from `coins_tokens_data` order by market_cap_usd desc limit 9";
		$res = mysqli_query($mysqlicon,$sql);
		while($val = mysqli_fetch_array($res)){
			$i++;
			$coin['label']	= $val['name'];
			$coin['id']		= $val['symbol'];
			$coin['icon']		= BASE_URL_IMG.'img/coins/24x24/'.$val['ct_id'].'.png';	
			$coin['value']	= number_format(($val['market_cap_usd']/$market_cap_usd)*100,2);
			$coin['color']	= $color[$i];
			$coin['price']		= 100;
			$volume					+= $val['market_cap_usd'];
			$coindata[] 			= $coin;
		}
		$i++;
		$coin['label']	= 'Others';
		$coin['id']		= 'Others';
		$coin['icon']	= '';
		$coin['value']	= number_format((($market_cap_usd-$volume)/$market_cap_usd)*100,2);
		$coin['color']	= '#9e9e9e';
		$coin['price']		= 100;
		$coindata[] 	= $coin;	
		$returndata['selected_dtd']	= '1D';
		$returndata['total_market_cap_usd']	= $market_cap_usd;
		$returndata['coindata']				= $coindata;
		
		
		
	}
	header("Access-Control-Allow-Origin:*"); 
	header('Content-Type: application/json');		
	echo json_encode($returndata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
?>