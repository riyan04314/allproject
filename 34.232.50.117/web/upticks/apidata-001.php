<?php
	@session_start();
	include_once('src/congif.php');
	$myfavlist = "";
	if(!empty($_SESSION["member_id"])){
		$sql = "select * from `usersfavorites` where userid = ".$_SESSION["member_id"];
		$res = mysqli_query($mysqlicon,$sql);
		while($val=mysqli_fetch_array($res)){
			$myfavlist .= "'".$val['ct_name_symbol']."',";
		}
		$myfavlist = substr($myfavlist,0,-1);
	}
	if(isset($_COOKIE['FAV']) && $_COOKIE['FAV'] != '') {
		if($myfavlist == ''){
			$myfavlist .= '\'coinlist\'';
		}
		$arr = explode(',',$_COOKIE['FAV']);
		foreach($arr as $val){
			$myfavlist .= ',\''.$val.'\'';
		}
	}
	$new = array();
	$limit 	= 100;
	
	$pnum = 1;
	$slimit = 0;
	if(!empty($_REQUEST['slimit'])){
		$slimit = $_REQUEST['slimit'];
		$limit = $_REQUEST['slimit'].",10";
		$i = $slimit;
	}
	elseif(!empty($_REQUEST['start']) && $_REQUEST['start']>1){
		//$limit = $_REQUEST['start'].",100";
		$limit = (($_REQUEST['start']-1)*100).",100";
		$i = $_REQUEST['start'];
		$pnum = $_REQUEST['start'];
	}
	
	$totalcnt = 1988;
	
	
	
	if(isset($_COOKIE['details'])){
		$new = explode(',', $_COOKIE['details']);
	}
	$topsortby 	= 'marketcap';
	$topgroupby = 'cointoken';
	if(!empty($_POST['topgroupby']) && in_array($_POST['topgroupby'],array('coin','token','both'))){
		$topgroupby	= $_POST['topgroupby'];
		$_SESSION['topgroupby']	= $topgroupby;
	}
	if(!empty($_POST['topsortby']) && in_array($_POST['topsortby'],array('market_cap_usd','price_usd','24h_volume_usd','percent_change_1h','percent_change_24h','percent_change_7d'))){
		$topsortby	= $_POST['topsortby'];
		$_SESSION['topsortby']	= $topsortby;
	}

	$type 		= 'both';
	$orderby 	= 'market_cap_usd';
	if(!empty($_SESSION['topgroupby']) && in_array($_SESSION['topgroupby'],array('coin','token'))){
		$type = $_SESSION['topgroupby'];
	}
	if(!empty($_SESSION['topsortby']) && in_array($_SESSION['topsortby'],array('market_cap_usd','price_usd','24h_volume_usd','percent_change_1h','percent_change_24h','percent_change_7d'))){
		$orderby = $_SESSION['topsortby'];
	}
	
	$whr = '';
	if(!empty($_POST['search']['value'])){
		$whr = " AND c.ct_name_symbol like '%".$_POST['search']['value']."%' ";
	}
		
	$ct_type = '';
	$favactive = '';
	if(!empty($_REQUEST['ct_type'])){
		if($_REQUEST['ct_type'] == 'coin'){
			$ct_type = 'coin';
		}
		elseif($_REQUEST['ct_type'] == 'token'){
			$ct_type = 'token';
		}
		elseif($_REQUEST['ct_type'] == 'favorites'){
			$ct_type = 'favorites';
			$favactive = ' active ';
		}
	}	
				$no_of_col = 18;
				$query = "SELECT
				c.ct_id,
				c.ct_name_symbol,
				c.name,
				c.seourl,
				c.symbol,				
				c.price_usd,
				c.price_btc,
				c.market_cap_usd,
				c.24h_volume_usd,
				c.percent_change_1h,
				c.percent_change_24h,
				c.percent_change_7d,
				g.minprice_7days,
				g.maxprice_7days,
				g.minprice_1months,
				g.maxprice_1months,
				g.ytd,
				g.atd,
				g.gi_month_1,
				g.gi_month_2,
				g.gi_month_3,
				g.gi_month_6
				FROM coins_tokens_data c
				LEFT JOIN growth_indexes g
				ON c.ct_id = g.name 
				WHERE  c.ids > 0  
				and c.price_usd > 0 ".$whr."
				and c.ct_name_symbol not in ('rimbit_rbt')
				";
				
				if($ct_type == 'coin' or $ct_type == 'token'){
					$query .= " AND c.type = '".$ct_type."' ";
				}
				elseif($ct_type == 'favorites' && $myfavlist != ''){
					$query .= "  AND c.ct_name_symbol in (".$myfavlist.")";
				}
				
				$query .= " ORDER BY c.".$orderby."+0 DESC ";
				$query .= " LIMIT ".$limit;
				$favquery = $query;
				//echo $favquery;exit;
		$apidata = array();
		$apidata['totalpages'] 	= 26;
		$apidata['currentpage'] = $pnum;
		$apires = mysqli_query($mysqlicon,$favquery);
		$i = -1;
		$coinsname = '';
		$historydata	= array();
		foreach($apires as $val)
		{
			$i++;
			$apidata['coins'][$val['ct_name_symbol']]	= $val;	
			$apidata['coins'][$val['ct_name_symbol']]['percent_change_7d']		= number_format($val['percent_change_7d'],2);
			if($val['seourl'] == 'seourl'){
				$apidata['coins'][$val['ct_name_symbol']]['seourl'] = BASE_URL.'searchr?search='.$val['name'].'&symbol='.$val['symbol'];
			}
			$apidata['coins'][$val['ct_name_symbol']]['icon']	= BASE_URL_IMG.'img/coins/24x24/'.$val['ct_id'].'.png';			
			$coinsname .= '\''.$val['ct_name_symbol'].'\',';
			$historydata[$val['ct_name_symbol']] = time()+1548327577000;
		}
		
		
		$coinsname = substr($coinsname,0,-1);
		
		/*$sql = "select ct_name_symbol, date, close price, volum volume from `historical_data` where ct_name_symbol in (".$coinsname.") and date > '".date('Y-m-d',strtotime('-29 day'))."' order by date desc";
		$apires = mysqli_query($mysqlicon,$sql);
		$i = -1;
		$dtd = array();
		foreach($apires as $val)
		{
			if(in_array($val['date'],$dtd)){
				//continue;
			}
			$dtd[] = $val['date'];
			
			$date 	= $val['date'];
			$price 	= $val['price'];
			$volume = $val['volume'];
			//
			
			$pricearr['x'] 		= $date;
			$pricearr['y'] 		= $price;
			$volumearr['x'] 	= $date;
			$volumearr['y'] 	= $volume;			
			$i++;
			$ct_name_symbol = $val['ct_name_symbol'];
			unset($val['ct_name_symbol']);
			//$apidata['coins'][$ct_name_symbol]['history_data'][]	= $val;
			
			$apidata['coins'][$ct_name_symbol]['history_price_data'][]	= $pricearr;
			$apidata['coins'][$ct_name_symbol]['history_volume_data'][]	= $volumearr;
		}*/

		
		$sql 	= "select * from `graphdata_final` where ct_name_symbol in (".$coinsname.")  and ( dtdtime like '%:00:01' or  dtdtime like '%:00:00' or  dtdtime like '%:59:00') and ( dtd > '".date("Y-m-d",strtotime("-7 days"))."') order by timestamp asc";
		//$sql 	= "select * from `graphdata_bk` where ct_name_symbol in (".$coinsname.") and timestamp  > 1547300764000 order by timestamp desc";
		//echo $sql 	= "select * from `graphdata` where ct_name_symbol in ('bitcoin_btc','ripple_xrp','ethereum_eth') order by timestamp desc limit 2000";
		//echo $sql;exit;
		$apires = mysqli_query($mysqlicon,$sql);
		foreach($apires as $val)
		{
			//echo '<pre>';print_r($val);echo '</pre>';
			if(strtolower($val['ct_name_symbol']) == 'bitcoin_btc'){
				//echo '('.$historydata[strtolower($val['ct_name_symbol'])].'-'.$val['timestamp'].')<10000000<br>';
			}
			
			if(($historydata[strtolower($val['ct_name_symbol'])]-$val['timestamp'])<4*60*60*1000){//8
				//continue;
			}
			
			$historydata[strtolower($val['ct_name_symbol'])] = $val['timestamp'];
			$pricearr['x'] 		= date('Y-m-d H:i:s',$val['timestamp']/1000);
			$pricearr['y'] 		= $val['price_usd'];
			$volumearr['x'] 	= date('Y-m-d H:i:s',$val['timestamp']/1000);
			$volumearr['y'] 	= $val['volume_usd'];
		
			$apidata['coins'][strtolower($val['ct_name_symbol'])]['history_price_data'][]	= $pricearr;
			$apidata['coins'][strtolower($val['ct_name_symbol'])]['history_volume_data'][]	= $volumearr;		
		}		
		
		
		
		$i = 0;
		$resdata = array();
		$resdata['totalpages'] 	= 26;
		$resdata['currentpage'] = $pnum;
		foreach($apidata['coins'] as $val)
		{
			$i++;
			$val['rank']	= $i;
			$resdata['coins'][] = $val;
		}
header("Access-Control-Allow-Origin:*"); 
header('Content-Type: application/json');		
echo json_encode($resdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
?>