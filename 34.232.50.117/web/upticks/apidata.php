<?php
	@session_start();
	include_once('src/congif.php');
	
	$i = 0;
	$arr = array();
	$current	= 0;
	$coins_ohlcv	= array();
	while($i<1){
		$i++;
		$arr['start_time']	= date('d-m-Y h:i A',time());
		$arr['end_time']	= date('d-m-Y h:i A',time()-24*60*60);
		
		$x = rand(5200,5500);
		$y = rand(5200,5500);
		if($current == 0){
			$current = rand(5200,5350);
		}		
		
		$arr['low']		= rand(5000,5150);
		$arr['current']	= $current;
		$arr['high']	= rand(5400,5500);
		$coins_ohlcv[]	= $arr;
	}
	
	
	$btcusd = 0;
	$sql = "select price_usd from `coins_tokens_data` where ct_name_symbol = 'bitcoin_btc'";
	$res = mysqli_query($mysqlicon,$sql);
	while($val = mysqli_fetch_array($res)){
		$btcusd = $val['price_usd'];
	}	
	
	
	$myfavlist = "";
	if(!empty($_SESSION["member_id"])){
		$sql = "select * from `usersfavorites` where userid = ".$_SESSION["member_id"];
		$res = mysqli_query($mysqlicon,$sql);
		while($val=mysqli_fetch_array($res)){
			$myfavlist .= "'".$val['ct_name_symbol']."',";
		}
		$myfavlist = substr($myfavlist,0,-1);
	}
	if(isset($_COOKIE['FAV']) && $_COOKIE['FAV'] != '') {
		if($myfavlist == ''){
			$myfavlist .= '\'coinlist\'';
		}
		$arr = explode(',',$_COOKIE['FAV']);
		foreach($arr as $val){
			$myfavlist .= ',\''.$val.'\'';
		}
	}
	$new = array();
	$limit 	= 100;
	
	$pnum = 1;
	$slimit = 0;
	if(!empty($_REQUEST['slimit']) && $_REQUEST['slimit']>0){
		$slimit = $_REQUEST['slimit'];
		$limit = $_REQUEST['slimit'].",10";
		$i = $slimit;
	}
	elseif(!empty($_REQUEST['start']) && $_REQUEST['start']>1){
		$slimit = $_REQUEST['start'];
		//$limit = $_REQUEST['start'].",100";
		$limit = (($_REQUEST['start']-1)*100).",100";
		$i = $_REQUEST['start'];
		$pnum = $_REQUEST['start'];
	}
	
	$totalcnt = 1988;
	
	
	
	if(isset($_COOKIE['details'])){
		$new = explode(',', $_COOKIE['details']);
	}
	$topsortby 	= 'marketcap';
	$topgroupby = 'cointoken';
	if(!empty($_POST['topgroupby']) && in_array($_POST['topgroupby'],array('coin','token','both'))){
		$topgroupby	= $_POST['topgroupby'];
		$_SESSION['topgroupby']	= $topgroupby;
	}
	if(!empty($_POST['topsortby']) && in_array($_POST['topsortby'],array('market_cap_usd','price_usd','24h_volume_usd','percent_change_1h','percent_change_24h','percent_change_7d'))){
		$topsortby	= $_POST['topsortby'];
		$_SESSION['topsortby']	= $topsortby;
	}

	$type 		= 'both';
	$orderby 	= 'market_cap_usd';
	if(!empty($_SESSION['topgroupby']) && in_array($_SESSION['topgroupby'],array('coin','token'))){
		$type = $_SESSION['topgroupby'];
	}
	if(!empty($_SESSION['topsortby']) && in_array($_SESSION['topsortby'],array('market_cap_usd','price_usd','24h_volume_usd','percent_change_1h','percent_change_24h','percent_change_7d'))){
		$orderby = $_SESSION['topsortby'];
	}
	
	$whr = '';
	if(!empty($_POST['search']['value'])){
		$whr = " AND c.ct_name_symbol like '%".$_POST['search']['value']."%' ";
	}
		
	$ct_type = '';
	$favactive = '';
	if(!empty($_REQUEST['ct_type'])){
		if($_REQUEST['ct_type'] == 'coin'){
			$ct_type = 'coin';
		}
		elseif($_REQUEST['ct_type'] == 'token'){
			$ct_type = 'token';
		}
		elseif($_REQUEST['ct_type'] == 'favorites'){
			$ct_type = 'favorites';
			$favactive = ' active ';
		}
	}	
				$no_of_col = 18;
				$query = "SELECT
				c.ct_id,
				c.ct_name_symbol,
				c.name,
				c.seourl,
				c.symbol,				
				c.price_usd,
				c.price_btc,
				c.market_cap_usd,
				c.24h_volume_usd,
				c.percent_change_1h,
				c.percent_change_24h,
				c.percent_change_7d,
				g.minprice_24hr,
				g.maxprice_24hr,
				g.minprice_7days,
				g.maxprice_7days,
				g.minprice_1months,
				g.maxprice_1months,
				g.ath_price,
				g.atl_price,
				g.ytd,
				g.atd,
				g.gi_month_1,
				g.gi_month_2,
				g.gi_month_3,
				g.gi_month_6
				FROM coins_tokens_data c
				LEFT JOIN growth_indexes g
				ON c.ct_id = g.name 
				WHERE  c.ids > 0  
				and c.price_usd > 0 ".$whr."
				and c.ct_name_symbol not in ('rimbit_rbt')
				";
				
	if($ct_type == 'coin' or $ct_type == 'token'){
		$query .= " AND c.type = '".$ct_type."' ";
	}
	elseif($ct_type == 'favorites' && $myfavlist != ''){
		$query .= "  AND c.ct_name_symbol in (".$myfavlist.")";
	}
	
	$query .= " ORDER BY c.".$orderby."+0 DESC ";
	$query .= " LIMIT ".$limit;
	$favquery = $query;
	//echo $favquery;exit;
	$apidata = array();
	$apidata['totalpages'] 	= 26;
	$apidata['currentpage'] = $pnum;
	$apires = mysqli_query($mysqlicon,$favquery);
	$i = -1;
	$coinsname = '';
	$historydata	= array();
	foreach($apires as $val)
	{
		$i++;
		$apidata['coins'][$val['ct_name_symbol']]	= $val;	
		$apidata['coins'][$val['ct_name_symbol']]['percent_change_7d']		= number_format($val['percent_change_7d'],2);
		if($val['seourl'] == 'seourl'){
			$apidata['coins'][$val['ct_name_symbol']]['seourl'] = BASE_URL.'searchr?search='.$val['name'].'&symbol='.$val['symbol'];
		}
		$apidata['coins'][$val['ct_name_symbol']]['icon']	= BASE_URL_IMG.'img/coins/24x24/'.$val['ct_id'].'.png';
		
		
		$decimalplace = 2;
		if($val['price_usd']<1){
			$decimalplace = 4;
		}		
		

		$coins_ohlcv[0]['low']		= number_format($val['minprice_24hr'],$decimalplace);
		$coins_ohlcv[0]['current']	= '';//number_format($val['price_usd'],$decimalplace);
		$coins_ohlcv[0]['high']		= number_format($val['maxprice_24hr'],$decimalplace);		
		
		
		if($coins_ohlcv[0]['low'] > $val['price_usd']){
			$coins_ohlcv[0]['low']	= number_format($val['price_usd'],$decimalplace);
		}
		if($coins_ohlcv[0]['high'] < $val['price_usd']){
			$coins_ohlcv[0]['high']	= number_format($val['price_usd'],$decimalplace);
		}
		
		

		
		/*$sql 		= "select min(price_btc) min_price, max(price_btc) max_price from `graphdata_final` where ct_name_symbol = '".$val['ct_name_symbol']."' and dtdtime > '2019-04-02'  order by dtdtime desc ";
		$ohlcv_res 	= mysqli_query($mysqlicon,$sql);
		foreach($ohlcv_res as $ohlcv_val)
		{
			$decimalplace = 2;
			if($val['price_usd']<1){
				$decimalplace = 4;
			}
			$coins_ohlcv[0]['low']		= number_format($ohlcv_val['min_price']*$btcusd,$decimalplace);
			$coins_ohlcv[0]['current']	= number_format($val['price_usd'],$decimalplace);
			$coins_ohlcv[0]['high']		= number_format($ohlcv_val['max_price']*$btcusd,$decimalplace);
			//echo '<pre>';print_r($coins_ohlcv);echo '</pre>';
			//$coins_ohlcv
		}/**/		
		
		$apidata['coins'][$val['ct_name_symbol']]['ohlcv'] 	= $coins_ohlcv;
		$coinsname .= '\''.$val['ct_name_symbol'].'\',';
		$historydata[$val['ct_name_symbol']] = time()+1548327577000;
		
		
	}
	
	$coinsname = substr($coinsname,0,-1);	
	$sql 	= "select * from `graphdata_final` where ct_name_symbol in (".$coinsname.")  and ( dtdtime like '%:00:01' or  dtdtime like '%:00:00' or  dtdtime like '%:59:00') and ( dtd > '".date("Y-m-d",strtotime("-7 days"))."') order by timestamp asc";
	$apires = mysqli_query($mysqlicon,$sql);
	foreach($apires as $val)
	{
		
		if(($historydata[strtolower($val['ct_name_symbol'])]-$val['timestamp'])<4*60*60*1000){//8
			//continue;
		}
		
		if(time()-24*60*60*1000<$val['timestamp']/1000)
		{
			//echo $apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_24hr'].' < '.$val['price_usd'];exit;
			
			if($apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_ytd']<$val['price_usd']){
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_ytd'] = $val['price_usd'];
			}
			
			
			if($apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_ytd']>$val['price_usd']){
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_ytd'] = $val['price_usd'];
			}
			
			if($apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_24hr']<$val['price_usd'])
			{
				$decimalplace = 2;
				if($val['price_usd']<1){
					$decimalplace = 4;
				}					
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['ohlcv'][0]['high']	= number_format($val['price_usd'],$decimalplace);;
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_24hr'] = $val['price_usd'];
				if($apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_7days']<$val['price_usd'])
				{
					$apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_7days'] = $val['price_usd'];
					if($apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_1months']<$val['price_usd'])
					{
						$apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_1months'] = $val['price_usd'];
						if($apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_3months']<$val['price_usd'])
						{
							$apidata['coins'][strtolower($val['ct_name_symbol'])]['maxprice_3months'] = $val['price_usd'];
							if($apidata['coins'][strtolower($val['ct_name_symbol'])]['ath_price']<$val['price_usd'])
							{
								$apidata['coins'][strtolower($val['ct_name_symbol'])]['ath_price'] = $val['price_usd'];
								
							}
						}						
					}
				}
			}
			
			if($apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_24hr']>$val['price_usd'])
			{
				$decimalplace = 2;
				if($val['price_usd']<1){
					$decimalplace = 4;
				}					
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['ohlcv'][0]['low']	= number_format($val['price_usd'],$decimalplace);;
				$apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_24hr'] = $val['price_usd'];
				if($apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_7days']>$val['price_usd'])
				{
					$apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_7days'] = $val['price_usd'];
					if($apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_1months']>$val['price_usd'])
					{
						$apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_1months'] = $val['price_usd'];
						if($apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_3months']>$val['price_usd'])
						{
							$apidata['coins'][strtolower($val['ct_name_symbol'])]['minprice_3months'] = $val['price_usd'];
							if($apidata['coins'][strtolower($val['ct_name_symbol'])]['atl_price']>$val['price_usd'])
							{
								$apidata['coins'][strtolower($val['ct_name_symbol'])]['atl_price'] = $val['price_usd'];
							}
						}
					}					
				}
			}
				
			
			/*
			
			minprice_ytd
			maxprice_ytd
			
			minprice_24hr			
			minprice_7days
			minprice_1months
			minprice_3months
			atl_price
			
			maxprice_24hr
			maxprice_7days
			maxprice_1months
			maxprice_3months
			ath_price
			
			*/			
		}

		
		
		
		
		
		$historydata[strtolower($val['ct_name_symbol'])] = $val['timestamp'];
		$pricearr['x'] 		= date('Y-m-d H:i:s',$val['timestamp']/1000);
		$pricearr['y'] 		= $val['price_usd'];
		$volumearr['x'] 	= date('Y-m-d H:i:s',$val['timestamp']/1000);
		$volumearr['y'] 	= $val['volume_usd'];
	
		$apidata['coins'][strtolower($val['ct_name_symbol'])]['history_price_data'][]	= $pricearr;
		$apidata['coins'][strtolower($val['ct_name_symbol'])]['history_volume_data'][]	= $volumearr;			
	}		
	
	
	if($slimit == 0){
		$slimit = 1;
	}
	$i = ($slimit-1)*$limit;
	$resdata = array();
	$resdata['totalpages'] 	= 3;
	$resdata['currentpage'] = $pnum;
	foreach($apidata['coins'] as $val)
	{
		$i++;
		$val['rank']		= $i;
		$resdata['coins'][] = $val;
	}
	header("Access-Control-Allow-Origin:*"); 
	header('Content-Type: application/json');		
	echo json_encode($resdata, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
?>