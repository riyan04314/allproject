-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: webspoons
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additional_functions`
--

DROP TABLE IF EXISTS `additional_functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `additional_functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscription_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `additional_functions_subscription_id_foreign` (`subscription_id`),
  CONSTRAINT `additional_functions_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `additional_functions`
--

LOCK TABLES `additional_functions` WRITE;
/*!40000 ALTER TABLE `additional_functions` DISABLE KEYS */;
/*!40000 ALTER TABLE `additional_functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_state_id_foreign` (`state_id`),
  CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sortname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_websites`
--

DROP TABLE IF EXISTS `custom_websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_websites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maker_answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `custom_websites_website_id_foreign` (`website_id`),
  KEY `custom_websites_user_id_foreign` (`user_id`),
  CONSTRAINT `custom_websites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `custom_websites_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_websites`
--

LOCK TABLES `custom_websites` WRITE;
/*!40000 ALTER TABLE `custom_websites` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_websites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deployed_site_domains`
--

DROP TABLE IF EXISTS `deployed_site_domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployed_site_domains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain_id` int(10) unsigned NOT NULL,
  `deployed_site_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deployed_site_domains_domain_id_unique` (`domain_id`),
  KEY `deployed_site_domains_deployed_site_id_foreign` (`deployed_site_id`),
  CONSTRAINT `deployed_site_domains_deployed_site_id_foreign` FOREIGN KEY (`deployed_site_id`) REFERENCES `deployed_sites` (`id`),
  CONSTRAINT `deployed_site_domains_domain_id_foreign` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deployed_site_domains`
--

LOCK TABLES `deployed_site_domains` WRITE;
/*!40000 ALTER TABLE `deployed_site_domains` DISABLE KEYS */;
/*!40000 ALTER TABLE `deployed_site_domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deployed_site_site_host`
--

DROP TABLE IF EXISTS `deployed_site_site_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployed_site_site_host` (
  `deployed_site_id` int(10) unsigned NOT NULL,
  `site_host_id` int(10) unsigned NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`deployed_site_id`,`site_host_id`),
  KEY `deployed_site_site_host_deployed_site_id_index` (`deployed_site_id`),
  KEY `deployed_site_site_host_site_host_id_index` (`site_host_id`),
  CONSTRAINT `deployed_site_site_host_deployed_site_id_foreign` FOREIGN KEY (`deployed_site_id`) REFERENCES `deployed_sites` (`id`) ON DELETE CASCADE,
  CONSTRAINT `deployed_site_site_host_site_host_id_foreign` FOREIGN KEY (`site_host_id`) REFERENCES `site_hosts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deployed_site_site_host`
--

LOCK TABLES `deployed_site_site_host` WRITE;
/*!40000 ALTER TABLE `deployed_site_site_host` DISABLE KEYS */;
/*!40000 ALTER TABLE `deployed_site_site_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deployed_sites`
--

DROP TABLE IF EXISTS `deployed_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployed_sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  `subscription_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version_id` int(10) unsigned DEFAULT NULL,
  `isp_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isp_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isp_base_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deployed_sites_customer_id_foreign` (`customer_id`),
  KEY `deployed_sites_website_id_foreign` (`website_id`),
  KEY `deployed_sites_subscription_id_foreign` (`subscription_id`),
  KEY `deployed_sites_version_id_foreign` (`version_id`),
  CONSTRAINT `deployed_sites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `deployed_sites_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `subscriptions` (`id`),
  CONSTRAINT `deployed_sites_version_id_foreign` FOREIGN KEY (`version_id`) REFERENCES `versions` (`id`),
  CONSTRAINT `deployed_sites_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deployed_sites`
--

LOCK TABLES `deployed_sites` WRITE;
/*!40000 ALTER TABLE `deployed_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `deployed_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain_contacts`
--

DROP TABLE IF EXISTS `domain_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain_contacts_user_id_foreign` (`user_id`),
  CONSTRAINT `domain_contacts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_contacts`
--

LOCK TABLES `domain_contacts` WRITE;
/*!40000 ALTER TABLE `domain_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `domain_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain_registars`
--

DROP TABLE IF EXISTS `domain_registars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_registars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `library` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_registars`
--

LOCK TABLES `domain_registars` WRITE;
/*!40000 ALTER TABLE `domain_registars` DISABLE KEYS */;
INSERT INTO `domain_registars` VALUES (1,'namecheap','','2017-02-17 23:18:14','2017-02-17 23:18:14');
/*!40000 ALTER TABLE `domain_registars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domain_zones`
--

DROP TABLE IF EXISTS `domain_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_zones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_cost` decimal(10,2) NOT NULL,
  `prolongate_cost` decimal(10,2) NOT NULL,
  `domain_registar_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain_zones_domain_registar_id_foreign` (`domain_registar_id`),
  CONSTRAINT `domain_zones_domain_registar_id_foreign` FOREIGN KEY (`domain_registar_id`) REFERENCES `domain_registars` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_zones`
--

LOCK TABLES `domain_zones` WRITE;
/*!40000 ALTER TABLE `domain_zones` DISABLE KEYS */;
INSERT INTO `domain_zones` VALUES (1,'.dev',13.00,39.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(2,'.local',38.00,31.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(3,'.free',14.00,29.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(4,'.com',39.00,15.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(5,'.net',28.00,37.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(6,'.org',26.00,34.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(7,'.biz',36.00,12.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(8,'.info',16.00,19.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(9,'.co.uk',10.00,39.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(10,'.org.uk',13.00,12.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(11,'.me.uk',28.00,37.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(12,'.us',21.00,35.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(13,'.ca',15.00,36.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(14,'.eu',18.00,15.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(15,'.mobi',39.00,28.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14'),(16,'.me',23.00,31.00,1,'2017-02-17 23:18:14','2017-02-17 23:18:14');
/*!40000 ALTER TABLE `domain_zones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `managed` tinyint(1) NOT NULL DEFAULT '0',
  `domain_zone_id` int(10) unsigned DEFAULT NULL,
  `domain_registar_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `domain_contact_id` int(10) unsigned DEFAULT NULL,
  `is_registered` tinyint(1) NOT NULL DEFAULT '0',
  `domain_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domains_domain_zone_id_foreign` (`domain_zone_id`),
  KEY `domains_domain_registar_id_foreign` (`domain_registar_id`),
  KEY `domains_user_id_foreign` (`user_id`),
  KEY `domains_domain_contact_id_foreign` (`domain_contact_id`),
  CONSTRAINT `domains_domain_contact_id_foreign` FOREIGN KEY (`domain_contact_id`) REFERENCES `domain_contacts` (`id`),
  CONSTRAINT `domains_domain_registar_id_foreign` FOREIGN KEY (`domain_registar_id`) REFERENCES `domain_registars` (`id`),
  CONSTRAINT `domains_domain_zone_id_foreign` FOREIGN KEY (`domain_zone_id`) REFERENCES `domain_zones` (`id`),
  CONSTRAINT `domains_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domains`
--

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;
/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (1,'General questions','Consequatur itaque ut eius quos nam.?','Ipsam corporis quae officiis aut inventore ducimus..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'General questions','Aspernatur vel fugit natus.?','Consequuntur animi qui qui..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'General questions','Magni maiores nihil dolor quas error.?','Quam dolore iusto dolores earum rerum dolorum dicta ipsam..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,'General questions','Nam molestiae debitis unde.?','Quae provident quod illo maxime optio odio..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(5,'Sellers','Praesentium quo at mollitia labore.?','Non officia voluptas eveniet rerum..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(6,'Sellers','Nam aut qui fugiat ad facere vel.?','Iusto dolores qui quam asperiores est culpa et..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(7,'Sellers','Qui maxime beatae quia dolorem.?','Excepturi et eveniet porro..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(8,'Buyers','Soluta ea nisi corrupti quia omnis qui quis.?','Harum delectus aut ipsam velit..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(9,'Buyers','Voluptas est tenetur aliquid alias qui quia.?','Dolor mollitia architecto et et ullam..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(10,'Buyers','Quia culpa cum voluptatem rerum et rerum ad.?','Deleniti earum aut ullam et quae nulla..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(11,'Buyers','Molestias sint nulla aut qui sapiente.?','Molestias recusandae dolor mollitia..','2017-02-17 23:18:13','2017-02-17 23:18:13'),(12,'Buyers','Quos illo perferendis ab ut sunt sed unde.?','Modi eveniet cum autem ea non voluptatem sit..','2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `server` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `public_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `filesize` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_sizes`
--

DROP TABLE IF EXISTS `image_sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_sizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `public_url` text COLLATE utf8_unicode_ci NOT NULL,
  `local_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `server` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_sizes`
--

LOCK TABLES `image_sizes` WRITE;
/*!40000 ALTER TABLE `image_sizes` DISABLE KEYS */;
INSERT INTO `image_sizes` VALUES (1,'/storage/website/oor/xhq/h9m/z5h/uy5/pqz/i4d/ptp.png','website/oor/xhq/h9m/z5h/uy5/pqz/i4d/ptp.png','public',1920,1080,1607505,'2017-02-17 23:18:15','2017-02-17 23:18:15'),(2,'/storage/website/sui/y01/fkd/bu1/vwy/mfc/04g/rcx.png','website/sui/y01/fkd/bu1/vwy/mfc/04g/rcx.png','public',1200,600,789110,'2017-02-17 23:18:16','2017-02-17 23:18:16'),(3,'/storage/website/ixm/mtt/9sf/4yo/qzw/2sm/ovl/bvg.png','website/ixm/mtt/9sf/4yo/qzw/2sm/ovl/bvg.png','public',750,375,347837,'2017-02-17 23:18:17','2017-02-17 23:18:17'),(4,'/storage/website/cql/nof/rze/kzt/kzp/pi1/nho/1fp.png','website/cql/nof/rze/kzt/kzp/pi1/nho/1fp.png','public',250,125,49038,'2017-02-17 23:18:17','2017-02-17 23:18:17'),(5,'/storage/website/dav/xqy/boz/wrw/gn1/anu/dvo/esv.png','website/dav/xqy/boz/wrw/gn1/anu/dvo/esv.png','public',1920,1080,3976384,'2017-02-17 23:18:17','2017-02-17 23:18:17'),(6,'/storage/website/a9b/qjg/w7p/g6d/ejr/8uz/ufx/saj.png','website/a9b/qjg/w7p/g6d/ejr/8uz/ufx/saj.png','public',1200,600,1470246,'2017-02-17 23:18:19','2017-02-17 23:18:19'),(7,'/storage/website/wts/b8c/aja/hra/tnf/4p2/xxx/o1a.png','website/wts/b8c/aja/hra/tnf/4p2/xxx/o1a.png','public',750,375,604608,'2017-02-17 23:18:19','2017-02-17 23:18:19'),(8,'/storage/website/423/9bl/fez/jbr/iv7/hnq/h3j/fzk.png','website/423/9bl/fez/jbr/iv7/hnq/h3j/fzk.png','public',250,125,77603,'2017-02-17 23:18:19','2017-02-17 23:18:19'),(9,'/storage/website/8je/pwo/8m8/i8y/9zy/txq/lgk/8ie.png','website/8je/pwo/8m8/i8y/9zy/txq/lgk/8ie.png','public',1920,1080,3374395,'2017-02-17 23:18:20','2017-02-17 23:18:20'),(10,'/storage/website/9pf/kym/ogk/q8g/ict/hrw/f43/ncd.png','website/9pf/kym/ogk/q8g/ict/hrw/f43/ncd.png','public',1200,600,1305913,'2017-02-17 23:18:21','2017-02-17 23:18:21'),(11,'/storage/website/4by/pxe/uzo/6ox/tsm/ylt/s7p/yod.png','website/4by/pxe/uzo/6ox/tsm/ylt/s7p/yod.png','public',750,375,542581,'2017-02-17 23:18:22','2017-02-17 23:18:22'),(12,'/storage/website/v7b/1rd/ysa/izj/kpe/tn9/kae/iew.png','website/v7b/1rd/ysa/izj/kpe/tn9/kae/iew.png','public',250,125,68025,'2017-02-17 23:18:22','2017-02-17 23:18:22'),(13,'/storage/website/1i6/im6/dmd/pku/dmy/vsw/zfd/ug2.png','website/1i6/im6/dmd/pku/dmy/vsw/zfd/ug2.png','public',1920,1080,1922321,'2017-02-17 23:18:23','2017-02-17 23:18:23'),(14,'/storage/website/2ne/e4r/nni/qwg/xw5/qmx/oly/iga.png','website/2ne/e4r/nni/qwg/xw5/qmx/oly/iga.png','public',1200,600,884485,'2017-02-17 23:18:24','2017-02-17 23:18:24'),(15,'/storage/website/mug/uoj/1yb/wwi/tkz/saw/mux/pcx.png','website/mug/uoj/1yb/wwi/tkz/saw/mux/pcx.png','public',750,375,377081,'2017-02-17 23:18:24','2017-02-17 23:18:24'),(16,'/storage/website/no8/vxz/d2s/c8v/of4/fs8/rfw/ov4.png','website/no8/vxz/d2s/c8v/of4/fs8/rfw/ov4.png','public',250,125,50452,'2017-02-17 23:18:24','2017-02-17 23:18:24'),(17,'/storage/website/o7v/1q0/caj/jgb/dac/nsk/sku/pga.png','website/o7v/1q0/caj/jgb/dac/nsk/sku/pga.png','public',1920,1080,2157427,'2017-02-17 23:18:26','2017-02-17 23:18:26'),(18,'/storage/website/1yw/ffe/bll/ja1/u8w/xfx/utr/nlp.png','website/1yw/ffe/bll/ja1/u8w/xfx/utr/nlp.png','public',1200,600,996272,'2017-02-17 23:18:27','2017-02-17 23:18:27'),(19,'/storage/website/0kw/g5u/kev/n0f/nvk/m82/2mb/zbv.png','website/0kw/g5u/kev/n0f/nvk/m82/2mb/zbv.png','public',750,375,437410,'2017-02-17 23:18:27','2017-02-17 23:18:27'),(20,'/storage/website/6wo/4fk/ndu/cxk/erx/6ii/0v0/61z.png','website/6wo/4fk/ndu/cxk/erx/6ii/0v0/61z.png','public',250,125,65667,'2017-02-17 23:18:27','2017-02-17 23:18:27'),(21,'/storage/website/3vk/l3m/s2k/vtv/sun/upy/elu/hxx.png','website/3vk/l3m/s2k/vtv/sun/upy/elu/hxx.png','public',1920,1080,2067389,'2017-02-17 23:18:28','2017-02-17 23:18:28'),(22,'/storage/website/dmy/icv/sxb/oyv/o8u/pmb/qqr/8cu.png','website/dmy/icv/sxb/oyv/o8u/pmb/qqr/8cu.png','public',1200,600,984766,'2017-02-17 23:18:29','2017-02-17 23:18:29'),(23,'/storage/website/g8q/ad6/jxv/gfw/6lc/rlx/6ra/pys.png','website/g8q/ad6/jxv/gfw/6lc/rlx/6ra/pys.png','public',750,375,421077,'2017-02-17 23:18:29','2017-02-17 23:18:29'),(24,'/storage/website/kte/tif/hfe/q0k/w0i/ifj/fnm/zgs.png','website/kte/tif/hfe/q0k/w0i/ifj/fnm/zgs.png','public',250,125,62333,'2017-02-17 23:18:29','2017-02-17 23:18:29'),(25,'/storage/website/xc5/out/mdr/aer/adi/wel/uzz/5g7.png','website/xc5/out/mdr/aer/adi/wel/uzz/5g7.png','public',1920,1080,2071362,'2017-02-17 23:18:30','2017-02-17 23:18:30'),(26,'/storage/website/x8n/e5m/ryn/nsg/lsz/nis/of4/ijt.png','website/x8n/e5m/ryn/nsg/lsz/nis/of4/ijt.png','public',1200,600,935256,'2017-02-17 23:18:31','2017-02-17 23:18:31'),(27,'/storage/website/5my/rpm/orp/ta3/re1/ivu/6cw/cu0.png','website/5my/rpm/orp/ta3/re1/ivu/6cw/cu0.png','public',750,375,391553,'2017-02-17 23:18:31','2017-02-17 23:18:31'),(28,'/storage/website/qz0/yfj/u6a/dpj/o6o/tae/lft/nwb.png','website/qz0/yfj/u6a/dpj/o6o/tae/lft/nwb.png','public',250,125,50240,'2017-02-17 23:18:31','2017-02-17 23:18:31'),(29,'/storage/website/bam/qr6/v5y/0ev/1pm/wds/gea/6h9.png','website/bam/qr6/v5y/0ev/1pm/wds/gea/6h9.png','public',1920,1080,1847536,'2017-02-17 23:18:32','2017-02-17 23:18:32'),(30,'/storage/website/aee/ndu/usi/3bt/gbk/8jo/80m/051.png','website/aee/ndu/usi/3bt/gbk/8jo/80m/051.png','public',1200,600,817974,'2017-02-17 23:18:33','2017-02-17 23:18:33'),(31,'/storage/website/ghp/we9/icv/dig/pbg/gts/p8v/jza.png','website/ghp/we9/icv/dig/pbg/gts/p8v/jza.png','public',750,375,343946,'2017-02-17 23:18:34','2017-02-17 23:18:34'),(32,'/storage/website/jdt/oqw/fpn/7jf/yxr/n4k/vrl/tzq.png','website/jdt/oqw/fpn/7jf/yxr/n4k/vrl/tzq.png','public',250,125,46094,'2017-02-17 23:18:34','2017-02-17 23:18:34'),(33,'/storage/website/zjv/v0b/pym/glr/jil/llj/sez/jig.png','website/zjv/v0b/pym/glr/jil/llj/sez/jig.png','public',1920,1080,1698852,'2017-02-17 23:18:34','2017-02-17 23:18:34'),(34,'/storage/website/4rs/po6/nnn/mfu/oj3/qn9/kme/wui.png','website/4rs/po6/nnn/mfu/oj3/qn9/kme/wui.png','public',1200,600,815536,'2017-02-17 23:18:35','2017-02-17 23:18:35'),(35,'/storage/website/ydk/o7n/la0/o4c/qck/mxa/yg4/3ka.png','website/ydk/o7n/la0/o4c/qck/mxa/yg4/3ka.png','public',750,375,359398,'2017-02-17 23:18:36','2017-02-17 23:18:36'),(36,'/storage/website/wc2/kmx/qae/jet/b7s/cj1/fgx/stw.png','website/wc2/kmx/qae/jet/b7s/cj1/fgx/stw.png','public',250,125,50132,'2017-02-17 23:18:36','2017-02-17 23:18:36'),(37,'/storage/website/xaf/7es/pnm/tak/668/xgl/sxs/1r1.png','website/xaf/7es/pnm/tak/668/xgl/sxs/1r1.png','public',1920,1080,2375197,'2017-02-17 23:18:36','2017-02-17 23:18:36'),(38,'/storage/website/e6t/ec9/8h1/c6q/po8/j7t/4w8/zfx.png','website/e6t/ec9/8h1/c6q/po8/j7t/4w8/zfx.png','public',1200,600,1020438,'2017-02-17 23:18:38','2017-02-17 23:18:38'),(39,'/storage/website/pwl/97m/tnu/fwl/sno/b6v/kw7/9sb.png','website/pwl/97m/tnu/fwl/sno/b6v/kw7/9sb.png','public',750,375,436723,'2017-02-17 23:18:38','2017-02-17 23:18:38'),(40,'/storage/website/7mn/wrv/yup/bul/nsx/mxs/ssa/uns.png','website/7mn/wrv/yup/bul/nsx/mxs/ssa/uns.png','public',250,125,62852,'2017-02-17 23:18:38','2017-02-17 23:18:38'),(41,'/storage/website/i5e/oya/irr/5iq/ldd/03v/tai/wmz.png','website/i5e/oya/irr/5iq/ldd/03v/tai/wmz.png','public',1920,1080,1657244,'2017-02-17 23:18:39','2017-02-17 23:18:39'),(42,'/storage/website/zn5/ntz/fcf/few/gmq/80w/jr0/fqs.png','website/zn5/ntz/fcf/few/gmq/80w/jr0/fqs.png','public',1200,600,788094,'2017-02-17 23:18:40','2017-02-17 23:18:40'),(43,'/storage/website/niw/yfw/a8h/gcp/r1d/mqj/rfu/xcd.png','website/niw/yfw/a8h/gcp/r1d/mqj/rfu/xcd.png','public',750,375,363745,'2017-02-17 23:18:40','2017-02-17 23:18:40'),(44,'/storage/website/dvu/mnm/jyo/nic/7to/j7j/kha/yvk.png','website/dvu/mnm/jyo/nic/7to/j7j/kha/yvk.png','public',250,125,57424,'2017-02-17 23:18:40','2017-02-17 23:18:40'),(45,'/storage/website/sla/amx/w5u/u4r/vw6/kow/zjh/gl1.png','website/sla/amx/w5u/u4r/vw6/kow/zjh/gl1.png','public',1920,1080,1316270,'2017-02-17 23:18:41','2017-02-17 23:18:41'),(46,'/storage/website/7ft/pk4/vke/n9q/99e/rf1/fsd/yge.png','website/7ft/pk4/vke/n9q/99e/rf1/fsd/yge.png','public',1200,600,744681,'2017-02-17 23:18:42','2017-02-17 23:18:42'),(47,'/storage/website/8zg/exd/giy/ufv/com/4tc/mla/xu5.png','website/8zg/exd/giy/ufv/com/4tc/mla/xu5.png','public',750,375,323345,'2017-02-17 23:18:42','2017-02-17 23:18:42'),(48,'/storage/website/ogr/djb/x1i/wiv/sp8/y8w/fcm/ucl.png','website/ogr/djb/x1i/wiv/sp8/y8w/fcm/ucl.png','public',250,125,47452,'2017-02-17 23:18:42','2017-02-17 23:18:42'),(49,'/storage/website/361/ybm/cju/ypn/rw0/aqp/ylt/ofj.png','website/361/ybm/cju/ypn/rw0/aqp/ylt/ofj.png','public',1920,1080,2619659,'2017-02-17 23:18:43','2017-02-17 23:18:43'),(50,'/storage/website/lpu/tuz/fyo/gwm/cbp/hdc/xjs/6dq.png','website/lpu/tuz/fyo/gwm/cbp/hdc/xjs/6dq.png','public',1200,600,1089505,'2017-02-17 23:18:44','2017-02-17 23:18:44'),(51,'/storage/website/dyz/c7j/elf/vzx/sw6/cbv/o0d/bp1.png','website/dyz/c7j/elf/vzx/sw6/cbv/o0d/bp1.png','public',750,375,455061,'2017-02-17 23:18:44','2017-02-17 23:18:44'),(52,'/storage/website/cws/aox/yj5/tap/fqr/hnk/plv/wze.png','website/cws/aox/yj5/tap/fqr/hnk/plv/wze.png','public',250,125,60354,'2017-02-17 23:18:44','2017-02-17 23:18:44');
/*!40000 ALTER TABLE `image_sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imageable_id` int(10) unsigned NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_id` int(10) unsigned NOT NULL,
  `big_id` int(10) unsigned DEFAULT NULL,
  `medium_id` int(10) unsigned DEFAULT NULL,
  `small_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sort_index` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `images_original_id_unique` (`original_id`),
  UNIQUE KEY `images_big_id_unique` (`big_id`),
  UNIQUE KEY `images_medium_id_unique` (`medium_id`),
  UNIQUE KEY `images_small_id_unique` (`small_id`),
  KEY `images_imageable_id_imageable_type_index` (`imageable_id`,`imageable_type`),
  CONSTRAINT `images_big_id_foreign` FOREIGN KEY (`big_id`) REFERENCES `image_sizes` (`id`),
  CONSTRAINT `images_medium_id_foreign` FOREIGN KEY (`medium_id`) REFERENCES `image_sizes` (`id`),
  CONSTRAINT `images_original_id_foreign` FOREIGN KEY (`original_id`) REFERENCES `image_sizes` (`id`),
  CONSTRAINT `images_small_id_foreign` FOREIGN KEY (`small_id`) REFERENCES `image_sizes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,1,'App\\Website',1,2,3,4,'2017-02-17 23:18:17','2017-02-17 23:18:17',NULL,0),(2,1,'App\\Website',5,6,7,8,'2017-02-17 23:18:19','2017-02-17 23:18:19',NULL,0),(3,2,'App\\Website',9,10,11,12,'2017-02-17 23:18:22','2017-02-17 23:18:22',NULL,0),(4,2,'App\\Website',13,14,15,16,'2017-02-17 23:18:25','2017-02-17 23:18:25',NULL,0),(5,2,'App\\Website',17,18,19,20,'2017-02-17 23:18:27','2017-02-17 23:18:27',NULL,0),(6,3,'App\\Website',21,22,23,24,'2017-02-17 23:18:29','2017-02-17 23:18:29',NULL,0),(7,3,'App\\Website',25,26,27,28,'2017-02-17 23:18:31','2017-02-17 23:18:31',NULL,0),(8,3,'App\\Website',29,30,31,32,'2017-02-17 23:18:34','2017-02-17 23:18:34',NULL,0),(9,3,'App\\Website',33,34,35,36,'2017-02-17 23:18:36','2017-02-17 23:18:36',NULL,0),(10,4,'App\\Website',37,38,39,40,'2017-02-17 23:18:38','2017-02-17 23:18:38',NULL,0),(11,4,'App\\Website',41,42,43,44,'2017-02-17 23:18:40','2017-02-17 23:18:40',NULL,0),(12,4,'App\\Website',45,46,47,48,'2017-02-17 23:18:42','2017-02-17 23:18:42',NULL,0),(13,5,'App\\Website',49,50,51,52,'2017-02-17 23:18:44','2017-02-17 23:18:44',NULL,0);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_01_15_105324_create_roles_table',1),('2015_01_15_114412_create_role_user_table',1),('2015_01_26_115212_create_permissions_table',1),('2015_01_26_115523_create_permission_role_table',1),('2015_02_09_132439_create_permission_user_table',1),('2016_04_23_173929_create_transaction_categories_table',1),('2016_04_23_175112_create_transactions_table',1),('2016_04_23_175924_create_social_auths_table',1),('2016_04_23_185252_create_website_categories_table',1),('2016_04_23_185353_create_website_types_table',1),('2016_04_23_185456_create_website_technologies_table',1),('2016_04_23_185936_create_websites_table',1),('2016_04_23_190127_create_tags_table',1),('2016_04_23_190446_create_taggables_table',1),('2016_04_23_191027_create_ticket_priorities_table',1),('2016_04_23_191111_create_ticket_statuses_table',1),('2016_04_23_192157_create_subscriptions_table',1),('2016_04_23_192249_create_deployed_sites_table',1),('2016_04_23_192413_create_ticket_topics_table',1),('2016_04_23_192901_create_tickets_table',1),('2016_04_23_193332_create_ticket_messagess_table',1),('2016_04_23_193858_create_ticket_fees_table',1),('2016_04_23_194048_create_services_table',1),('2016_04_23_194508_create_website_reviews_table',1),('2016_04_25_113331_create_orders_table',1),('2016_04_25_113529_create_order_items_table',1),('2016_05_04_150812_create_image_sizes_table',1),('2016_05_04_151058_create_images_table',1),('2016_05_07_184531_add_slug_to_website_categories_table',1),('2016_05_08_154818_recreate_website_technologies_relation_table',1),('2016_05_08_155018_create_website_technology_website_pivot_table',1),('2016_05_12_174616_add_price_to_websites_table',1),('2016_05_17_171436_add_photo_to_users_table',1),('2016_05_19_150048_create_jobs_table',1),('2016_05_19_150123_create_failed_jobs_table',1),('2016_05_23_182322_create_shoppingcart_table',1),('2016_05_24_191943_update_parent_id_in_subsciptions_table',1),('2016_05_27_123125_create_tasks_table',1),('2016_05_29_072147_create_domain_registars_table',1),('2016_05_29_072413_create_domain_zones_table',1),('2016_05_29_072552_create_domains_table',1),('2016_05_29_091323_create_deployed_site_domains_table',1),('2016_05_29_091649_create_files_table',1),('2016_05_29_091946_create_versions_table',1),('2016_05_29_092406_create_site_hosts_table',1),('2016_05_29_093756_create_deployed_site_site_host_pivot_table',1),('2016_05_29_094051_add_published_at_to_websites_table',1),('2016_05_29_101914_add_sort_index_to_images_table',1),('2016_05_30_122333_create_referrals_table',1),('2016_06_08_130207_create_transactables_table',1),('2016_06_14_120114_create_website_admin_reviews_table',1),('2016_06_14_120348_add_published_at_and_status_to_websites_table',1),('2016_06_23_081213_add_price_and_commissions_to_website_types_table',1),('2016_06_23_081610_remove_prices_from_transaction_categories_table',1),('2016_06_23_081856_remove_prices_from_websites_table',1),('2016_06_28_184426_create_domain_contacts_table',1),('2016_06_30_025825_add_is_registred_to_domains_table',1),('2016_07_19_114600_create_payout_requests_table',1),('2016_07_20_111431_create_payout_transactions_table',1),('2016_07_21_122220_create_subscripts_table',1),('2016_07_29_100707_add_sales_to_website_types_table',1),('2016_08_08_090836_add_status_remove_domain_from_deployed_sites_table',1),('2016_08_10_115825_add_deployed_site_id_to_versions_table',1),('2016_08_12_063416_add_isp_credentials_to_deployed_site',1),('2016_08_12_170650_make_subscription_id_be_null_int_deployed_sites_table',1),('2016_08_14_120617_public_patch_can_be_nullable_in_files_table',1),('2016_08_14_120825_patch_file_id_can_be_nullable_in_files_table',1),('2016_09_01_074127_add_attachment_id_to_tickets_table',1),('2016_09_01_105051_create_departments_table',1),('2016_09_01_105317_add_department_id_to_users_table',1),('2016_09_01_110922_add_department_id_to_tickets_table',1),('2016_09_13_120016_delete_attachment_id_from_tickets_table',1),('2016_09_13_120056_add_attachment_id_to_ticket_messages_table',1),('2016_09_20_104437_change_agreed_to_status_in_ticket_fees_table',1),('2016_09_20_104600_add_is_custom_to_tickets_table',1),('2016_10_04_111818_add_website_id_to_tickets_table',1),('2016_10_04_121851_add_orderid_to_order_items_table',1),('2016_10_19_112848_add_payment_id_to_transtactions_table',1),('2016_10_19_114315_add_website_id_to_websites_table',1),('2016_10_19_125035_add_fields_to_tasks_table',1),('2016_10_20_120935_add_is_assignee_to_users_table',1),('2016_10_21_055010_add_deadline_to_tickets_table',1),('2016_10_24_122441_add_referral_id_to_referrals_table',1),('2016_10_24_125102_add_domain_id_to_domains_table',1),('2016_10_28_065618_create_notifications_table',1),('2016_10_28_073248_change_deadline_in_tickets_table',1),('2016_11_05_063849_add_google_credentials_to_users_table',1),('2016_11_15_134210_create_countries_table',1),('2016_11_15_135213_create_states_table',1),('2016_11_15_135315_create_cities_table',1),('2016_11_23_114745_create_additional_functions_table',1),('2016_11_25_083112_create__custom_websites_table',1),('2016_11_29_115352_add_user_id_to_customs_table',1),('2016_12_20_102026_add_domain_id_to_tickets_table',1),('2017_01_23_125307_add_is_banned_to_users_table',1),('2017_02_10_131954_create_faqs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) unsigned NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemable_id` int(10) unsigned NOT NULL,
  `itemable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `removable` tinyint(1) NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `orderid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_itemable_id_itemable_type_index` (`itemable_id`,`itemable_type`),
  KEY `order_items_order_id_foreign` (`order_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payout_requests`
--

DROP TABLE IF EXISTS `payout_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payout_requests_user_id_foreign` (`user_id`),
  CONSTRAINT `payout_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payout_requests`
--

LOCK TABLES `payout_requests` WRITE;
/*!40000 ALTER TABLE `payout_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `payout_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payout_transactions`
--

DROP TABLE IF EXISTS `payout_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payout_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payout_request_id` int(10) unsigned NOT NULL,
  `transaction_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payout_transactions_payout_request_id_foreign` (`payout_request_id`),
  KEY `payout_transactions_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `payout_transactions_payout_request_id_foreign` FOREIGN KEY (`payout_request_id`) REFERENCES `payout_requests` (`id`),
  CONSTRAINT `payout_transactions_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payout_transactions`
--

LOCK TABLES `payout_transactions` WRITE;
/*!40000 ALTER TABLE `payout_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `payout_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,6,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,2,4,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,1,4,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,3,2,'2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Panel','panel','Give access to admin panel',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'Websites','websites','Give access to websites',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'All websites','all_websites','Give access to all websites',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `premium_plans`
--

DROP TABLE IF EXISTS `premium_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `premium_plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `premium_plans`
--

LOCK TABLES `premium_plans` WRITE;
/*!40000 ALTER TABLE `premium_plans` DISABLE KEYS */;
/*!40000 ALTER TABLE `premium_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `referrals`
--

DROP TABLE IF EXISTS `referrals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referrals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `expire_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `referral_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `referrals_seller_id_foreign` (`seller_id`),
  KEY `referrals_user_id_foreign` (`user_id`),
  CONSTRAINT `referrals_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`),
  CONSTRAINT `referrals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `referrals`
--

LOCK TABLES `referrals` WRITE;
/*!40000 ALTER TABLE `referrals` DISABLE KEYS */;
/*!40000 ALTER TABLE `referrals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,1,2,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,3,3,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,4,4,'2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator','admin','Full featured user',5,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'Moderator','moderator','Content moderator',4,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'Buyer','buyer','Buyer',1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,'Developer','developer','Developer',2,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(5,'Sales Merchant','merchant','Sales Merchant',2,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(6,'Staff','staff','Staff, people who has access to panel',2,'2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingcart`
--

LOCK TABLES `shoppingcart` WRITE;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_hosts`
--

DROP TABLE IF EXISTS `site_hosts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_hosts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_hosts`
--

LOCK TABLES `site_hosts` WRITE;
/*!40000 ALTER TABLE `site_hosts` DISABLE KEYS */;
INSERT INTO `site_hosts` VALUES (1,'GoDaddy','102.102.102','root','root','2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `site_hosts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_auths`
--

DROP TABLE IF EXISTS `social_auths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_auths` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `social_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_auths_user_id_foreign` (`user_id`),
  CONSTRAINT `social_auths_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_auths`
--

LOCK TABLES `social_auths` WRITE;
/*!40000 ALTER TABLE `social_auths` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_auths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `states_country_id_foreign` (`country_id`),
  CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subscribable_id` int(10) unsigned NOT NULL,
  `subscribable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `begin_at` datetime NOT NULL,
  `end_at` datetime DEFAULT NULL,
  `duration` int(10) unsigned NOT NULL,
  `removable` tinyint(1) NOT NULL,
  `canceled` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_subscribable_id_subscribable_type_index` (`subscribable_id`,`subscribable_type`),
  KEY `subscriptions_parent_id_foreign` (`parent_id`),
  KEY `subscriptions_user_id_foreign` (`user_id`),
  CONSTRAINT `subscriptions_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `subscriptions` (`id`),
  CONSTRAINT `subscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriptions`
--

LOCK TABLES `subscriptions` WRITE;
/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggables`
--

DROP TABLE IF EXISTS `taggables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `taggables_tag_id_foreign` (`tag_id`),
  KEY `taggables_taggable_id_taggable_type_index` (`taggable_id`,`taggable_type`),
  CONSTRAINT `taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggables`
--

LOCK TABLES `taggables` WRITE;
/*!40000 ALTER TABLE `taggables` DISABLE KEYS */;
INSERT INTO `taggables` VALUES (1,8,1,'App\\Website',NULL,NULL),(2,27,1,'App\\Website',NULL,NULL),(3,29,1,'App\\Website',NULL,NULL),(4,54,1,'App\\Website',NULL,NULL),(5,16,2,'App\\Website',NULL,NULL),(6,60,2,'App\\Website',NULL,NULL),(7,9,3,'App\\Website',NULL,NULL),(8,11,3,'App\\Website',NULL,NULL),(9,56,3,'App\\Website',NULL,NULL),(10,63,3,'App\\Website',NULL,NULL),(11,10,4,'App\\Website',NULL,NULL),(12,48,4,'App\\Website',NULL,NULL),(13,27,5,'App\\Website',NULL,NULL),(14,29,5,'App\\Website',NULL,NULL),(15,52,5,'App\\Website',NULL,NULL);
/*!40000 ALTER TABLE `taggables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Community / Non Profit','2017-02-17 23:18:14','2017-02-17 23:18:14'),(2,'Wedding','2017-02-17 23:18:14','2017-02-17 23:18:14'),(3,'Travel & Lifestyle','2017-02-17 23:18:14','2017-02-17 23:18:14'),(4,'Hotels & Resorts','2017-02-17 23:18:14','2017-02-17 23:18:14'),(5,'Personal/Bio (CV, Actor, Personality, etc)','2017-02-17 23:18:14','2017-02-17 23:18:14'),(6,'Professional Services ( Legal, Consulting, Healthcare, etc )','2017-02-17 23:18:14','2017-02-17 23:18:14'),(7,'Photography','2017-02-17 23:18:14','2017-02-17 23:18:14'),(8,'Media & Entertainment','2017-02-17 23:18:14','2017-02-17 23:18:14'),(9,'Food & Drinks (Food blogs, Bar, Restaurant, etc)','2017-02-17 23:18:14','2017-02-17 23:18:14'),(10,'Fashion','2017-02-17 23:18:14','2017-02-17 23:18:14'),(11,'Health & Fitness','2017-02-17 23:18:14','2017-02-17 23:18:14'),(12,'Art & Design','2017-02-17 23:18:14','2017-02-17 23:18:14'),(13,'publishing & Editorial','2017-02-17 23:18:14','2017-02-17 23:18:14'),(14,'Architecture','2017-02-17 23:18:14','2017-02-17 23:18:14'),(15,'Kids & Toys','2017-02-17 23:18:14','2017-02-17 23:18:14'),(16,'Books','2017-02-17 23:18:14','2017-02-17 23:18:14'),(17,'Education','2017-02-17 23:18:14','2017-02-17 23:18:14'),(18,'Flowers','2017-02-17 23:18:14','2017-02-17 23:18:14'),(19,'Jewelry','2017-02-17 23:18:14','2017-02-17 23:18:14'),(20,'Church / Religious','2017-02-17 23:18:14','2017-02-17 23:18:14'),(21,'Money','2017-02-17 23:18:14','2017-02-17 23:18:14'),(22,'Wealth Management','2017-02-17 23:18:14','2017-02-17 23:18:14'),(23,'Escrow','2017-02-17 23:18:14','2017-02-17 23:18:14'),(24,'Advertising','2017-02-17 23:18:14','2017-02-17 23:18:14'),(25,'Blog','2017-02-17 23:18:14','2017-02-17 23:18:14'),(26,'Coach','2017-02-17 23:18:14','2017-02-17 23:18:14'),(27,'News','2017-02-17 23:18:14','2017-02-17 23:18:14'),(28,'Personal Page','2017-02-17 23:18:14','2017-02-17 23:18:14'),(29,'Life Coach ','2017-02-17 23:18:14','2017-02-17 23:18:14'),(30,'Video Blog','2017-02-17 23:18:14','2017-02-17 23:18:14'),(31,'Clergy','2017-02-17 23:18:14','2017-02-17 23:18:14'),(32,'Donations','2017-02-17 23:18:14','2017-02-17 23:18:14'),(33,'Events','2017-02-17 23:18:14','2017-02-17 23:18:14'),(34,'Handyman ','2017-02-17 23:18:14','2017-02-17 23:18:14'),(35,'Credit','2017-02-17 23:18:14','2017-02-17 23:18:14'),(36,'Credit Repair','2017-02-17 23:18:14','2017-02-17 23:18:14'),(37,'Construction','2017-02-17 23:18:14','2017-02-17 23:18:14'),(38,'Freelancer','2017-02-17 23:18:14','2017-02-17 23:18:14'),(39,'Home Maintenance','2017-02-17 23:18:14','2017-02-17 23:18:14'),(40,'Electrician','2017-02-17 23:18:14','2017-02-17 23:18:14'),(41,'Mechanic','2017-02-17 23:18:14','2017-02-17 23:18:14'),(42,'Painter','2017-02-17 23:18:14','2017-02-17 23:18:14'),(43,'Plumber','2017-02-17 23:18:14','2017-02-17 23:18:14'),(44,'Repair Main','2017-02-17 23:18:14','2017-02-17 23:18:14'),(45,'Magazine','2017-02-17 23:18:14','2017-02-17 23:18:14'),(46,'Newspaper','2017-02-17 23:18:14','2017-02-17 23:18:14'),(47,'Charity','2017-02-17 23:18:14','2017-02-17 23:18:14'),(48,'Non Profit','2017-02-17 23:18:14','2017-02-17 23:18:14'),(49,'One Page','2017-02-17 23:18:14','2017-02-17 23:18:14'),(50,'Pet','2017-02-17 23:18:14','2017-02-17 23:18:14'),(51,'Animal Care','2017-02-17 23:18:14','2017-02-17 23:18:14'),(52,'Online Learning','2017-02-17 23:18:14','2017-02-17 23:18:14'),(53,'Tutoring','2017-02-17 23:18:14','2017-02-17 23:18:14'),(54,'Training','2017-02-17 23:18:14','2017-02-17 23:18:14'),(55,'Spa','2017-02-17 23:18:14','2017-02-17 23:18:14'),(56,'Calendar','2017-02-17 23:18:14','2017-02-17 23:18:14'),(57,'Herbal','2017-02-17 23:18:14','2017-02-17 23:18:14'),(58,'Taxi (Cab /Car Hire)','2017-02-17 23:18:14','2017-02-17 23:18:14'),(59,'Tattoo (Tattoo Shop / Tattoo Studio )','2017-02-17 23:18:14','2017-02-17 23:18:14'),(60,'Video (Webcast / Podcast )','2017-02-17 23:18:14','2017-02-17 23:18:14'),(61,'Yacht','2017-02-17 23:18:14','2017-02-17 23:18:14'),(62,'Yellow Pages','2017-02-17 23:18:14','2017-02-17 23:18:14'),(63,'Yoga','2017-02-17 23:18:14','2017-02-17 23:18:14');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `taskable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `taskable_id` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completion_timeline` datetime DEFAULT NULL,
  `attachment_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_user_id_foreign` (`user_id`),
  KEY `tasks_attachment_id_foreign` (`attachment_id`),
  CONSTRAINT `tasks_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `files` (`id`),
  CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_fees`
--

DROP TABLE IF EXISTS `ticket_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_fees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `manager_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'waiting_for_reply',
  PRIMARY KEY (`id`),
  KEY `ticket_fees_ticket_id_foreign` (`ticket_id`),
  KEY `ticket_fees_manager_id_foreign` (`manager_id`),
  KEY `ticket_fees_customer_id_foreign` (`customer_id`),
  CONSTRAINT `ticket_fees_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `ticket_fees_manager_id_foreign` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`),
  CONSTRAINT `ticket_fees_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_fees`
--

LOCK TABLES `ticket_fees` WRITE;
/*!40000 ALTER TABLE `ticket_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_messages`
--

DROP TABLE IF EXISTS `ticket_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `ticket_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_messages_ticket_id_foreign` (`ticket_id`),
  KEY `ticket_messages_user_id_foreign` (`user_id`),
  KEY `ticket_messages_attachment_id_foreign` (`attachment_id`),
  CONSTRAINT `ticket_messages_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `files` (`id`),
  CONSTRAINT `ticket_messages_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`),
  CONSTRAINT `ticket_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_messages`
--

LOCK TABLES `ticket_messages` WRITE;
/*!40000 ALTER TABLE `ticket_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_priorities`
--

DROP TABLE IF EXISTS `ticket_priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_priorities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_priorities`
--

LOCK TABLES `ticket_priorities` WRITE;
/*!40000 ALTER TABLE `ticket_priorities` DISABLE KEYS */;
INSERT INTO `ticket_priorities` VALUES (1,'Low','2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'Normal','2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'High','2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,'Critical','2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `ticket_priorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_statuses`
--

DROP TABLE IF EXISTS `ticket_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_statuses`
--

LOCK TABLES `ticket_statuses` WRITE;
/*!40000 ALTER TABLE `ticket_statuses` DISABLE KEYS */;
INSERT INTO `ticket_statuses` VALUES (1,'new','New','2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'need_info','Additional information requested','2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'waiting_answer','Awaiting Response','2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,'closed','Closed','2017-02-17 23:18:13','2017-02-17 23:18:13'),(5,'reopened','Reopened','2017-02-17 23:18:13','2017-02-17 23:18:13'),(6,'waiting_fee_approve','Awaiting Approval','2017-02-17 23:18:13','2017-02-17 23:18:13'),(7,'in_process','In process','2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `ticket_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_topics`
--

DROP TABLE IF EXISTS `ticket_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_topics`
--

LOCK TABLES `ticket_topics` WRITE;
/*!40000 ALTER TABLE `ticket_topics` DISABLE KEYS */;
INSERT INTO `ticket_topics` VALUES (1,'I have problems with my account','2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'I need to make some corrections to my site','2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'Other questions','2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `ticket_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_topic_id` int(10) unsigned NOT NULL,
  `ticket_priority_id` int(10) unsigned NOT NULL,
  `ticket_status_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `assignee_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `department_id` int(10) unsigned DEFAULT NULL,
  `is_custom` tinyint(1) DEFAULT NULL,
  `website_id` int(10) unsigned DEFAULT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tickets_ticket_topic_id_foreign` (`ticket_topic_id`),
  KEY `tickets_ticket_priority_id_foreign` (`ticket_priority_id`),
  KEY `tickets_ticket_status_id_foreign` (`ticket_status_id`),
  KEY `tickets_user_id_foreign` (`user_id`),
  KEY `tickets_assignee_id_foreign` (`assignee_id`),
  KEY `tickets_department_id_foreign` (`department_id`),
  KEY `tickets_website_id_foreign` (`website_id`),
  KEY `tickets_domain_id_foreign` (`domain_id`),
  CONSTRAINT `tickets_assignee_id_foreign` FOREIGN KEY (`assignee_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tickets_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `tickets_domain_id_foreign` FOREIGN KEY (`domain_id`) REFERENCES `domains` (`id`),
  CONSTRAINT `tickets_ticket_priority_id_foreign` FOREIGN KEY (`ticket_priority_id`) REFERENCES `ticket_priorities` (`id`),
  CONSTRAINT `tickets_ticket_status_id_foreign` FOREIGN KEY (`ticket_status_id`) REFERENCES `ticket_statuses` (`id`),
  CONSTRAINT `tickets_ticket_topic_id_foreign` FOREIGN KEY (`ticket_topic_id`) REFERENCES `ticket_topics` (`id`),
  CONSTRAINT `tickets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tickets_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactables`
--

DROP TABLE IF EXISTS `transactables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `transactable_id` int(10) unsigned NOT NULL,
  `transactable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactables_transaction_id_foreign` (`transaction_id`),
  KEY `transactables_transactable_id_transactable_type_index` (`transactable_id`,`transactable_type`),
  CONSTRAINT `transactables_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactables`
--

LOCK TABLES `transactables` WRITE;
/*!40000 ALTER TABLE `transactables` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_categories`
--

DROP TABLE IF EXISTS `transaction_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direction` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_categories_code_name_unique` (`code_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_categories`
--

LOCK TABLES `transaction_categories` WRITE;
/*!40000 ALTER TABLE `transaction_categories` DISABLE KEYS */;
INSERT INTO `transaction_categories` VALUES (1,'Payment for order','order-income',1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(2,'Charge for order','order-outcome',-1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(3,'Payment for domain','domain-income',1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(4,'Charge for domain','domain-outcome',-1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(5,'Payment for website','website-income',1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(6,'Charge for website','website-outcome',-1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(7,'Commission fee','commission',1,'2017-02-17 23:18:13','2017-02-17 23:18:13'),(8,'Withdrawal','withdrawal',-1,'2017-02-17 23:18:13','2017-02-17 23:18:13');
/*!40000 ALTER TABLE `transaction_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,2) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `gate_info` text COLLATE utf8_unicode_ci,
  `initial` tinyint(1) NOT NULL DEFAULT '0',
  `transaction_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `failed` tinyint(1) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `pay_till` datetime DEFAULT NULL,
  `tries` int(11) NOT NULL DEFAULT '0',
  `next_try` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_transaction_category_id_foreign` (`transaction_category_id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  CONSTRAINT `transactions_transaction_category_id_foreign` FOREIGN KEY (`transaction_category_id`) REFERENCES `transaction_categories` (`id`),
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `second_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `hash` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` int(10) unsigned DEFAULT NULL,
  `is_assignee` tinyint(1) NOT NULL DEFAULT '0',
  `google_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_enabled` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `google_emergency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_department_id_foreign` (`department_id`),
  CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Webspoons','Admin',NULL,'admin@webspoons.com','$2y$10$jsKu0bEmIqcW5adXdPjCVO0wayhrxxABhQUeikxvBBIECNDhcr0g2',1,NULL,NULL,NULL,NULL,NULL,0.00,NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13',NULL,NULL,0,NULL,'0',NULL,0),(2,'John','Admin',NULL,'admin1@test.com','$2y$10$tkWhoY83aDpQJGFBRgCNMeNHSwfE7IwOiGH9etc3wUGBvgPWsAgrW',1,NULL,NULL,NULL,NULL,NULL,0.00,NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13',NULL,NULL,0,NULL,'0',NULL,0),(3,'John','Buyer',NULL,'admin2@test.com','$2y$10$qs3ZEQlVf40rOkKmCHnsO.HQBJT1TNOH8V3pnAz98eNFk/Tzaddm.',1,NULL,NULL,NULL,NULL,NULL,0.00,'PvUCiYwh0iowUBBLpXOsYduC9q1SgNE2bSdg83gZ8IKuKy8l1On4VmInZ9og','2017-02-17 23:18:13','2017-02-20 14:58:22',NULL,NULL,0,NULL,'0',NULL,0),(4,'John','Developer',NULL,'admin3@test.com','$2y$10$FkRayLagZL882nN4dIBAYutKLvFbjfz2pzdsTmt5nXX98EejrMCYS',1,NULL,NULL,NULL,NULL,NULL,0.00,NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13',NULL,NULL,0,NULL,'0',NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `change_log` text COLLATE utf8_unicode_ci NOT NULL,
  `full_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patch_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_file_id` int(10) unsigned NOT NULL,
  `patch_file_id` int(10) unsigned DEFAULT NULL,
  `website_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `versions_full_file_id_foreign` (`full_file_id`),
  KEY `versions_patch_file_id_foreign` (`patch_file_id`),
  KEY `versions_website_id_foreign` (`website_id`),
  CONSTRAINT `versions_full_file_id_foreign` FOREIGN KEY (`full_file_id`) REFERENCES `files` (`id`),
  CONSTRAINT `versions_patch_file_id_foreign` FOREIGN KEY (`patch_file_id`) REFERENCES `files` (`id`),
  CONSTRAINT `versions_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_admin_reviews`
--

DROP TABLE IF EXISTS `website_admin_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_admin_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(10) unsigned NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_admin_reviews_website_id_foreign` (`website_id`),
  CONSTRAINT `website_admin_reviews_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_admin_reviews`
--

LOCK TABLES `website_admin_reviews` WRITE;
/*!40000 ALTER TABLE `website_admin_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `website_admin_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_categories`
--

DROP TABLE IF EXISTS `website_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `website_categories_slug_unique` (`slug`),
  KEY `website_categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `website_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `website_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_categories`
--

LOCK TABLES `website_categories` WRITE;
/*!40000 ALTER TABLE `website_categories` DISABLE KEYS */;
INSERT INTO `website_categories` VALUES (1,'Restaurants & Bars',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','restaurants-bars'),(2,'Pets & Animal Care',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','pets-animal-care'),(3,'Sports & fitness',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','sports-fitness'),(4,'Hotel',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','hotel'),(5,'Lawyers',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','lawyers'),(6,'Florist',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','florist'),(7,'Fashion',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','fashion'),(8,'Hair & Beauty',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','hair-beauty'),(9,'Plumbers',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','plumbers'),(10,'Painters',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','painters'),(11,'Real Estate',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','real-estate'),(12,'Lawn care',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','lawn-care'),(13,'Auto Services',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','auto-services'),(14,'Accountants',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','accountants'),(15,'Insurance',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','insurance'),(16,'Arts & Culture',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','arts-culture'),(17,'Education & Books',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','education-books'),(18,'Business Services',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','business-services'),(19,'Ecommerce',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','ecommerce'),(20,'Medical & HealthCare',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','medical-healthcare'),(21,'Holiday & Travel',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','holiday-travel'),(22,'Photography & Design',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','photography-design'),(23,'Churches',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','churches'),(24,'General contractors',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','general-contractors'),(25,'Consultants & Professionals',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','consultants-professionals'),(26,'Entertainment',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','entertainment'),(27,'Events',NULL,'2017-02-17 23:18:13','2017-02-17 23:18:13','events');
/*!40000 ALTER TABLE `website_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_reviews`
--

DROP TABLE IF EXISTS `website_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_reviews_website_id_foreign` (`website_id`),
  KEY `website_reviews_user_id_foreign` (`user_id`),
  CONSTRAINT `website_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `website_reviews_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_reviews`
--

LOCK TABLES `website_reviews` WRITE;
/*!40000 ALTER TABLE `website_reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `website_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_technologies`
--

DROP TABLE IF EXISTS `website_technologies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_technologies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_technologies`
--

LOCK TABLES `website_technologies` WRITE;
/*!40000 ALTER TABLE `website_technologies` DISABLE KEYS */;
INSERT INTO `website_technologies` VALUES (1,'animi','2017-02-17 23:18:14','2017-02-17 23:18:14'),(2,'et','2017-02-17 23:18:14','2017-02-17 23:18:14'),(3,'et','2017-02-17 23:18:14','2017-02-17 23:18:14'),(4,'rerum','2017-02-17 23:18:14','2017-02-17 23:18:14'),(5,'dolores','2017-02-17 23:18:14','2017-02-17 23:18:14');
/*!40000 ALTER TABLE `website_technologies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_technology_website`
--

DROP TABLE IF EXISTS `website_technology_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_technology_website` (
  `website_technology_id` int(10) unsigned NOT NULL,
  `website_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`website_technology_id`,`website_id`),
  KEY `website_technology_website_website_technology_id_index` (`website_technology_id`),
  KEY `website_technology_website_website_id_index` (`website_id`),
  CONSTRAINT `website_technology_website_website_id_foreign` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`) ON DELETE CASCADE,
  CONSTRAINT `website_technology_website_website_technology_id_foreign` FOREIGN KEY (`website_technology_id`) REFERENCES `website_technologies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_technology_website`
--

LOCK TABLES `website_technology_website` WRITE;
/*!40000 ALTER TABLE `website_technology_website` DISABLE KEYS */;
INSERT INTO `website_technology_website` VALUES (1,1),(1,3),(2,1),(2,2),(2,4),(2,5),(3,3),(3,4),(3,5),(4,1),(4,2),(4,3),(4,4),(4,5),(5,1);
/*!40000 ALTER TABLE `website_technology_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_types`
--

DROP TABLE IF EXISTS `website_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` decimal(14,2) NOT NULL DEFAULT '0.00',
  `merchant_fee` decimal(14,2) NOT NULL DEFAULT '0.00',
  `developer_fee` decimal(14,2) NOT NULL DEFAULT '0.00',
  `sale_6_months` int(11) NOT NULL DEFAULT '0',
  `sale_12_months` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_types`
--

LOCK TABLES `website_types` WRITE;
/*!40000 ALTER TABLE `website_types` DISABLE KEYS */;
INSERT INTO `website_types` VALUES (1,'Personal',NULL,NULL,2000.00,500.00,500.00,0,0),(2,'Bussines',NULL,NULL,5000.00,1000.00,1000.00,0,0),(3,'E-commerce',NULL,NULL,10000.00,2000.00,2000.00,0,0);
/*!40000 ALTER TABLE `website_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites`
--

DROP TABLE IF EXISTS `websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `websites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `brief` text COLLATE utf8_unicode_ci NOT NULL,
  `demo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website_type_id` int(10) unsigned NOT NULL,
  `website_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'req_review',
  `website_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `websites_website_type_id_foreign` (`website_type_id`),
  KEY `websites_website_category_id_foreign` (`website_category_id`),
  KEY `websites_user_id_foreign` (`user_id`),
  CONSTRAINT `websites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `websites_website_category_id_foreign` FOREIGN KEY (`website_category_id`) REFERENCES `website_categories` (`id`),
  CONSTRAINT `websites_website_type_id_foreign` FOREIGN KEY (`website_type_id`) REFERENCES `website_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites`
--

LOCK TABLES `websites` WRITE;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;
INSERT INTO `websites` VALUES (1,'commodi voluptates maiores ullam','In ullam occaecati voluptas vel. Ea provident sit cum voluptatem. Ea sunt quia magnam libero illo eveniet cupiditate. Et blanditiis ratione corrupti ut non aut. Maxime eum iure consequatur illum praesentium excepturi perferendis.\n\nCommodi quod cupiditate voluptatibus. Accusantium inventore eos sit rerum qui. Molestiae adipisci sit quia. Et sit temporibus enim facilis provident.\n\nPlaceat quam dignissimos officiis vel beatae consequatur placeat. Voluptate placeat provident porro autem aut similique et. Dolor dolorum natus quis nostrum. Optio alias ea unde ratione voluptas. Corporis rerum repudiandae vel recusandae nesciunt quos.\n\nCulpa dolor modi doloremque quia omnis fugit repellendus. Suscipit molestias optio vel alias quia autem. Ut doloremque in nostrum ut nostrum perferendis nemo. Aut veniam quod modi dicta.\n\nEst quibusdam voluptas hic quis. Sit aspernatur neque quod occaecati omnis blanditiis deleniti. Temporibus mollitia voluptatibus id ad.\n\nNemo iusto deserunt asperiores impedit corrupti. Quam soluta expedita quo iste. Soluta reiciendis laboriosam facere minima. Magnam quam est deserunt quasi quasi repellendus. Facere laboriosam consequatur et harum quis.\n\nQuis sunt a et dolores. Voluptatibus voluptas ad provident explicabo. A sint quos molestias modi asperiores.\n\nMolestias sint aspernatur dolores omnis amet. Reiciendis eos in doloremque perferendis voluptatibus praesentium. Sequi doloremque quia provident quam nulla in cumque. Explicabo mollitia dolores qui sed.\n\nEos illum voluptatem aut animi. Et quaerat quas deserunt laboriosam. Vel ducimus sed totam non.\n\nQui nam illum perferendis sunt totam. Consequatur accusamus voluptate magni alias id. Adipisci nihil rem qui ratione perferendis minima nihil temporibus. Ut ut omnis fugit esse inventore.','Culpa architecto dicta vel animi. Sequi nesciunt sunt ut mollitia.\n\nReprehenderit sapiente consectetur voluptatem. Dolor rerum necessitatibus cupiditate sunt suscipit officiis itaque omnis. Facilis odit quibusdam a aut repellat voluptas velit.','https://www.bing.com/search?q=sit dicta qui','commodi-voluptates-maiores-ullam',2,25,2,'2017-02-17 23:18:14','2017-02-17 23:18:14','2017-02-17 18:18:14','active',NULL),(2,'hic occaecati assumenda dignissimos voluptas consequatur voluptatem','Eos magni officia qui dicta et ipsa amet. Sit beatae veritatis at. Earum aliquam eum optio nihil at.\n\nSint quibusdam possimus dolores sequi tempora. Aut laborum dolores tempora nobis. Et velit veniam neque et commodi voluptatem. Qui voluptates quas omnis quia deleniti ut ratione.\n\nItaque unde non earum laboriosam quam. Culpa omnis nemo vero. Reprehenderit ad vitae illum aliquid fugit fugiat. Aspernatur nihil et qui dolores corrupti laboriosam ullam.\n\nHarum et nemo eos in amet suscipit provident natus. Et consequatur et adipisci dolorem possimus repellendus dolores. Autem accusamus quia maiores assumenda eum. Nisi commodi eos ex ducimus.\n\nConsectetur placeat consequatur et vero fugit saepe occaecati ullam. A sint rem laborum. Molestiae exercitationem soluta porro veniam temporibus cupiditate. Et accusantium quam quo fuga ex cupiditate.\n\nDolorem ut delectus et quis consectetur est laboriosam accusamus. Praesentium recusandae sunt et. Et consequatur quo maiores dolorum temporibus placeat ipsa vel.\n\nOfficiis vero molestias porro est. Quia quis voluptates aut occaecati. Laboriosam maxime occaecati porro sint.\n\nVoluptatem accusantium enim beatae aspernatur exercitationem. Ut dolores qui dicta eveniet in sunt. Et dicta est dolorem beatae.\n\nNumquam et officiis molestiae omnis eum vel. Officia doloribus temporibus inventore corporis molestiae. Commodi neque est sit nulla laborum nesciunt aut. Et commodi ducimus numquam voluptas dolor enim.\n\nOmnis soluta nobis vero quia et eveniet cum. Cum mollitia ut et in enim temporibus eligendi. Neque tenetur nemo voluptatem laboriosam qui. Sed alias ipsam aspernatur et sint.','Aspernatur aut et molestias autem suscipit provident enim culpa. Eum illum in sit cum vitae maiores. Soluta et eveniet similique nisi asperiores et.\n\nPlaceat sequi vitae est amet numquam et ad. Repudiandae nihil eligendi aliquid perferendis sed accusantium. Error temporibus quis nulla fugit.','https://www.bing.com/search?q=repudiandae dolorem molestiae sit eum','hic-occaecati-assumenda-dignissimos-voluptas-consequatur-voluptatem',1,8,1,'2017-02-17 23:18:14','2017-02-17 23:18:14','2017-02-17 18:18:14','active',NULL),(3,'quis ipsam ea consectetur','Alias est cupiditate id illum eum. Cumque dolor et a molestias illo aliquam. Inventore qui et distinctio quae. Recusandae blanditiis temporibus earum quod quas neque aut.\n\nDistinctio non sit iure consequuntur cupiditate. Laboriosam ratione laudantium quibusdam doloremque velit quisquam veniam. Dolorem omnis mollitia et iure. Rerum officia commodi architecto est impedit voluptates est.\n\nHarum enim repellat culpa. Expedita unde officia magni exercitationem qui consequatur. Officiis molestias et optio.\n\nAut consequatur eos earum. Ut qui quo mollitia a atque minima. Aut aut voluptatem consequatur numquam quos rerum fuga molestias. Architecto consequatur voluptatum harum vel quisquam in et nihil.\n\nTempore et quaerat enim excepturi. Rerum tempore odio non tenetur assumenda et molestias. Laboriosam cum et doloribus iste.\n\nDoloribus et unde et fugiat porro velit. Minus excepturi molestias nobis praesentium dolore nesciunt. Est laboriosam dignissimos ea aut quia perspiciatis pariatur. Eligendi recusandae eveniet delectus consequatur. Aut eos quidem voluptatem dolor esse praesentium.\n\nAliquid dolores repudiandae at impedit dolores illum nam. Unde velit odio totam magnam quia. Dolorum quaerat dolor dolorem odio.\n\nRepudiandae eveniet eos similique. Quod distinctio ex omnis est labore quaerat impedit. Quo ratione architecto fugit velit. Aut voluptate dicta iure magnam molestias officia distinctio enim.\n\nAut ratione voluptatum voluptas aliquid nisi quia exercitationem. Suscipit earum dolores deleniti amet consectetur deleniti. Nostrum ducimus et repellat aspernatur. Ea voluptatum repellendus unde voluptas rerum sequi.\n\nOmnis minus est quibusdam tenetur exercitationem dolore repellat amet. Aut qui est cupiditate odio vero accusantium. Quas tempore eveniet quam tenetur natus.','Animi consequuntur nemo commodi et aut est. Autem quia quibusdam libero eum. Molestiae ea quia maxime maiores.\n\nEos nobis voluptas sit quam expedita a temporibus. Est omnis dolore quae ipsam. Labore ea sed tempore ducimus ipsa architecto labore. Ut optio dolorem dicta et voluptates doloribus et.','https://www.bing.com/search?q=nihil quos cumque','quis-ipsam-ea-consectetur',1,22,1,'2017-02-17 23:18:14','2017-02-17 23:18:14','2017-02-17 18:18:14','active',NULL),(4,'quia numquam vitae','Omnis eum cupiditate quo eaque. Itaque illum sit quisquam maiores fugit. Mollitia voluptatum vero id temporibus aut magni laudantium.\n\nEsse beatae ducimus sint nesciunt vitae id. Dignissimos vero officia porro nihil officia id. Laudantium qui numquam aliquid dolor. Odit et iure quos dolore quo.\n\nQui et qui sint dolores. Velit nesciunt rerum tenetur rem dolor veniam aliquid. Sit est voluptatem suscipit.\n\nOmnis enim adipisci minima harum. Quia illo unde earum sint voluptatem enim facilis. Deleniti sint voluptas nisi aut. Nesciunt quo est non rem.\n\nQuibusdam dolorem iure amet facere earum quis quis officiis. Et cupiditate nam pariatur quos ad vitae ut quidem.\n\nConsequatur fugiat quibusdam id quos impedit. Consequatur fuga illo mollitia non earum ut dignissimos. Omnis ipsa non inventore.\n\nNihil iste sint sapiente. Rerum voluptatem voluptate voluptas blanditiis ad. Doloremque nesciunt accusantium dolorum dignissimos distinctio veritatis.\n\nUt et dicta asperiores quisquam nobis voluptatem aut. In rem odio fugit voluptatem. Ratione porro non recusandae. Consectetur et labore sapiente quia.\n\nUnde et sint reprehenderit voluptas minus. Possimus aut nisi fugit saepe est accusamus eum. Suscipit repellat excepturi reiciendis nobis sed. Suscipit dicta minima voluptas qui minima.\n\nDoloribus et tempore aut quisquam ea. Sed maiores autem ab quas quibusdam tenetur. Voluptate illum dicta aut qui culpa. Explicabo quasi beatae et maxime quibusdam. Sit explicabo quod vero nam iure.','Laudantium et et expedita dolores cumque magni optio laboriosam. Dolores enim velit modi rem. Earum quo natus in itaque repudiandae fuga. Aspernatur quas ut odit doloremque voluptatem ex non rerum. Deleniti sed quia eaque nesciunt quibusdam id illo nesciunt.\n\nAtque et autem dolor temporibus. Enim corporis molestias eius nihil aut voluptatum et suscipit. Error modi repellendus perferendis ipsum ut. Ipsum eaque aut qui ratione porro voluptas.','https://www.bing.com/search?q=distinctio qui quo quis id optio','quia-numquam-vitae',2,9,2,'2017-02-17 23:18:14','2017-02-17 23:18:14','2017-02-17 18:18:14','active',NULL),(5,'ratione earum tempora sunt exercitationem','Voluptatibus pariatur laborum quia magni. Delectus ullam ex in magnam aspernatur qui. Similique laudantium numquam animi. Voluptatem deserunt quasi fugit aut aperiam ut.\n\nDolore sed reprehenderit dolore consectetur sequi. Corporis dicta occaecati autem natus odio officia minima. Veniam nam autem nihil vel nam libero in sint. Qui dicta consequatur praesentium et neque porro ut.\n\nExplicabo beatae qui a nobis. Veritatis unde facere deserunt nobis et. Totam minima quis rerum perferendis. Molestiae reprehenderit autem facilis accusamus adipisci.\n\nVoluptates porro fugit reprehenderit dolores eum dignissimos. Est praesentium amet dolorum debitis molestias magni eligendi repudiandae. Saepe consectetur iusto dolorem est repellendus rerum. Et consequatur aliquam delectus provident. Esse sunt exercitationem perferendis fugit eaque et.\n\nRepellendus aut laborum dolorum aspernatur amet. Ut ipsa provident et beatae dolores. Quaerat perferendis porro sit excepturi modi facilis in. Rerum saepe magnam tempora a ut.\n\nTenetur inventore architecto veniam quia cumque tempore veniam. Voluptatem vel nihil odio perspiciatis. Totam labore ipsam molestiae recusandae ut voluptas qui.\n\nSit tempore ducimus non rerum et quibusdam vitae. Dolorum nulla animi quis id magnam. Deserunt a consectetur aut reprehenderit eum excepturi. Sunt exercitationem dignissimos vel asperiores deserunt maiores.\n\nDolores quas illum a id qui quaerat voluptatem. Rerum est praesentium fugit sequi. Tempora quaerat esse consequatur vitae fugiat reiciendis.\n\nEligendi optio id earum laudantium ut. Dolor molestiae sunt et voluptates non rerum odio.\n\nSunt accusamus sit quaerat fugit. Et et autem molestias voluptas. Ducimus repellat dicta doloribus incidunt facilis. Incidunt voluptates enim quibusdam incidunt ex. Repellat nobis eum minima ea.','Ut ex assumenda sit blanditiis voluptatem. Exercitationem numquam id hic quos. Pariatur quia consequatur repudiandae maiores perspiciatis sint ab ut.\n\nAd dolor saepe est aut. Iure deserunt sit suscipit sunt sapiente est. Amet qui ab sit delectus. Aperiam incidunt omnis cumque eligendi natus magni placeat.','https://www.bing.com/search?q=delectus molestias aut cum quo','ratione-earum-tempora-sunt-exercitationem',2,16,4,'2017-02-17 23:18:14','2017-02-17 23:18:14','2017-02-17 18:18:14','active',NULL);
/*!40000 ALTER TABLE `websites` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-22  1:05:21
