<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 1000,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<div id='#accuracystr'></div>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Exchange Name</td>
			<td>Upticks Volume</td>
			<td>Coinmarketcap Volume</td>
			<td>Accurcy</td>
			<td>Comment</td>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($data['exchangevolume'] as $val)
			{
				$accuracy = 0;
				if($val->coinmarketcap_volume>0 && $val->exchange_volume >0){
					$accuracy = (((($val->exchange_volume)*$data['btcusd'])/$val->coinmarketcap_volume)-1)*100;
					if($accuracy<0){
						$accuracy = (-1)*$accuracy;
					}
					$accuracy = 100-$accuracy;
				}
				
				$i++;
				echo '<tr>
					<td>'.$i.'</td>
					<td><a href="https://api.upticks.io/exchange/details/'.$val->exchange_id.'" target="_blank" style="color:#60abc7;">'.$val->exchange_name.'<a/></td>
					<td>'.number_format(($val->exchange_volume)*$data['btcusd']).'</td>
					<td>'.number_format($val->coinmarketcap_volume).'</td>
					<td>'.number_format($accuracy).'</td>
					<td> -- </td>
				</tr>';
			}
		?>
	</tbody>
</table>

</body>
</html>
