<?php 
//prd($data);

?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<div id='#accuracystr'></div>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Name</td>
			<td>Update Time</td>
			<td>Price</td>
			<td>Volume</td>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$prevprice = 0;
			foreach($data['pricelist'] as $val)
			{
				$i++;
				if($prevprice == $val->price){
					continue;
				}
				$prevprice = $val->price;
				echo '<tr>
				<td>'.$i.'</td>
				<td>'.$val->symbol.'</td>
				<td>'.$val->dtd_time.'</td>
				<td>'.$val->price.'</td>
				<td>'.$val->volume.'</td>
				</tr>';			
			}
			
		?>	
	</tbody>
</table>
</body>
</html>
