<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({"pageLength": 5000});
} );
</script>
<body>
<h1>All Exchanges Data</h1><?php echo apimenu();?>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>Market ID</td>
		<td>Exchange Name</td>
		<td>Convert Type</td>
		<td>Pair</td>
		<td>Currency 1</td>
		<td>Currency 2</td>
		<td> **** </td>
		<td style="text-align:right;">Price</td>
		<td style="text-align:right;">Volume</td>
		<td style="text-align:right;">Price USD</td>
		<td style="text-align:right;">Price BTC</td>
		<td style="text-align:right;">Price KRW</td>
		
		<td style="text-align:right;">Volume USD</td>
		<td style="text-align:right;">Volume BTC</td>
		<td style="text-align:right;">Volume KRW</td>
		
	</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($data as $val)
			{
				//echo '<pre>';print_r($val->price_btc);exit;
				$url = '<a href="/exchange/converttype/'.$val->exchanges_id.'/0" target="_blank">'.$val->convert_type.'</a>';
				$i++;
				echo '<tr>
				<td>'.$val->market_id.'</td>
				<td>'.$val->exchange_name.'</td>
				<td>'.(($val->convert_type == 0)?$url:'').'</td>				
				<td>'.$val->market_name.'</td>
				<td>'.$val->symbol1.'</td>
				<td>'.$val->symbol2.'</td>
				<td>'.$val->symbol1.' to '.$val->symbol2.'<br>'.$val->symbol1.' to USD </td>
				<td style="text-align:right;">'.number_format($val->price,18).'</td>
				<td style="text-align:right;">'.(($val->bigvolume == 0)?number_format($val->volume,8):number_format($val->bigvolume,8)).'</td>
				<td style="text-align:right;">'.number_format($val->price_usd,18).'</td>
				<td style="text-align:right;">'.number_format($val->price_btc,18).'</td>
				<td style="text-align:right;">'.number_format($val->price_krw,18).'</td>
				<td style="text-align:right;">'.number_format($val->bigvolume_usd,2).'</td>
				<td style="text-align:right;">'.number_format($val->bigvolume_btc,2).'</td>
				<td style="text-align:right;">'.number_format($val->bigvolume_krw,2).'</td>
				</tr>';
			}
		?>	
	<tbody>
</table>


</body>
</html>

