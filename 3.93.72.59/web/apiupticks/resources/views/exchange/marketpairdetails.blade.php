<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>

<body>
<?php echo apimenu();?>
<br>
Pair :: <?php echo $data['marketname'];?>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Exchange Name</td>
			<td>Market Pair</td>
			<td>BTC Price</td>
			<td>USD Price</td>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$btcusd = btcusd();
			foreach($data['details'] as $val)
			{
				$i++;
				echo '<tr><td>'.$i.'</td>
				<td><a href="/exchange/details/'.$val->exchange_id.'" target="_blank" style="color:#00F;">'.$data['exchange'][$val->exchange_id]->exchange_name.'</td><td>'.$val->marketname.'</td>
				<td>'.number_format($val->price_btc,8).'</td>
				<td>'.number_format($btcusd*$val->price_btc,2).'</td>
				</tr>';
			}
		?>	
	</tbody>
</table>


<script>
$(document).ready(function() {
	
} );
</script>
</body>
</html>
