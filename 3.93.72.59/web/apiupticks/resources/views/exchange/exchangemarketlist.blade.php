<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Market Pair</td>
			<td style="text-align:right;">Avg. BTC Price</td>
			<td style="text-align:right;">Avg. USD Price</td>
			<td>--</td>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$btcusd = btcusd();
			foreach($data['allmarket'] as $val)
			{
				$i++;
				echo '<tr><td>'.$i.'</td>
				<td><a href="/marketpair/details/'.str_replace('/','_',$val->marketname).'" target="_blank" style="color:#00F;">'.$val->marketname.'</td>
				<td style="text-align:right;">'.number_format($val->avgprice_btc,8).'</td>
				<td style="text-align:right;">'.number_format($btcusd*$val->avgprice_btc,2).'</td>
				<td>---</td>
				</tr>';
			}
		?>	
	</tbody>
</table>


<script>
$(document).ready(function() {
	
} );
</script>
</body>
</html>
