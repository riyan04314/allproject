<?php 
	//echo '<pre>';print_r($data['exchanges_arr']);exit;
	session_start();
	$developer = 0;
	if(!empty($_SESSION['developer']) && $_SESSION['developer'] == 1){
		$developer = 1;
	}
	if(!empty($_REQUEST['developer'])){
		if($_REQUEST['developer'] == 1){
			$developer = 1;
			$_SESSION['developer'] = 1;
		}else{
			$developer = 2;
			$_SESSION['developer'] = 2;
		}				
	}
	
	$top = 0;
	if(!empty($_REQUEST['top'])){
		$top = 1;
	}
	$updname = array();
	$developer = 1;
	//echo $developer;exit;
?>
<?php

?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 1000,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<div id='#accuracystr'></div>
<br>
<?php if(FALSE){?>
<table border="1">
	
	<tbody>
		<?php
			$i = 0;
			$activeno = 0;
			$btcusd = btcusd();
			$decimalplaces	= 4;
			$fouentyfour_volume = '24h_volume_usd';
			//prd($data['coinprice']);
			$time = time();
			$coinstr = '';
			
			$accuracyarr['99_5'] = array();
			$accuracyarr['99_0'] = array();
			$accuracyarr['98_5'] = array();
			$accuracyarr['98_0'] = array();
			$accuracyarr['97_0'] = array();
			$accuracyarr['95_0'] = array();
			$accuracyarr['90_0'] = array();
			$accuracyarr['85_0'] = array();
			$accuracyarr['80_0'] = array();
			$accuracyarr['10_0'] = array();
			
			$colorarr['99_5'] = '';
			$colorarr['99_0'] = '';
			$colorarr['98_5'] = '';
			$colorarr['98_0'] = ' background-color:#d6fbf8; ';
			$colorarr['97_0'] = ' background-color:#f3e42e; ';
			$colorarr['95_0'] = ' background-color:#e0acbf; ';
			
			$colorarr['90_0'] = ' background-color:#fd86b2; ';
			$colorarr['85_0'] = ' background-color:#fd86b2; ';
			$colorarr['80_0'] = ' background-color:#fd86b2; ';
			$colorarr['10_0'] = ' background-color:#fd86b2; ';
			
			foreach($data['coinprice'] as $val)
			{
				
				/*if(!empty($_REQUEST['makeactive']) && $_REQUEST['makeactive'] == 1 && $activeno<10){
					if($val->approved == 1){continue;}
					//if($price_accuracy < 98){continue;}		
					//$activeno++;
					//include('https://api.upticks.io/coinstatus/'.$val->id);
				}*/
				
				$symbol = strtoupper($val->symbol);
				if(in_array($symbol,array('USD','XBB','QASH'))){
					continue;
				}
				$diff 					= 0;
				$price_accuracy 		= 0;
				$volume_accuracy		= 0;
				$coinmarketcap_price 	= 0;
				$coinmarketcap_volume 	= 0;
				$coinprice 				= $val->price;
				$coinvolume 			= $val->volume;	
				$hr24change = 0;


				$coinmarketcap_price 	= $val->coinmarketcapprice;
				$coinmarketcap_volume 	= $val->coinmarketcapvolume;

				if(!empty($_REQUEST['top']) && $_REQUEST['top']<101)
				{									
					if(!empty($data['coinmarketcap'][$symbol]))
					{
						$coinmarketcap_price 	= $data['coinmarketcap'][$symbol]->price_usd;
						$coinmarketcap_volume 	= $data['coinmarketcap'][$symbol]->$fouentyfour_volume;
					}				
					else{
						if($top){
							continue;
						}
					}
				} /* */
				
				if(!empty($_REQUEST['top']) && $_REQUEST['top']<$i-1){
					break;
				}
				

				if($coinmarketcap_price>0){
					$price_accuracy 	= (($coinprice/$coinmarketcap_price)-1)*100;
					if($price_accuracy<0){
						$price_accuracy = 100+$price_accuracy;
					}
					else{
						$price_accuracy = 100-$price_accuracy;
					}
				}
				if($coinmarketcap_volume>0){
					$volume_accuracy 	= 100-(($coinprice/$coinmarketcap_volume)-1)*100;
					if($volume_accuracy<0){
						$volume_accuracy = 100+$volume_accuracy;
					}
					else{
						$volume_accuracy = 100-$volume_accuracy;
					}
				}

				
				
				$coinstr .= '\''.strtoupper($symbol).'\',';
				
				if($top){
					if($price_accuracy == 0){
						continue;
					}
				}
				$i++;
				$t = round(($time-strtotime($val->price_update_time))/60,0);
				$decimalplaces = 4;
				if($coinprice<=0.001){
					$decimalplaces = 6;
				}
				if($coinprice<=0.0001){
					$decimalplaces = 8;
				}
				if($coinprice<=0.00000001){
					continue;
				}
				
				$hr01change = 0;
				$hr12change = 0;
				
				
						/*$val->t00010 = 0;
						$val->t00020 = 0;
						$val->t00030 = 0;
						$val->t00040 = 0;
						$val->t00050 = 0;
						$val->t00060 = 0;
						$val->t00180 = 0;
						$val->t00360 = 0;
						$val->t00720 = 0;	
						$val->t01440 = 0;*/			
				
				
				
				
				
				if($val->t00060>0){
					$hr01change = number_format((($val->price/$val->t00060)-1)*100,8);
				}
				if($val->t00720){
					$hr12change = number_format((($val->price/$val->t00720)-1)*100,8);
				}/**/
				if($val->t01440){
					$hr24change = number_format((($val->price/$val->t01440)-1)*100,8);
					$hr24change = number_format((($val->price-$val->t01440)/$val->t01440)*100,8);
				}/**/
				
				$volume_accuracy = 0;
				if(!empty($coinmarketcap_volume) && $coinmarketcap_volume >0){
					$volume_accuracy = (($coinvolume/$coinmarketcap_volume)-1)*100;
				}	
				if($volume_accuracy<0){
					$volume_accuracy = $volume_accuracy*(-1);
				}

				
				$color = '';
				if($price_accuracy>=99.5){
					$accuracyarr['99_5'][] = $symbol;
					$color = $colorarr['99_5'];
				}
				elseif($price_accuracy>=99){
					$accuracyarr['99_0'][] = $symbol;
					$color = $colorarr['99_0'];
				}
				elseif($price_accuracy>=98.5){
					$accuracyarr['98_5'][] = $symbol;
					$color = $colorarr['98_5'];
				}
				elseif($price_accuracy>=98){
					$accuracyarr['98_0'][] = $symbol;
					$color = $colorarr['98_0'];
				}
				elseif($price_accuracy>=97){
					$accuracyarr['97_0'][] = $symbol;
					$color = $colorarr['97_0'];
				}
				elseif($price_accuracy>=95){
					$accuracyarr['95_0'][] = $symbol;
					$color = $colorarr['95_0'];
				}
				elseif($price_accuracy>=90){
					$accuracyarr['90_0'][] = $symbol;
					$color = $colorarr['90_0'];
				}
				elseif($price_accuracy>=85){
					$accuracyarr['85_0'][] = $symbol;
					$color = $colorarr['85_0'];
				}
				elseif($price_accuracy>=80){
					$accuracyarr['80_0'][] = $symbol;
					$color = $colorarr['80_0'];
				}
				else{
					$accuracyarr['10_0'][] = $symbol;
					$color = $colorarr['10_0'];
				}
				
				
				$xx = 0;
				$name	= trim($val->name1);				
				$name1 	= explode(' ',$name);
				if(isset($name1[0]) && isset($name1[1]) && strlen($name1[0]) && strlen($name1[1]) && $symbol == $name1[0] ){
					$xx = 1;
					//prd($name1);
					$name 	= substr($name,strlen($name1[0]));
					$name 	= trim($name);
					
					$updname[$i]['id']		= $val->id;
					$updname[$i]['name']	= $name;
					$updname[$i]['oldname']	= $val->name1;
					$updname[$i]['symbol']	= $name1[0];
				}	

				
				if(!empty($_REQUEST['makeactive']) && $_REQUEST['makeactive'] == 1){
					if($val->approved == 1){continue;}
					if($price_accuracy < 98){continue;}		
					$activeno++;
				
					//include('https://api.upticks.io/coinstatus/'.$val->id);
					if($activeno>9){
						//exit;
					}
				}/**/
				//continue;
				
				//<td>'.(($xx == 1)?'<a href="/editcoin/'.$val->id.'/'.trim($val->name).'/5" target="_blank">Ok</a>':'').'</td>
				echo '<tr style=""><td>'.strtoupper($symbol).'</td>
				<td><table>';
				$top_10exchangesvolume 	= explode(',',$val->top_10exchangesvolume);
				$top_10exchangesid 		= explode(',',$val->top_10exchangesid);
				$i = -1;
				foreach($top_10exchangesid as $exchange){
					$i++;
					if($i>6){
						break;
					}
					$exchange_vol = 0;
					if(!empty($top_10exchangesvolume[$i])){
						$exchange_vol  	= $top_10exchangesvolume[$i];
						$exchange_vol	= $btcusd*$exchange_vol;
					}
					if($exchange_vol <1 ){
						continue;
					}
					echo '<tr><td>'.$data['exchanges_arr'][$exchange].'</td><td>'.number_format($exchange_vol).'</td></tr>';
					
				}
				echo '</table></td>
				</tr>';
			}
			exit;
		?>	
	</tbody>
</table>
<br>
<?php } ?>

<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td rowspan="2">SL.</td>
			<td rowspan="2">Icon</td>
			<td rowspan="2">Symbol</td>
			<td rowspan="2">Name</td>
			<td rowspan="2">Status</td>
			<td rowspan="2">Top Exchange</td>
			<td rowspan="2" style="text-align:right;">Last Update<br>(Min. Ago.)</td>
			<td rowspan="2" style="text-align:right;">Upticks Price(USD)</td>
			<td rowspan="2" style="text-align:right;">Coinmarketcap Price(USD)</td>
			<td rowspan="2" style="text-align:right;">Accuracy(%)</td>
			<td rowspan="2" style="text-align:right;">Upticks USD Volume</td>
			<td rowspan="2" style="text-align:right;">Coinmarketcap USD Volume</td><?php /**/?>
			<td rowspan="2" style="text-align:right;">Volume Accuracy</td><?php /**/?>
			<td rowspan="2" style="text-align:right;">Details</td>
			<td rowspan="2" style="text-align:right;">History</td>
			<td rowspan="2">Details</td>
			<td colspan="4" text-align="center">Growth</td>
			<td colspan="15" text-align="center">History Price</td>
		</tr>
		<tr>
			<td style="text-align:right;">1 Hr.</td>
			<td style="text-align:right;">12 Hr.</td>
			<td style="text-align:right;">24 Hr.</td>
			<td style="text-align:right;">7 Day</td>
			
			<td style="text-align:right;">10 Min.</td>
			<td style="text-align:right;">20 Min.</td>
			<td style="text-align:right;">30 Min.</td>
			<td style="text-align:right;">40 Min.</td>
			<td style="text-align:right;">50 Min.</td>
			<td style="text-align:right;">1 Hr.</td>
			<td style="text-align:right;">3 Hr.</td>
			<td style="text-align:right;">6 Hr.</td>
			<td style="text-align:right;">9 Hr.</td>
			<td style="text-align:right;">12 Hr.</td>
			<td style="text-align:right;">15 Hr.</td>
			<td style="text-align:right;">18 Hr.</td>
			<td style="text-align:right;">21 Hr.</td>
			<td style="text-align:right;">24 Hr.</td>
			<td style="text-align:right;">27 Hr.</td>
		</tr>
	</thead>
	
	<tbody>
		<?php
			$i = 0;
			$activeno = 0;
			$btcusd = btcusd();
			$decimalplaces	= 4;
			$fouentyfour_volume = '24h_volume_usd';
			//prd($data['coinprice']);
			$time = time();
			$coinstr = '';
			
			$accuracyarr['99_5'] = array();
			$accuracyarr['99_0'] = array();
			$accuracyarr['98_5'] = array();
			$accuracyarr['98_0'] = array();
			$accuracyarr['97_0'] = array();
			$accuracyarr['95_0'] = array();
			$accuracyarr['90_0'] = array();
			$accuracyarr['85_0'] = array();
			$accuracyarr['80_0'] = array();
			$accuracyarr['10_0'] = array();
			
			$colorarr['99_5'] = '';
			$colorarr['99_0'] = '';
			$colorarr['98_5'] = '';
			$colorarr['98_0'] = ' background-color:#d6fbf8; ';
			$colorarr['97_0'] = ' background-color:#f3e42e; ';
			$colorarr['95_0'] = ' background-color:#e0acbf; ';
			
			$colorarr['90_0'] = ' background-color:#fd86b2; ';
			$colorarr['85_0'] = ' background-color:#fd86b2; ';
			$colorarr['80_0'] = ' background-color:#fd86b2; ';
			$colorarr['10_0'] = ' background-color:#fd86b2; ';
			
			foreach($data['coinprice'] as $val)
			{
				
				/*if(!empty($_REQUEST['makeactive']) && $_REQUEST['makeactive'] == 1 && $activeno<10){
					if($val->approved == 1){continue;}
					//if($price_accuracy < 98){continue;}		
					//$activeno++;
					//include('https://api.upticks.io/coinstatus/'.$val->id);
				}*/
				
				$symbol = strtoupper($val->symbol);
				if(in_array($symbol,array('USD','XBB','QASH'))){
					continue;
				}
				$diff 					= 0;
				$price_accuracy 		= 0;
				$volume_accuracy		= 0;
				$coinmarketcap_price 	= 0;
				$coinmarketcap_volume 	= 0;
				$coinprice 				= $val->price;
				$coinvolume 			= $val->volume;	
				$hr24change = 0;


				$coinmarketcap_price 	= $val->coinmarketcapprice;
				$coinmarketcap_volume 	= $val->coinmarketcapvolume;

				if(!empty($_REQUEST['top']) && $_REQUEST['top']<101)
				{									
					if(!empty($data['coinmarketcap'][$symbol]))
					{
						$coinmarketcap_price 	= $data['coinmarketcap'][$symbol]->price_usd;
						$coinmarketcap_volume 	= $data['coinmarketcap'][$symbol]->$fouentyfour_volume;
					}				
					else{
						if($top){
							continue;
						}
					}
				} /* */
				
				if(!empty($_REQUEST['top']) && $_REQUEST['top']<$i-1){
					break;
				}
				

				if($coinmarketcap_price>0){
					$price_accuracy 	= (($coinprice/$coinmarketcap_price)-1)*100;
					if($price_accuracy<0){
						$price_accuracy = 100+$price_accuracy;
					}
					else{
						$price_accuracy = 100-$price_accuracy;
					}
				}
				if($coinmarketcap_volume>0){
					$volume_accuracy 	= 100-(($coinprice/$coinmarketcap_volume)-1)*100;
					if($volume_accuracy<0){
						$volume_accuracy = 100+$volume_accuracy;
					}
					else{
						$volume_accuracy = 100-$volume_accuracy;
					}
				}

				
				
				$coinstr .= '\''.strtoupper($symbol).'\',';
				
				if($top){
					if($price_accuracy == 0){
						continue;
					}
				}
				$i++;
				$t = round(($time-strtotime($val->price_update_time))/60,0);
				$decimalplaces = 4;
				if($coinprice<=0.001){
					$decimalplaces = 6;
				}
				if($coinprice<=0.0001){
					$decimalplaces = 8;
				}
				if($coinprice<=0.00000001){
					continue;
				}
				
				$hr01change = 0;
				$hr12change = 0;
				
				
						/*$val->t00010 = 0;
						$val->t00020 = 0;
						$val->t00030 = 0;
						$val->t00040 = 0;
						$val->t00050 = 0;
						$val->t00060 = 0;
						$val->t00180 = 0;
						$val->t00360 = 0;
						$val->t00720 = 0;	
						$val->t01440 = 0;*/			
				
				
				
				
				
				if($val->t00060>0){
					$hr01change = number_format((($val->price/$val->t00060)-1)*100,8);
				}
				if($val->t00720){
					$hr12change = number_format((($val->price/$val->t00720)-1)*100,8);
				}/**/
				if($val->t01440){
					$hr24change = number_format((($val->price/$val->t01440)-1)*100,8);
					$hr24change = number_format((($val->price-$val->t01440)/$val->t01440)*100,8);
				}/**/
				
				$volume_accuracy = 0;
				if(!empty($coinmarketcap_volume) && $coinmarketcap_volume >0){
					$volume_accuracy = (($coinvolume/$coinmarketcap_volume)-1)*100;
				}	
				if($volume_accuracy<0){
					$volume_accuracy = $volume_accuracy*(-1);
				}

				
				$color = '';
				if($price_accuracy>=99.5){
					$accuracyarr['99_5'][] = $symbol;
					$color = $colorarr['99_5'];
				}
				elseif($price_accuracy>=99){
					$accuracyarr['99_0'][] = $symbol;
					$color = $colorarr['99_0'];
				}
				elseif($price_accuracy>=98.5){
					$accuracyarr['98_5'][] = $symbol;
					$color = $colorarr['98_5'];
				}
				elseif($price_accuracy>=98){
					$accuracyarr['98_0'][] = $symbol;
					$color = $colorarr['98_0'];
				}
				elseif($price_accuracy>=97){
					$accuracyarr['97_0'][] = $symbol;
					$color = $colorarr['97_0'];
				}
				elseif($price_accuracy>=95){
					$accuracyarr['95_0'][] = $symbol;
					$color = $colorarr['95_0'];
				}
				elseif($price_accuracy>=90){
					$accuracyarr['90_0'][] = $symbol;
					$color = $colorarr['90_0'];
				}
				elseif($price_accuracy>=85){
					$accuracyarr['85_0'][] = $symbol;
					$color = $colorarr['85_0'];
				}
				elseif($price_accuracy>=80){
					$accuracyarr['80_0'][] = $symbol;
					$color = $colorarr['80_0'];
				}
				else{
					$accuracyarr['10_0'][] = $symbol;
					$color = $colorarr['10_0'];
				}
				
				
				$xx = 0;
				$name	= trim($val->name1);				
				$name1 	= explode(' ',$name);
				if(isset($name1[0]) && isset($name1[1]) && strlen($name1[0]) && strlen($name1[1]) && $symbol == $name1[0] ){
					$xx = 1;
					//prd($name1);
					$name 	= substr($name,strlen($name1[0]));
					$name 	= trim($name);
					
					$updname[$i]['id']		= $val->id;
					$updname[$i]['name']	= $name;
					$updname[$i]['oldname']	= $val->name1;
					$updname[$i]['symbol']	= $name1[0];
				}	

				
				if(!empty($_REQUEST['makeactive']) && $_REQUEST['makeactive'] == 1){
					if($val->approved == 1){continue;}
					if($price_accuracy < 98){continue;}		
					$activeno++;
				
					//include('https://api.upticks.io/coinstatus/'.$val->id);
					if($activeno>9){
						//exit;
					}
				}/**/
				//continue;
				
				//<td>'.(($xx == 1)?'<a href="/editcoin/'.$val->id.'/'.trim($val->name).'/5" target="_blank">Ok</a>':'').'</td>
				echo '<tr style=""><td>'.$i.'</td>
				<td><img src="https://cdn.upticks.io/img/coins/24x24/'.str_replace(' ','-',strtolower($val->name)).'.png" width="24" /></td>
				<td>'.strtoupper($symbol).'</td>
				<td><a href="/editcoin/'.$val->id.'/'.trim($val->name).'/1" target="_blank" style="color:#4CAF50;font-size:larger;">'.$val->name.'</a></td>
				<td style="text-align:right;"><span id="coinstatus_'.$val->id.'" onclick="coinstatus('.$val->id.')" style="cursor:pointer;color:#4CAF50;font-size:larger;">'.(($val->approved == 1)?'Active':'Inactive').'</span></td>
				<td style="text-align:left;"><ol>';
				$top_10exchangesvolume 	= explode(',',$val->top_10exchangesvolume);
				$top_10exchangesid 		= explode(',',$val->top_10exchangesid);
				$i = -1;
				foreach($top_10exchangesid as $exchange){
					$i++;
					if($i>6){
						break;
					}
					$exchange_vol = 0;
					if(!empty($top_10exchangesvolume[$i])){
						$exchange_vol  = $top_10exchangesvolume[$i];
					}
					echo '<li><a href="http://api.upticks.io/exchange/details/'.$exchange.'?coin='.$val->name.'" style="color:#5058b7; cursor:pointer" target="_blank">'.$data['exchanges_arr'][$exchange].'</a>
					<a href="http://api.upticks.io/coindata/'.$exchange.'" style="color:#5058b7; cursor:pointer" target="_blank">($'.number_format($exchange_vol).')</a></li>';
					/**/
				}
				echo '</ol></td>
				<td style="text-align:right;">'.$t.' </td>
				<td style="text-align:right;">'.numberformat($coinprice,$decimalplaces).' </td>';
				echo '<td style="text-align:right;">'.numberformat($coinmarketcap_price,$decimalplaces).' </td>';			
				echo '<td style="text-align:right;">'.number_format($price_accuracy,4).'</td>';
				echo '<td style="text-align:right;">'.number_format($coinvolume,0).'</td>';
				echo '<td style="text-align:right;">'.number_format($coinmarketcap_volume,0).'</td>';/**/					
				echo '<td style="text-align:right;">'.number_format($volume_accuracy,2).'%</td>';
				echo '<td style="text-align:center;">
				<a href="/coin/exchange/price/'.$val->symbol.'" target="_blank" style="color:#4c95d0;">Price</a> &nbsp; &nbsp;
				</td><td style="text-align:center;">
				<a href="/coinhistorypricelist/'.$val->symbol.'" target="_blank" style="color:#4c95d0;">History</a>
				</td>';
				
				
				$day7percent = 0;
				if(isset($data['oldprice'][$val->symbol])){
					$day7percent 	= (($coinprice/$data['oldprice'][$val->symbol])-1)*100;
				}
				
				echo '<td><a href="/coin/exchange/volume/'.$val->symbol.'" target="_blank" style="color:#4c95d0;">Volume</a></td>
						<td style="text-align:right;">'.$hr01change.'%</td>
						<td style="text-align:right;">'.$hr12change.'%</td>
						<td style="text-align:right;">'.$hr24change.'%</td>
						<td style="text-align:right;">'.$day7percent.'</td>
						<td style="text-align:right;">'.number_format($val->t00010,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00020,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00030,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00040,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00050,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00060,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00180,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00360,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00540,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00720,8).'</td>
						<td style="text-align:right;">'.number_format($val->t00900,8).'</td>
						<td style="text-align:right;">'.number_format($val->t01080,8).'</td>
						<td style="text-align:right;">'.number_format($val->t01260,8).'</td>
						<td style="text-align:right;">'.number_format($val->t01440,8).'</td>
						<td style="text-align:right;">'.number_format($val->t01610,8).'</td>
						
				</tr>';
			}
			
		?>	
	</tbody>

</table>
<?php
	$str  = '<hr><h4>Current Time :: '.date('d/m/Y h:i A').' :: </h4><hr>';
	foreach($accuracyarr as $key=>$val){
		$str .= '<div style="'.$colorarr[$key].'; float:left;">Accuracy : '.str_replace('_','.',$key).'% : CNT : '.count($val).' : : </div>';
		$str .= '<div style="float:left1;">';
		foreach($val as $val1){
			$str .= '<a href="/coin/exchange/price/'.$val1.'" target="_blank">'.$val1.'</a>, &nbsp;';;
		}
		$str .= '</div><div class="clearfix"><br></div>';
		$str .= '<hr>';
	}
	echo $str;
?>
<?php
//prd($updname);
//echo $coinstr;
?>
<script>

coinstatus = function(){
	var id  = arguments[0];
	var url = "https://api.upticks.io/coinstatus/"+id;
	$('#coinstatus_'+id).html('Wait');
	$.ajax({
		type: "GET",
		url: url,
		data:'',
		success: function(res){
			if(res == 1){
				$('#coinstatus_'+id).html('Active');
			}else{
				$('#coinstatus_'+id).html('Inactive');
			}
		}
	});
}

$(document).ready(function() {


} );
</script>
</body>
</html>
