<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<div id='#accuracystr'></div>
<br>
<a href="https://api.upticks.io/exchange-volume-accuracy" target="_blank">Test</a>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<th>SL.</th>
			<th>Rank</th>
			<th>Exchange Name</th>
			<th style="text-align:right;">Accuracy</th>
			<th style="text-align:right;">Upticks Volume</th>
			<th style="text-align:right;">Coinmarketcap Volume</th>
			<th style="text-align:right;">New Market List</th>
			<th width="300">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$accarr['91_100'] = array();
			$accarr['81_90'] = array();
			$accarr['71_80'] = array();
			$accarr['51_70'] = array();
			$accarr['0_50'] = array();
			////////////////////////////////////////////////
			$bgcolor['91_100'] = ' background-color:#a7dec7;';
			$bgcolor['81_90'] = ' background-color:#a7dec7;';
			$bgcolor['71_80'] = ' background-color:#a7c9e6;';
			$bgcolor['51_70'] = ' background-color:#e6c7e1;';
			$bgcolor['0_50'] = ' background-color:#ffdce2;';
			$btcusd = btcusd();
			foreach($data as $val)
			{
				$val->exchange_volume = $btcusd*$val->exchange_volume;
				//prd($val);
				$acuracy = 0;
				$color = '';
				if($val->coinmarketcap_volume>0){
					if($val->exchange_volume>$val->coinmarketcap_volume){
						$acuracy = (($val->exchange_volume/$val->coinmarketcap_volume)-1)*100;
					}else{
						$acuracy = (1-($val->exchange_volume/$val->coinmarketcap_volume))*100;
					}
					$acuracy	= 100-$acuracy;
					
					if($acuracy<51){
						$color = ' background-color:#ffdce2;';
						$accarr['0_50'][] = $val->exchange_name;
					}elseif($acuracy<71){
						$color = ' background-color:#e6c7e1;';
						$accarr['51_70'][] = $val->exchange_name;
					}elseif($acuracy<81){
						$color = ' background-color:#a7c9e6;';
						$accarr['71_80'][] = $val->exchange_name;
					}elseif($acuracy<91){
						$accarr['81_90'][] = $val->exchange_name;
					}else{
						$accarr['91_100'][] = $val->exchange_name;
					}
					$acuracy	= number_format($acuracy);
				}
				$i++;
				echo '<tr style="'.$color.'"><td>'.$i.'</td>
				<td>'.$val->coinmarketcap_rank.' </td>
				<td><a href="/exchange/details/'.$val->id.'" target="_blank" style="color:#9a3005">'.$val->exchange_name.'</a></td>
				<td style="text-align:right;">'.$acuracy.'% </td>
				<td style="text-align:right;">'.number_format($val->exchange_volume).' </td>
				<td style="text-align:right;">'.number_format($val->coinmarketcap_volume).' </td>
				<td style="text-align:right;" >';
				//echo '<a href="/insertmarket?exchange_id='.$val->id.'" target="_blank">xyz</a> &nbsp; &nbsp;';
				echo '<span style="cursor:pointer" onclick="newmarketlist('.$val->id.')">Show New Market</span></td>
				<td id="exchangeid_'.$val->id.'" style="display:none1;"></td>
				</tr>';
			}
		?>	
	</tbody>
</table>
<br><br>
<?php
echo '<h3>Accuracy status of exchange compare to Coinmarketcap : '.date('d-m-Y H:i:s').'</h3>';
foreach($accarr as $key=>$val){
	echo '<div style="'.$bgcolor[$key].'">Accuracy :: '.str_replace('_',' to ',$key).'% : Count :: '.count($accarr[$key]).' &nbsp; &nbsp;<br>';
	foreach($val as $val1){
		echo $val1.', &nbsp;';
	}
	echo '</div><hr><br>';
}
?>
<script>
$(document).ready(function() {
	
} );
newmarketlist = function(){
	var exchange_id = arguments[0];
	var url = "https://api.upticks.io/insertmarket?exchange_id="+exchange_id;
	$('#exchangeid_'+exchange_id).html('Work in process');
	$.ajax({
		url: url, 
		contentType: "application/json",
		success: function (result) {
			$('#exchangeid_'+exchange_id).html(result);
		}
	});	
}
addallmarket = function(){
	var exchange_id = arguments[0];
	var url = "insertmarket?exchange_id="+exchange_id+"&insertmarket=55";
	//$('#exchangeid_'+exchange_id).html('Work in process');
	$.ajax({
		url: url, 
		contentType: "application/json",
		success: function (result) {
			$('#exchangeid_'+exchange_id).html(result);
		}
	});	
}
addmarket = function(){
	var exchange_id	= arguments[0];
	var market_name	= arguments[1];
	var url = 'insertmarket?exchange_id='+exchange_id+'&insertmarket='+market_name;
	$('#addmarket_'+exchange_id+'_'+market_name).html('Wait..');
	$.ajax({
		url: url, 
		contentType: "application/json",
		success: function (result) {
			$('#addmarket_'+exchange_id+'_'+market_name).html('Market added successfully!');
		}
	});	
}

addallcaoinmarketcap = function(){
	var exchange_id	= arguments[0];
	var market_name	= arguments[1];
	var url = 'insertmarket?exchange_id='+exchange_id+'&insertmarket=coinmarketcap';
	$('#addmarket_'+exchange_id+'_'+market_name).html('Wait..');
	$.ajax({
		url: url, 
		contentType: "application/json",
		success: function (result) {
			$('#addmarket_'+exchange_id+'_'+market_name).html('Market added successfully!');
		}
	});	
}
</script>
</body>
</html>
