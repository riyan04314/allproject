<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 100,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Exchange</td>
			<td>Coin Symbol</td>
			<td style="text-align:right;">USD Volume</td>
			<td>...</td>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($data['coinexchangevolume'] as $val)
			{
				if($val->volume1<=0){
					continue;
				}
				$i++;
				$volume = 3578*$val->volume1;
				echo '<tr><td>'.$i.'</td>
				<td><a href="/exchange/details/'.$data['exchange'][$val->exchange_id]['id'].'" target="_blank" style="color:#4c95d0;">'.$data['exchange'][$val->exchange_id]['exchange_name'].'</a></td>
				<td>'.$val->symbol1.'/'.$val->symbol2.'</td>
				<td style="text-align:right;">'.number_format($volume,0).'</td>
				<td> &nbsp; </td>
				</tr>';
			}
		?>	
	</tbody>
</table>


<script>
$(document).ready(function() {
	
} );
</script>
</body>
</html>
