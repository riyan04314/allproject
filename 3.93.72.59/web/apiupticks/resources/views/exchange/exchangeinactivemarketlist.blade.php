<?php 
	session_start();
	$developer = 0;
	if(!empty($_SESSION['developer']) && $_SESSION['developer'] == 1){ 
		$developer = 1;
	}
	if(!empty($_REQUEST['developer'])){
		if($_REQUEST['developer'] == 1){
			$developer = 1;
			$_SESSION['developer'] = 1;
		}else{
			$developer = 2;
			$_SESSION['developer'] = 2;
		}				
	}
	//$developer = 1;
	//echo $developer;exit;
?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="http://api.upticks.io/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 100,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>




<br><br>

<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>SL.</td>
		<td>Market ID.</td>
		<td>Exchange Name</td>
		<td>Market Name</td>
		<td>Update</td>
		<td>API URL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($data['exchangemarket'] as $val){
				//prd($val);
				$i++;
				echo '<tr><td>'.$i.'</td>
				<td>'.$val->id.'</td>
				<td>'.$data['exchange'][$val->exchange_id]->exchange_name.'</td>
				<td>'.$val->market_name.'</td>
				<td><a href="http://api.upticks.io/coindata/'.$val->exchange_id.'?exchnage_market_id='.$val->id.'" target="_blank">Update</a></td>
				<td><a href="'.$val->apidataurl.'" target="_blank">'.$val->apidataurl.'</a></td>
				</tr>';
			}
		?>
	</tbody>
</table>

</body>
</html>
