<?php //pr($data['exchange']);?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>

<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true
        }
	});
} );
</script>
</head>
<body>

<h3><?php echo apimenu().'API Data ALL URL List ('.count($data['allapiurl']).')';?></h3><br><br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>SL. No.</td>
		<td>Exchange..</td>
		<td>Time Stamp</td>
		<td>URL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$slno = 0;
			foreach($data['allapiurl'] as $val)
			{
				$slno++;
				//'.date('d-m-Y H:i:s',strtotime($val->timestamp)).'
				echo '<tr>
						<td>'.$slno.'</td>
						<td>'.$data['exchange'][$val->exchange_id]->exchange_name.'</td>
						<td></td>
						<td><a href="'.$val->apidataurl.'" target="_blank" style="color:#6434d6;">'.$val->apidataurl.'</a></td>
					</tr>';
			}
		?>	
	<tbody>
</table>
<br><br>

</body>
</html>

