<?php 
	session_start();
	$developer = 0;
	if(!empty($_SESSION['developer']) && $_SESSION['developer'] == 1){ 
		$developer = 1;
	}
	if(!empty($_REQUEST['developer'])){
		if($_REQUEST['developer'] == 1){
			$developer = 1;
			$_SESSION['developer'] = 1;
		}else{
			$developer = 2;
			$_SESSION['developer'] = 2;
		}				
	}
	//$developer = 1;
	//echo $developer;exit;
?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 100,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php echo apimenu();?>
<div>
<h3>
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>">Exchange API Data</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>?coinsprice=1">Exchange Coin's Price and Volume</a>&nbsp; &nbsp; 
<a target="_blank" style="color:#009" href="/coindata/<?php echo $data['exchange'][0]->id;?>">Update Exchange Data</a> &nbsp; &nbsp;
<a target="_blank" style="color:#009" href="http://api.upticks.io/exchange/currencyconvert/btc?exchange_id=<?php echo $data['exchange'][0]->id;?>">Convert Exchange Volume</a> &nbsp; &nbsp;
<br><br>
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>?showbtc=btc">Show BTC Price Only</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>?showbtc=usd">Show USD Price Only</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/coins/<?php echo $data['exchange'][0]->id;?>">Coins BTC Volume</a>
</h3>
<form action="/pairsymbolfrm" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="exchangeid" value="<?php echo $data['exchange_id'];?>" />
	<select name="mid">
		<?php
			if(!empty($data['exchangemarket'][$data['exchange_id']])){
				foreach($data['exchangemarket'][$data['exchange_id']] as $val1)
				{
					if($val1->symbol1 == '' or $val1->symbol2 == ''){
						echo '<option value="'.$val1->id.'">'.$val1->market_name.'</option>';
					}
				}
			}/**/
			
		?>
	</select>
	<input type="text" name="symbol1" value="" />
	<input type="text" name="symbol2" value="" />
	<input type="submit" name="sbmt" value="Submit" />
</form>
<br>
</div>
<?php
	$total_market = 0;
	if(!empty($data['exchangemarket'][$data['exchange'][0]->id])){
		$total_market = count($data['exchangemarket'][$data['exchange'][0]->id]);
	}
?>



<div>
Note : <?php echo $data['exchange'][0]->note;?><br>
API URL :: <?php if(isset($data['exchangeurl'][$data['exchange_id']]))foreach($data['exchangeurl'][$data['exchange_id']] as $val){echo '<a href="'.$val.'" style="color:#008;" target="_blank">'.$val.'</a> &nbsp; &nbsp; ';}?>
</div>
<?php
$exchange_id = $data['exchange'][0]->id;
if(in_array($exchange_id,array(1,5,7,11,19,21,22,24,30,35))){
	$exchangeurl = "http://api05.upticks.io/exchange/details/".$exchange_id;
}
elseif(in_array($exchange_id,array(36,42,52,55,56,60,67,72,79,89))){
	$exchangeurl = "http://api02.upticks.io/exchange/details/".$exchange_id;
}
elseif(in_array($exchange_id,array(93,133,135,211,347,394,454,477,664,48519))){
	$exchangeurl = "https://api03.upticks.io/exchange/details/".$exchange_id;
}
else{
	$exchangeurl = "http://api01.upticks.io/exchange/details/".$exchange_id;
}
?>
<h3 style="<?php if($data['exchange'][0]->data_update_type == 1){?>background-color: #a4e8b9;<?php }elseif($data['exchange'][0]->data_update_type == 2){?>background-color: #c3dc38;<?php }?>"><?php echo '<a href="/setzero/'.$data['exchange_id'].'" style="color:#000; text-decoration:none;" target="_blank">Type : '.$data['convert_type'].'</a> &nbsp; 
<a href="'.$exchangeurl.'" style="color:#000;">E</a>xchange : '.$data['exchange_name'].' ('.$total_market.')';?> &nbsp; &nbsp; 
( &nbsp;<span id="total_volume"></span> &nbsp; )  &nbsp; 
<a href="/exchange/details/<?php echo $data['exchange'][0]->id;?>" style="color:#000; text-decoration:none;">Show</a> 
<a href="/exchange/active/<?php echo $data['exchange'][0]->id;?>" style="color:#000; text-decoration:none;" target="_blank">All</a>
</h3>

<br><br>
<?php
	$showbtc = '';
	if(isset($_REQUEST['showbtc'])){
		$showbtc = $_REQUEST['showbtc'];//'btc';
	}
	
	$onlyvolume=0;
	if(isset($_REQUEST['onlyvolume']) && $_REQUEST['onlyvolume'] == 1){
		$onlyvolume = ' display:none; ';
	}

	$total_volume = 0;
	
	$coinsprice = 0;
	if(!empty($_REQUEST['coinsprice']) && $_REQUEST['coinsprice'] == 1){
		$coinsprice = 1;
		$showbtc = 'pricevolume';
	}
	
	if($coinsprice == 1)
	{
?>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>SL.</td>
		<td>ID.</td>
		<td>Coin Symbol</td>
		<td style="text-align:right;">BTC Price</td>
		<td style="text-align:right;">USD Price</td>
		<td style="text-align:right;">BTC Volume</td>
		<td style="text-align:right;">USD Volume</td>
		<td>Status</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$btcusd = btcusd();
			foreach($data['exchangecoinsprice'] as $val)
			{
				$i++;
				$btcprice = 1;
				if(strtoupper($val->symbol) != 'BTC'){
					$btcprice = $val->price_btc;
				}
				
				
				echo '<tr><td>'.$i.'</td><td>'.$val->id.'</td>
				<td>'.strtoupper($val->symbol).'</td>
				<td style="text-align:right;">'.number_format($btcprice,8).'</td>
				<td style="text-align:right;">'.number_format($btcusd*$btcprice,4).'</td>
				<td style="text-align:right;">'.number_format($val->volume_btc,0).'</td>
				<td style="text-align:right;">'.number_format($btcusd*$val->volume_btc,0).'</td>
				<td>Active</td></tr>';
			}
		?>	
	</tbody>
</table>

<?php		
	}
	
	
		if($showbtc == '')
		{
?>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>id</td>
		<td>Market ID</td>
		<td>Last Update (Min)</td>
		<?php if(empty($_REQUEST['hide'])){	?>
		<td>Pair</td>
		<?php }	?>
		<td>Sym1</td>
		<td>Sym2</td>
		<?php if(empty($_REQUEST['hide'])){	?>
		<td>Sym1->Sym2</td>
		<?php //}	?>
		<td style="text-align:right;">Base Price</td>
		<td style="text-align:right;">Base Volume</td>
		<?php if($_ENV['SERVERNAME'] == 0){ ?>
		<td style="text-align:right;">Base BTC_Price</td>
		<td style="text-align:right;">Base BTC_Volume</td>
		<?php }
		}
		/*<td style="text-align:right;">Sym1 BTC_Price</td>
		<td style="text-align:right;">Sym2 BTC_Price</td>
		
		<td style="text-align:right;">Price USD</td>*/?>
		<td style="text-align:right;">Volume USD</td>
		
		<td>Status</td>
		<td>Deleted</td>
		<td>Action</td>
		<td>URL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$btcusd = btcusd();
			//$btcusd = $data['btcusd'];
			
			foreach($data['exchange'] as $val)
			{
				$urls 		= '';
				$markets 	= '';
				$exchangeid	= $val->id;
				if(!empty($data['exchangeurl'][$exchangeid])){
					foreach($data['exchangeurl'][$exchangeid] as $val1){
						$urls .= '<a href="'.$val1.'" target="_blank">'.$val1.'</a><br>';
					}
				}
			
				if(!empty($data['exchangemarket'][$exchangeid])){
					$i 			= 0;
					$markets 	= '';//'<b>Total Market : '.count($data['exchangemarket'][$exchangeid]).' </b><br><br>';
					foreach($data['exchangemarket'][$exchangeid] as $val1){
						//pr($val1);
						$i++;
						$market_name = $val1->market_name;
						if($exchangeid == 64){
							$market_name = $val1->symbol1.'/'.$val1->symbol2;
						}/**/
						
						$volume = $val1->volume;
						if($val1->bigvolume>0){
							$volume = $val1->bigvolume;
						}
						$volume_btc = $val1->volume_btc;
						if($volume<0){
							continue;
						}
						if($exchangeid == 93 && $market_name == 'ustusd'){
							continue;
						}
						
						if($val1->id == 19771){
							//continue;
						}
						
						
						
						//$volume_btc = $val1->price_btc*$val1->volume;
						$total_volume += $volume_btc*$btcusd;
						$bgcolor = '';
						if($val1->status == 0){
							$bgcolor = 'background-color:#e7f573b0;';	
						}elseif($val1->status == 2){
							$bgcolor = 'background-color:#f58282;';	
						}else{
							$bgcolor = '';
						}
						
						
						
						if($developer == 1){
							$val1->symbol1_btcprice = $val1->price_btc;//$val1->price*
							$val1->symbol2_btcprice = 0;$val1->price_btc;
						}
						
						
						$decimal_places = 8;
						if($val1->price>100){
							$decimal_places = 0;
						}
						elseif($val1->price<0.00001){
							$decimal_places = 16;
						}
						
						
						
						$markets .= '<tr style="'.$bgcolor.'" >
						<td>'.$i.'</td>
						<td>'.$val1->id.'</td>
						<td>'.(int)(lastupdatetime($val1->lastupdate)/60).'</td>';
						
						if(empty($_REQUEST['hide'])){
						$markets .= '<td>'.$market_name.'</td>';
						}
						$markets .= '<td>'.$val1->symbol1.'</td>
						<td>'.$val1->symbol2.'</td>';
						
						if(empty($_REQUEST['hide'])){
						$markets .= '<td>'.(($data['exchange_convert_type'] == 1)?$val1->symbol1.' to '.$val1->symbol2:$val1->symbol2.' to '.$val1->symbol1).'</td>';
						
						$markets .= '<td style="text-align:right;">'.numberformat($val1->price,$decimal_places).'</td>';
						
					
						$markets .= '<td style="text-align:right;">'.numberformat($volume,2).'</td>';
						
						if($_ENV['SERVERNAME'] == 0){
						$markets .= '<td style="text-align:right;'.$onlyvolume.'">'.numberformat($val1->price_btc,8).'</td>';
						$markets .= '<td style="text-align:right;">'.numberformat($volume_btc,2).'</td>';
						}
						}
						//$markets .= '<td style="text-align:right;'.$onlyvolume.'">'.number_format($val1->symbol1_btcprice,8).'</td>';
						//$markets .= '<td style="text-align:right;'.$onlyvolume.'">'.number_format($val1->symbol2_btcprice,8).'</td>';
						//$markets .= '<td style="text-align:right;'.$onlyvolume.'">'.number_format($btcusd*$val1->price_btc,4).'</td>';
						$markets .= '<td style="text-align:right;">'.number_format($volume_btc*$btcusd).'</td>';
												
						/**/						
						$markets .= '
						<td><a href="/market/'.(($val1->status == 1)?'inactive/':'active/').$val1->id.'" target="_blank">'.(($val1->status == 1)?'A':'D').'</a> &nbsp; 
						</td>
						<td>'.(($val1->deleted == 1)?'Deleted':'').'</td>
						<td><a href="/coindata/'.$data['exchange'][0]->id.'?exchnage_market_id='.$val1->id.'" target="_blank">Update</a></td>
						<td><a href="'.$val1->apidataurl.'" style="color:#008;" target="_blank">'.substr($val1->apidataurl,0,15).'</a></td>
						</tr>';
					}
					echo $markets;
				}
			}
		?>	
	</tbody>
</table>

<?php 	}
		elseif($showbtc == 'btc')
		{	
?>

<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>Pair</td>
		<td>Last Update</td>
		<td style="text-align:right;">Price BTC</td>
		<td style="text-align:right;">Volume BTC</td>
		<td>URL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			foreach($data['exchange'] as $val)
			{
				$urls 		= '';
				$markets 	= '';
				$exchangeid	= $val->id;
				if(!empty($data['exchangeurl'][$exchangeid])){
					foreach($data['exchangeurl'][$exchangeid] as $val1){
						$urls .= '<a href="'.$val1.'" target="_blank">'.$val1.'</a><br>';
					}
				}
			
				if(!empty($data['exchangemarket'][$exchangeid])){
					$i 			= 0;
					$markets 	= '';//'<b>Total Market : '.count($data['exchangemarket'][$exchangeid]).' </b><br><br>';
					foreach($data['exchangemarket'][$exchangeid] as $val1){
						$i++;
						$market_name = $val1->market_name;
						if($exchangeid == 64){
							$market_name = $val1->symbol1.'/'.$val1->symbol2;
						}/**/
						$volume_btc = $val1->volume_btc;
						//$volume_btc = $val1->price_btc*$val1->volume;
						
						$markets .= '<tr>
						<td>'.$market_name.'</td>
						<td>'.lastupdatetime($val1->timestamp).'</td>
						<td style="text-align:right;">'.number_format($val1->price_btc,8).'</td>
						<td style="text-align:right;">'.number_format($volume_btc,8).'</td>
						<td><a href="'.$val1->apidataurl.'" style="color:#008;" target="_blank">URL</a></td>
						</tr>';
					}
					echo $markets;
				}
			}
		?>	
	</tbody>
</table>

<?php	}
		elseif($showbtc == 'usd')
		{	
?>

<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>Pair</td>
		<td style="text-align:right;">Price USD</td>
		<td style="text-align:right;">Volume USD</td>
		<td>URL</td>
	</tr>
	</thead>
	<tbody>
		<?php
			foreach($data['exchange'] as $val)
			{
				$urls 		= '';
				$markets 	= '';
				$exchangeid	= $val->id;
				if(!empty($data['exchangeurl'][$exchangeid])){
					foreach($data['exchangeurl'][$exchangeid] as $val1){
						$urls .= '<a href="'.$val1.'" target="_blank">'.$val1.'</a><br>';
					}
				}
			
				if(!empty($data['exchangemarket'][$exchangeid])){
					$i 			= 0;
					$markets 	= '';//'<b>Total Market : '.count($data['exchangemarket'][$exchangeid]).' </b><br><br>';
					foreach($data['exchangemarket'][$exchangeid] as $val1){
						$i++;
						$market_name = $val1->market_name;
						if($exchangeid == 64){
							$market_name = $val1->symbol1.'/'.$val1->symbol2;
						}/**/
						//$volume_btc = $val1->volume_btc*btcusd();
						//$volume_btc = $val1->price_btc*$val1->volume*btcusd();;
						
						$markets .= '<tr>
						<td>'.$market_name.'</td>
						<td style="text-align:right;">'.$val1->price_usd.'</td>
						<td style="text-align:right;">'.number_format($val1->volume_usd,2).'</td>
						<td><a href="'.$val1->apidataurl.'" style="color:#008;" target="_blank">URL</a></td>
						</tr>';
					}
					echo $markets;
				}
			}
		?>	
	</tbody>
</table>

<?php	}

	?>
<script>
$(document).ready(function() {
	$('#total_volume').html('<?php echo number_format($total_volume,0);?> USD');
} );
</script>
</body>
</html>
