<?php //echo '<pre>';print_r($mydata);echo '</pre>';?>

<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({"pageLength": 10});
} );
</script>
<body>
<h3><?php echo apimenu();?>
Exchange Market Volume</h3>


<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr><th>Exchange Name</th><th>Market</th><th>Volume ( BTC )</th></tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($res as $val)
			{
				$i++;
				echo '<tr><td style="text-align:center;">'.$val->exchange_name.'</td>
				<td style="text-align:center;">'.$val->market_name.'</td>
				<td style="text-align:center; padding-right:130px;">'.$val->volume_btc.'</td></tr>';
			}
		?>	
	<tbody>
</table>


</body>
</html>

