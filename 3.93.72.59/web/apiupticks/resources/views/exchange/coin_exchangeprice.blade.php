<?php
//prd($data['exchange']);
?>
<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="/api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 100,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<?php 
echo apimenu();
echo 'Coin Name : '.$data['coin_details']['name'].' ('.strtoupper($data['symbol']).') &nbsp; <img src="https://cdn.upticks.io/img/coins/24x24/'.str_replace(' ','-',strtolower($data['coin_details']['name'])).'.png" style="width:20px;" /> &nbsp; &nbsp;<br><br><span id="avgprice"></span> &nbsp; &nbsp; <span style="padding-right:15px;">Coinmarketcap Price :</span><hr>';
//echo '.$data['coin_details']['coinmarketcapprice'].' (<a href="/coinmarketcap/exchangerank" target="_blank">Update</a>)
?>
<br>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
		<tr>
			<td>SL.</td>
			<td>Exchange.</td>
			
			<td style="text-align:right;">Price ( <?php if(strtoupper($data['symbol']) == 'BTC'){echo 'USD';}else{echo 'BTC';}?> )</td>
			<?php if(strtoupper($data['symbol']) != 'BTC'){?>
			<td style="text-align:right;">Price ( USD )</td>			
			<td style="text-align:right;">Exchange Volume  ( <?php if(strtoupper($data['symbol']) == 'BTC'){echo 'USD';}else{echo 'BTC';}?> )</td>
			<?php } ?>
			<td style="text-align:right;">Exchange Volume  ( USD )</td>
			<td style="text-align:right;">Status</td>
			<td>Priority Group</td>
			<?php /*<td>Coin Symbol</td>
			<td style="text-align:right;">Exchange <?php echo strtoupper($data['symbol']);?> Volume ( BTC )</td>
			<td style="text-align:right;">Exchange <?php echo strtoupper($data['symbol']);?> Total Weight ( USD )</td>*/?>
			
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			$btcusd = btcusd();
			$exchangeids	= '';
			foreach($data['coindetails'] as $val)
			{
				//echo '---'.$val->exchange_id;
				if($val->price_btc == 0){
					continue;
				}
				
				$bgcolor = '';
				if($val->status == 0){
					$bgcolor = 'background-color:#f58282;';	
				}else{
					$bgcolor = '';
				}				
				
				$exchangeprioritycolor = '';
				if($data['exchange'][$val->exchange_id]['prioritygroup'] == 1){
					$exchangeprioritycolor = 'style="background-color:#77de77;" ';
				}
				elseif($data['exchange'][$val->exchange_id]['prioritygroup'] == 2){
					$exchangeprioritycolor = 'style="background-color:#ffc107;" ';
				}
				elseif($data['exchange'][$val->exchange_id]['prioritygroup'] == 3){
					$exchangeprioritycolor = 'style="background-color:#5d9be8;" ';
				}
				
				
				
				$exchangeids	.= $val->exchange_id.',';
				
				$i++;
				echo '<tr style="'.$bgcolor.'">
				<td>
					<a target="_blank" href="http://api.upticks.io/exchange/currencyconvert/btc?exchange_id='.$val->exchange_id.'">'.$i.'</a>
				</td>
				<td '.$exchangeprioritycolor.'>
					<a href="/exchange/details/'.$data['exchange'][$val->exchange_id]['id'].'?coin='.strtolower($data['symbol']).'" target="_blank">'.ucfirst($data['exchange'][$val->exchange_id]['exchange_name']).'</a>
				</td>
				
				<td style="text-align:right;">'.numberformat($val->price_btc,8).'</td>';
				if(strtoupper($data['symbol']) != 'BTC'){
					echo '<td style="text-align:right;">'.numberformat($btcusd*$val->price_btc,8).'</td>';
					echo '<td style="text-align:right;">'.numberformat($val->volume_btc,8).'</td>';
				}
				echo '<td style="text-align:right;">'.numberformat($btcusd*$val->volume_btc,8).'</td>';
				
				if($val->status == 1){
					echo '<td style="text-align:right; padding-right:10px;"><a target="_blank" href="/coin_exchangeprice/status/'.$val->id.'/0">Active</a></td>';	
				}else{
					echo '<td style="text-align:right; padding-right:10px;"><a target="_blank"  href="/coin_exchangeprice/status/'.$val->id.'/1">Inactive</a></td>';				
				}
				echo '<td '.$exchangeprioritycolor.'>PG:'.$data['exchange'][$val->exchange_id]['prioritygroup'].'</td>';
				echo '
				</tr>';
			}
		?>
		<?php
			$i 			= 0;
			$m = 0;
			$n = 0;
			$color 		= '';
			$avgprice 	= 0;
			
			$btcusd		= btcusd();
			if(strtoupper($data['symbol']) == 'BTC'){
				$btcusd		= btcusd();
			}
			
			foreach($data['coinexchangeprices'] as $val)
			{
				continue;
				//prd($val);
				if($val->price<=0){
					continue;
				}
				$i++;
				$color 		= '';
				$hrefcolor 	= ' style="color:#4c95d0;"';
				//$price 	= $val->price;
				$price 	= $btcusd*$val->price_btc;
				
				
				if($val->type == 2){
					$color 	= ' style="background-color:#30a5946b;" ';
				}elseif($val->type == 3){
					//$price 	= $price*$btcusd;
					$color 	= ' style="background-color:#eff16d;" ';
				}elseif($val->type == 4){
					//$price 	= $price*$btcusd;
					$color 	= ' style="background-color:#b4dbef; color: #000;" ';
					$hrefcolor 	= ' style="color: #000;" ';
				}
				$avgprice 		+= $price;
				$total_weight 	 = $data['exchange'][$val->exchange_id]['volume']*$btcusd;
				
				
				
				echo '<tr '.$color.'><td><a href="http://api.upticks.io/exchange/currencyconvert/btc?exchange_id='.$val->exchange_id.'">'.$i.'))</a></td>
				<td><a href="/exchange/details/'.$data['exchange'][$val->exchange_id]['id'].'?coin='.strtolower($data['symbol']).'" target="_blank" '.$hrefcolor.'>'.$data['exchange'][$val->exchange_id]['exchange_name'].'</a></td>
				<td>'.strtoupper($val->symbol1.'/'.$val->symbol2).'</td>
				<td style="text-align:right;">'.number_format($price,8).'</td>
				<td style="text-align:right;">'.number_format($data['exchange'][$val->exchange_id]['volume']*$btcusd,0).'</td>
				<td style="text-align:right;">'.number_format($data['exchange'][$val->exchange_id]['volume'],0).'</td>
				<td style="text-align:right;">'.number_format($total_weight,0).'</td>
				<td> &nbsp; </td>
				</tr>';

				//$m += $data['exchange'][$val->exchange_id]['volume'];
				//$n += $data['exchange'][$val->exchange_id]['volume']/$price;
				
			}
			/*if($i){
				$avgprice 	= $avgprice/$i;
			}*/
			
			$avgprice 	= 0;
			$avgprice=avgprice($data['symbol'],$avgprice);								
			
		?>
	</tbody>
</table>
<?php
//echo 'EIDS :: '.$exchangeids;
?>

<script>
$(document).ready(function() {
	$('#avgprice').html('Upticks Price :: <?php echo $avgprice?> USD ');
} );
</script>
</body>
</html>
