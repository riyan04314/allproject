<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeURL extends Model
{
    protected $table = 'exchanges_url';
}
