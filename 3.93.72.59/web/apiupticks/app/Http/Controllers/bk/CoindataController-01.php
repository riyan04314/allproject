<?php
namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use DB;
use Auth;

class CoindataController extends Controller
{
	function updatecoindatainactive($exchange_id,$market_name)
	{
		$wherecond['exchange_id']	= $exchange_id;
		$wherecond['market_name']	= $market_name;
		$updatedata['status'] 		= 0;
		DB::table('exchanges_market')->where($wherecond)->update($updatedata);
	}
	
	function setzeroall()
	{
		$updatedata['price'] = 0;
		$updatedata['volume'] = 0;
		$updatedata['bigvolume'] = 0;
		$updatedata['price_btc'] = 0;
		$updatedata['volume_btc'] = 0;
		$updatedata['price_usd'] = 0;
		$updatedata['volume_usd'] = 0;
		$updatedata['volume_krw'] = 0;
		$updatedata['bigvolume_usd'] = 0;
		$updatedata['bigvolume_btc'] = 0;
		$updatedata['price_krw'] = 0;
		$updatedata['bigvolume_krw'] = 0;
		DB::table('exchanges_market')->where('id','>',0)->update($updatedata);
		
		$updatedata	= array();
		$updatedata['price_btc']	= 0;
		$updatedata['volume_btc']	= 0;
		DB::table('exchage_coin_volume')->where('id','>',0)->update($updatedata);
		
		$updatedata	= array();
		$updatedata['price'] = 0;
		$updatedata['volume'] = 0;;
		DB::table('coins_price_volume')->where('id','>',0)->update($updatedata);
	}	
	
	function exchangeactive($exchange_id = 0)
	{
		$updatedata['status']	= 1;
		DB::table('exchanges_market')->where('exchange_id','=',$exchange_id)->update($updatedata);
	}
	
	function setzero($exchange_id = 0)
	{
		$updatedata['price'] = 0;
		$updatedata['volume'] = 0;
		$updatedata['bigvolume'] = 0;
		$updatedata['price_btc'] = 0;
		$updatedata['volume_btc'] = 0;
		$updatedata['price_usd'] = 0;
		$updatedata['volume_usd'] = 0;
		$updatedata['volume_krw'] = 0;
		$updatedata['bigvolume_usd'] = 0;
		$updatedata['bigvolume_btc'] = 0;
		$updatedata['price_krw'] = 0;
		$updatedata['bigvolume_krw'] = 0;
		$wherecond['exchange_id']	= $exchange_id;
		DB::table('exchanges_market')->where($wherecond)->update($updatedata);
		
		$update_data['price_btc'] 	= 0;
		$update_data['volume_btc'] 	= 0;
		$where_cond['exchange_id']	= $exchange_id;
		DB::table('exchage_coin_volume')->where($where_cond)->update($update_data);
		
	}
	
	function updatecoindataurl($exchange_id,$market_name,$price,$volume,$apidataurl = '')
	{
		$wherecond['exchange_id']	= $exchange_id;
		$wherecond['market_name']	= $market_name;
		//$updatedata['status']		= 1;
		$updatedata['lastupdate']	= date('Y-m-d H:i:s');
		$updatedata['apidataurl']	= $apidataurl;
		if($price>=0){
			$updatedata['price'] 		= $price;
		}		
		if($volume>0){
			if($volume<=999999999){
				$updatedata['volume']	 	= $volume;	
				$updatedata['bigvolume']	= 0;
			}else{
				$updatedata['volume']	 	= 0;
				$updatedata['bigvolume']	= $volume;
			}
		}
		//echo $market_name.'++////++<br>';pr($updatedata);
		DB::table('exchanges_market')->where($wherecond)->update($updatedata);
	}	
	
	function updatecoindata($exchange_id,$market_name,$price,$volume,$apidataurl = '')
	{
		$wherecond['exchange_id']	= $exchange_id;
		$wherecond['market_name']	= $market_name;
		$updatedata['status']		= 1;
		$updatedata['lastupdate']	= date('Y-m-d H:i:s');
		$updatedata['apidataurl']	= $apidataurl;
		$updatedata['volume']	 	= 0;	
		$updatedata['bigvolume']	= 0;		
		if($price>=0){
			$updatedata['price'] 		= $price;
		}		
		if($volume>0){
			if($volume<=999999999){
				$updatedata['volume']	 	= $volume;	
				$updatedata['bigvolume']	= 0;
			}else{
				$updatedata['volume']	 	= 0;
				$updatedata['bigvolume']	= (int)$volume;
			}
		}
		//echo '<hr><hr>';
		pr($wherecond);
		pr($updatedata);
		
			DB::table('exchanges_market')->where('status','!=',2)->where($wherecond)->update($updatedata);
		
		
		/*echo $apidataurl.'<br>';
		echo 'OK... DB Updated
		';
		echo 'Market name :: '.$market_name.'++////++<br>';pr($wherecond);pr($updatedata);//exit;
		*/
	}	

	function erravailabledata($exchange_id,$sourceurl,$availdata)
	{
		if(!empty($availdata)){
			if($sourceurl != ''){
				$updarr['sourceurl'] = $sourceurl;
			}
			$updarr['availdata'] = json_encode($availdata);
			DB::table('errexchanges')->where(['id'=>$exchange_id])->update($updarr);
		}		
	}
		
	function errupdatecoindata($exchange_id,$market_name,$price,$volume,$apidataurl = '')
	{
		$wherecond['exchange_id']	= $exchange_id;
		$wherecond['market_name']	= $market_name;
		$updatedata['apidataurl']	= $apidataurl;
		$updatedata['price'] 		= $price;
		if($volume<=999999999){
			$updatedata['volume']	 	= $volume;
			$updatedata['bigvolume']	= 0;
		}else{
			$updatedata['volume']	 	= 0;
			$updatedata['bigvolume']	= $volume;
		}
		DB::table('errexchanges_market')->where($wherecond)->update($updatedata);
	}	
	
	function chkexchangemarket($exchange_id)
	{
		$status = 1;
		$res 	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->take(1)->get();
		foreach($res as $val)
		{
			$status = 0;			
		}
		return $status;
	}	
	
	function availabledata($exchange_id,$sourceurl,$availdata)
	{
		if(!empty($availdata)){
			$updarr['sourceurl'] = $sourceurl;				
			$updarr['availdata'] = json_encode($availdata);
			//echo '<pre>';print_r($updarr);echo '</pre>';
			DB::table('exchanges')->where(['id'=>$exchange_id])->update($updarr);
		}		
	}
		
	public function exchangeMarket($exchange_id,$limit=10)
	{
		$limit=1;
		$wherecond[] = ['exchange_id','=', $exchange_id];
		$wherecond[] = ['status','=',1];
		
		
		if(!empty($_REQUEST['frequently']) && $_REQUEST['frequently'] == 2){
			$wherecond[] = ['volume_btc','>',1];
		}
		elseif(!empty($_REQUEST['frequently']) && $_REQUEST['frequently'] == 3){
			//$wherecond[] = ['volume_btc','>',50];
		}
		elseif(!empty($_REQUEST['frequently']) && $_REQUEST['frequently'] == 4){
			//$wherecond[] = ['volume_btc','>',100];
		}
		
		
		//pr($wherecond);
		$res = App\ExchangeMarket::where($wherecond)->orderBy('lastupdate', 'asc')->take($limit)->get();
		if(!empty($_REQUEST['exchnage_market_id'])){
			$exchnage_market_id = $_REQUEST['exchnage_market_id'];
			$res = App\ExchangeMarket::where('id','=', $exchnage_market_id)->take($limit)->get();
		}
		$updarr['status']		= 0;
		$updarr['lastupdate']	= date('Y-m-d H:i:s');
		foreach($res as $val){
			DB::table('exchanges_market')->where(['id'=>$val->id])->update($updarr);
		}
		return $res;
	}
	
	function marketlist($exchange_id,$limit=10000)
	{
		$res = App\ExchangeMarket::where('exchange_id','=', $exchange_id)->where('status','=',1)->orderBy('timestamp', 'asc')->take($limit)->get();		
		return $res;
	}
		
	public function coindata(Request $req)
	{
		//exit;//1
		$data 			= array();
		$availdata		= array();
		$sourceurl		= '';
		$t 				= time();
		$apiurl			= '';
		//$exchange_id	= " and id = 93 ";
		$exchange_id	= "";
		$coinlimit 		= 4;
		$coinlimit1 	= 1;
		$e8		= 0.00000001;
		$sql 	= "select id from `exchanges` where status in (1,5,7) ".$exchange_id." order by time asc limit 1";
		//$sql 	= "select id from `exchanges` where status in (1,5,7) order by time asc limit 1";
		
		$i = 0;//rand(1,4);
		
		if(!empty($_REQUEST['frequently'])){
			$i = $_REQUEST['frequently'];
			if($_REQUEST['frequently'] == 1){
				$i = 1;
				echo 'Frequently';
			}
			elseif($_REQUEST['frequently'] == 2){
				$i = 2;
				echo 'One Coin Frequently';
			}
		}
		
		
		//$i = 5;
		//echo '++++'.$i.'*-*-*-*-****>>>>>';//exit;
		
		if(empty($req->exchange_id))
		{
			if(in_array($i,array(1,5,6,7)))
			{
				//echo 'Here +++++';exit;
				echo 'Multiple Market Exchange....+';
				$exchange_id	= '';
				$sql 	= "select id from `exchanges` where data_update_type = 1";
				$res 	= DB::select($sql);
				foreach($res as $val){
					$exchange_id	.= $val->id.',';
				}
				$exchange_id	= substr($exchange_id,0,-1);
				$exchange_id	= " and id in (".$exchange_id.")";
				
				$rankcond = '';
				
				
				
				//$rankcond 	= " and coinmarketcap_rank<=50 ";
				if($_ENV['SERVERNAME'] == 3){
					$rankcond 	= " and coinmarketcap_rank>50 ";
				}else{
					//
				}
				
				$sql 	= "select * from `exchanges` where status in (1,5,7) ".$exchange_id.$rankcond." order by time asc limit 1";
				//echo $sql;exit;
				//$sql 	= "select * from `exchanges` where status in (1,5,7) and data_update_type = 1 and id in (2,3) order by time asc limit 1";
			}
			else
			{
				echo 'Single Market Exchange..++++....';//exit;
					
				$exchange_id	= '';
				$sql 	= "select id from `exchanges`  where data_update_type = 2";
				$res 	= DB::select($sql);
				foreach($res as $val){
					$exchange_id	.= $val->id.',';
				}
				$exchange_id	= substr($exchange_id,0,-1);
				$exchange_id	= " and id in (".$exchange_id.")";	
				
				
				$x = $_ENV['SERVERNAME'];
				
				if($x == 0){
					$sql 	= "select * from `exchanges` where status in (1,5,7) and data_update_type = 2 and id in (2,3) order by time asc limit 1";
					$sql 	= "select * from `exchanges` where status in (1,5,7) and data_update_type = 2 ".$exchange_id." order by time asc limit 1";
					$res = DB::select($sql);
					foreach($res as $val){
						$exchange_id = $val->id;
						$sql = "UPDATE exchanges SET time = ".$t." where id = ".$exchange_id;
						DB::update($sql);
						$this->exchangeimport($exchange_id);				
						exit;
					}
				}
				elseif($x == 1){
					$exchange_id	= " and id in (477,454,60,11,7,48519,394) ";
				}
				elseif($x == 2){
					$exchange_id	= " and id in (211,22,24,1,5) ";
				}
				elseif($x == 3){
					$exchange_id	= " and id in (211,22,24,1,5) ";
				}
				elseif($x == 4){
					//$exchange_id	= " and id in () ";
				}
				elseif($x == 5){
					$exchange_id	= " and id in (35,21,19,36,30,79,56,55,89,42,93,347,67,664,135,72) ";
				}
				/* */
				$sql = "select * from `exchanges` where status in (1,5,7) and data_update_type = 2 ".$exchange_id." order by time asc limit 1";
				//$sql = "select * from `exchanges` where status in (1,5,7) and data_update_type = 2 order by time asc limit 1";
				//echo $sql;exit;
			}
		}		
		echo '*-*-';
		if(!empty($req->exchange_id) and $req->exchange_id != 9999){
			$exchange_id	= $req->exchange_id;
			$sql 	= "select * from `exchanges` where status in (1,5,7) and id = ".$exchange_id." order by time asc limit 1";
		}else{
			//exit;
		}
		
		/*echo '
		
		Exchange ID '.$exchange_id.'....
		
		
		';EXIT;/* */
		if(in_array($exchange_id,array(48522,37,152))){
			echo 'Data not updated for this exchange due to EXCLUDE EXIT';
			exit;
		}
		if(in_array($exchange_id,array(37,14580,33,147,152))){
			//exit;
		}
		
		
		//$exchange_id	= 2;
		//$sql 	= "select * from `exchanges` where status in (1,5,7) ".$exchange_id." order by time asc limit 1";
		//echo '<br><br><br>'.$sql;exit;
		//exit;
		
		//$this->setzeroall();//exit;
		//$sql 	= "select id from `exchanges` where status in (1,5,7) and id in (9,25,123) order by time asc limit 1";
		//echo $sql;//exit;
		//echo '<br>------';exit;
		
		$convert_type	= 0;
		$res 	= DB::select($sql);		
		foreach($res as $val)
		{
			$exchange_id	= $val->id;
			$convert_type	= $val->convert_type;
			$sql = "UPDATE exchanges SET time = ".$t.", update_status = 0 where id = ".$exchange_id;
			DB::update($sql);
		}
		
		echo '---'.time().'+++....+++'.$exchange_id.'
		
		++++++<br><br><br>';/*exit; /* */
		
		
		
		if(empty($req->exchange_id)){}
		if($_ENV['SERVERNAME'] == 0){
			if(in_array($exchange_id,array(26,86,48524,48,625,25,616,48510,32,6,38,20,48523,48520,33,131,49,27,10,51,39,48526,187,53,48522,115,62,97,48527,61,46,40,14580,99,64,139,103,716,123,127,147,477,454,60,11,7,48519,394,211,22,24,1,5,35,21,19,36,30,79,56,55,89,42,93,347,67,664,135,72))){
			//if(in_array($exchange_id,array(477,454,60,11,7,48519,394,211,22,24,1,5,35,21,19,36,30,79,56,55,89,42,93,347,67,664,135,72) )){
				
				$sql = "UPDATE exchanges SET time = ".$t.", update_status = 1 where id = ".$exchange_id;
				DB::update($sql);		
				echo '...>>>'.$exchange_id.'<<<.....';				
				$this->exchangeimport($exchange_id);
				exit;
			}
		}else{
			echo '...>>>>>>>>>>>>'.$exchange_id.'<<<.....';		
		}/* */
		
		
		
		//echo '...........*+*+*+.'.$exchange_id;exit;
		switch($exchange_id)
		{
			case 1:
					$updatearr	= array();
					$availdata 	= array();
					$url 		= "https://api.bitfinex.com/v1/pubticker/";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$i 		= 0;
						//echo '<pre>';print_r($data);exit;
						$updatedata['price'] 	= $data->last_price;
						$volume 				= $data->volume;
						if($volume<=999999999){
							$updatedata['volume']	= $volume;
						}else{
							$updatedata['bigvolume']= $volume;
						}
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->market_name;
						
						//echo '++++++++++++++<pre>';print_r($data);exit;
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
					
						$this->updatecoindata($exchange_id,$val->market_name,$data->last_price,$data->last_price*$data->volume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					
					break;
			case 2:
					$availdata = array();
					$arr	= array();
					$url1 	= "https://www.binance.com/api/v1/ticker/allPrices";
					$data 	= file_get_contents($url1);
					$data	= json_decode($data);
					$i = -1;
					foreach($data as $key=>$val){
						if($val->symbol == 'BNBBTC'){
							$availdata[0] = $val;
						}
						//$this->updatecoindata($exchange_id,$val->symbol,$val->price,0);
					}
					//echo $availdata[0]->symbol;exit;
					
					$url2 	= "https://www.binance.com/exchange/public/product";
					$data 	= file_get_contents($url2);
					$data	= json_decode($data);
					$i = -1;
					foreach($data->data as $key=>$val){
						if($availdata[0]->symbol == $val->symbol){
							$availdata[1] = $val;
						}
						//$sql = "UPDATE exchanges_market SET symbol1 = '".$val->baseAsset."', symbol2 = '".$val->market."' WHERE exchange_id = ".$exchange_id." AND market_name = '".$val->symbol."'";
						//echo $sql.';<br><br>';
						//DB::update($sql);
						$this->updatecoindata($exchange_id,$val->symbol,$val->close,$val->tradedMoney,$url2);
						//$this->updatecoindata($exchange_id,$val->symbol,$val->close,$val->volume,$url2);
					}
					
					$url = '';
					$this->availabledata($exchange_id,$url1,$availdata);
					break;
			case 3:
					$apiurl = "https://www.okex.com/api/v1/tickers.do";
					$apiurl = "https://www.okex.com/api/v1/tickers.do";
					$data	= file_get_contents($apiurl);
					//echo $data;
					$data	= json_decode($data);
					//echo '<pre>';print_r($data->tickers);echo '</pre>';
					foreach($data->tickers as $val)
					{
						$volume = 0;
						if($val->last>0){
							$volume = $val->vol*$val->last;
						}
						if($val->symbol == 'ltc_eth')
						echo '+++ '.$val->symbol.' .. '.$val->last.' +++ '.$val->vol.' *** '.$volume.'<br>';
						//
						$this->updatecoindata($exchange_id,$val->symbol,$val->last,$volume,$apiurl);						
					}						
					break;
					
					/*$apiurl = "https://www.okex.com/v2/spot/markets/index-tickers?limit=100000000";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					foreach($data->tickers as $val)
					{
						if(!empty($val)){
							$sourceurl	= $apiurl;
							$availdata 	= $val;
						}
						if($val->volume<=999999999)
						{
							$sql = "update exchanges_market SET price = ".$val->last.", volume = ".$val->volume." WHERE exchange_id = ".$exchange_id." AND market_name = '".$val->symbol."'";
							//DB::update($sql);
						}
						else
						{
							$sql = "update exchanges_market SET price = ".$val->last." WHERE exchange_id = ".$exchange_id." AND market_name = '".$val->symbol."'";
							//DB::update($sql);
						}
						
						$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->coinVolume,$apiurl);
						
					}*/					
					break;
			case 4:
					$url	= "https://api.huobi.com/market/history/kline?period=1day&size=1&symbol=";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{					
						$apiurl = $url.$val->market_name;
						echo '
						<br><a href="'.$apiurl.'" target="_blank">'.$apiurl.'....</a><br>
						
						';//exit;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						if(isset($data->data))
						{
							foreach($data->data as $val1)
							{
								//pr($val1);
								$this->updatecoindata($exchange_id,$val->market_name,$val1->close,$val1->vol,$apiurl);
							}
						}
						else
						{
							$this->updatecoindataurl($exchange_id,$val->market_name,0,0,$apiurl);
						}
					}
					//exit;
					//$this->setzero($exchange_id);
					$apiurl = "https://api.huobi.com/market/tickers";
					$apiurl = "https://api.huobipro.com/market/tickers";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					
					
					//echo $apiurl;
					//prd($data);
					
					
					foreach($data->data as $val)
					{
						echo '<br>+++++';pr($val);
						$availdata = $val;
						$this->updatecoindata($exchange_id,$val->symbol,$val->close,$val->vol,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 5:
					$updatearr	= array();
					$url 		= "https://api.upbit.com/v1/ticker?markets=";//"KRW-XRP";
					$res 		= $this->exchangeMarket($exchange_id,30);
					foreach($res as $val)
					{
						$apiurl		= $url.$val->market_name;
						$data		= file_get_contents($apiurl);
						$data		= json_decode($data);
						$availdata 	= $data[0];
						//echo $apiurl.'<br>'.$val->market_name.' :: Price :: '.number_format($data[0]->trade_price,8).' :: Volume :: '.number_format($data[0]->acc_trade_price_24h,8).'<br>';
						$this->updatecoindata($exchange_id,$val->market_name,$data[0]->trade_price,$data[0]->acc_trade_price_24h,$apiurl);						
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;					
			case 6:
					$url 	= "https://api.bitflyer.com/v1/getticker?product_code=";
					$res	= $this->exchangeMarket($exchange_id,6);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//echo '<pre>';print_r($data);exit;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $data->product_code;						
						$updatedata['price'] 		= $data->ltp;
						if($data->volume<=999999999){
							$updatedata['volume']	 	= $data->volume;
						}else{
							$updatedata['bigvolume']	= $data->volume;
						}
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);
						$this->updatecoindata($exchange_id,$data->product_code,$data->ltp,$data->ltp*$data->volume_by_product,$apiurl);	
					}
					break;
			case 7:
					//$sql 	= "select * from `exchanges_market` where exchange_id = ".$exchange_id." order by id";
					//$res 	= DB::select($sql);
					$res	= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$market_name = strtolower(str_replace('/','',$val->market_name));
						$apiurl = "https://www.bitstamp.net/api/v2/ticker/".$market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$price 	= $data->last;
						$volume	= $data->last*$data->volume;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}
						$lup = date('Y-m-d H:i:s');
						$sql 	= "UPDATE exchanges_market SET status = 1, apidataurl = '".$apiurl."', lastupdate = '".$lup."', price = ".$price.", volume = ".$volume." WHERE id = ".$val->id." and exchange_id = ".$exchange_id."";
						DB::update($sql);
						//$this->updatecoindata($exchange_id,$val->id,$price,$volume,$apiurl);											
						//print_r($val);exit;
					}
					break;
			case 9:
					$apiurl = "https://api.bithumb.com/public/ticker/All";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i = 0;
					foreach($data->data as $key=>$val)
					{
						if($key == 'date'){;
							break;
						}
						$availdata = $val;
						if(isset($val->average_price) && isset($val->volume_1day)){
							//pr($val);
							$this->updatecoindata($exchange_id,$key,$val->average_price,$val->average_price*$val->volume_1day,$apiurl);	
						}
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);			
					break;
			case 10:
					$apiurl = "https://www.btcbox.co.jp/api/v1/ticker/";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$price 	= $data->last;
					$volume	= $data->vol;
					if(empty($availdata)){
						$sourceurl	= $apiurl;
						$availdata 	= $data;
					}						
					$sql 	= "UPDATE exchanges_market SET lastupdate = '".date('Y-m-d H:i:s')."', price = ".$price.", volume = ".$volume." WHERE id = 325 and exchange_id = ".$exchange_id."";
					DB::update($sql);
					//$this->updatecoindata($exchange_id,$key,$val->average_price,$val->average_price*$val->volume_1day,$apiurl);
					//print_r($data);
					break;
			case 11:
					$url	= "https://api.kraken.com/0/public/Ticker?pair=";//"BCHUSD";
					$res 	= $this->exchangeMarket($exchange_id,60);
					foreach($res as $val)
					{
						$pairname 	= str_replace("/","",$val->market_name);
						$sql 		= "update `exchanges_market` set market_name = '".$pairname."', status = 1 where id = ".$val->id;
						//echo '<br><br>'.$sql;
						DB::update($sql);
						
						if($val->symbol1 == 'XBT')
						{
							$sql 		= "update `exchanges_market` set symbol1 = 'BTC' where id = ".$val->id;
							echo '<br><br>'.$sql;
							DB::update($sql);
						}
						
						if($val->symbol2 == 'XBT')
						{
							$sql 		= "update `exchanges_market` set symbol2 = 'BTC' where id = ".$val->id;
							//echo '<br><br>'.$sql;
							DB::update($sql);
						}
						

						
						//continue;
						$apiurl	= $url.$pairname;
						$apiurl.'  &nbsp; &nbsp; &nbsp;  ++++<br>';
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$i 		= 0;
						//prd($data->result);
						foreach($data->result as $val)
						{
							$price	= $val->c[0];
							$volume	= $price*$val->v[1];
							//pr($volume);
							//pr($price);
							echo '<hr>';
							$this->updatecoindata($exchange_id,$pairname,$price,$volume,$apiurl);
						}
					}
					
					break;
			case 13:
					$url 	= "https://api.hitbtc.com/api/2/public/ticker";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $key=>$val){
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->symbol;						
						$updatedata['price'] 		= $val->last;
						if($val->volume<=999999999){
							$updatedata['volume']	 	= $val->volume;
						}else{
							$updatedata['bigvolume']	= $val->volume;
						}
						if(empty($val)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						
						$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->volume,$url);
							
					}			
					break;
			case 14:
					$url 	= "https://www.bcex.top/Api_Market/getPriceList";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;coin_from":"eth","coin_to":"btc","current":"0.02824450","count":
					$i = -1;
					foreach($data as $key=>$val)
					{			
						foreach($val as $val1)
						{
							$updatedata['price'] 		= $val1->current;
							if($val1->count<=999999999){
								$updatedata['volume']	 	= $val1->count;
							}else{
								$updatedata['bigvolume']	 	= $val1->count;
							}
							$wherecond['exchange_id']	= $exchange_id;
							$wherecond['market_name']	= $val1->coin_from.'/'.$val1->coin_to;
							if(empty($availdata)){
								$sourceurl	= $url;
								$availdata 	= $val1;
							}
							DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
							//echo $val1->coin_from.'/'.$val1->coin_to.' -- '.$val1->current.' == '.$val1->count.'<br>';
							//$this->updatecoindata($exchange_id,$val1->coin_from.'/'.$val1->coin_to,$val1->current,$val1->count,$url);
						}
					}
					break;
			case 15:
					$availdata = array();
					$apiurl = "https://api.lbkex.com/v1/ticker.do?symbol=all";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i = 0;
					foreach($data as $key=>$val)
					{
						$availdata = $val;
						$this->updatecoindata($exchange_id,$val->symbol,$val->ticker->latest,$val->ticker->latest*$val->ticker->vol,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);			
					break;
			case 16:
				$url 	= "https://simex.global/api/pairs";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->data);
				$i = -1;
				$arr = array();			
				foreach($data->data as $key=>$val){
$this->updatecoindata($exchange_id,$val->base->name.'/'.$val->quote->name,$val->last_price,$val->quote_volume,$url);
				}
				break;					
			case 17:
					$availdata = array();
					$apiurl = "https://apiv2.bitz.com/Market/tickerall";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i = 0;
					foreach($data->data as $key=>$val)
					{
						$availdata = $val;
						$this->updatecoindata($exchange_id,$val->symbol,$val->now,$val->quoteVolume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);			
					break;
			case 18:
  /*$post = ['batch_id'=> "2"];
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL,'https://api.exx.com/data/v1/tickers');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
  $response = curl_exec($ch);
  echo $response;
  echo '55555';  exit;
  $result = json_decode($response);*/
  
					
					//echo 'FILE GET CONTENT NOT WORKING. EXIT!';exit;
					$availdata = array();
					$apiurl = "https://api.exx.com/data/v1/tickers";
					$apiurl = "https://api.exx.com/data/v1/ticker?currency=eth_hsr";
					//$apiurl = "http://api.upticks.io/exxdata.json";
					$apiurl = "http://api.upticks.io/exxdata.json";
					
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i = 0;
					//echo $apiurl.'++++<pre>';print_r($data);echo '<pre>';exit;
					foreach($data as $key=>$val)
					{
						//echo '<pre>';print_r($val);echo '<pre>';
						$availdata = $val;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->vol,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);			
					break;
			case 19:		
					$updatearr	= array();
					$url 		= "https://api.gemini.com/v1/pubticker/";
					$res 		= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(10)->get();
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$i 		= 0;
						$price 		= $data->last;
						$volume 	= 0;
						foreach($data->volume as $v){
							$volume = $v;break;
						}
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}												
						DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
					}					
					break;
			case 20:
					
					$apiurl		= "https://poloniex.com/public?command=return24hVolume";
					$data		= file_get_contents($apiurl);
					$data		= json_decode($data);
					$i 			= 0;
					$updatearr	= array();
					foreach($data as $key=>$val)
					{
						$volume = 0;
						//echo '++++'.$i.'--'.$key.'<br>';//'<pre>';print_r($val);echo '</pre>';
						foreach($val as $val1)
						{
							$updatearr[$key]['volume'] = $val1;break;
							//$volume = $val1;break;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id])->where(['market_name'=>$key])->update(['volume' => $volume]);
						$i++;
						if($i>117)
						break;
					}
					
					
					$apiurl	= "https://poloniex.com/public?command=returnTicker";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i 		= 0;
					foreach($data as $key=>$val)
					{
						$updatearr[$key]['price'] = $val->last;
						//echo '<pre>';print_r($val);echo '</pre>';exit;
						//echo $price = $val->last;exit;
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id])->where(['market_name'=>$key])->update(['price' => $price]);
						$i++;
						if($i>117)
						break;
					}
					//echo '<pre>';print_r($updatearr);echo '</pre>';exit;
					foreach($updatearr as $key=>$val)
					{
						if(empty($val)){
							$sourceurl	= $apiurl;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$key])->update(['price' => $val['price'],'volume'=>$val['volume']]);
						$this->updatecoindata($exchange_id,$key,$val['price'],$val['volume'],$apiurl);
						//exit;
					}
					break;
			case 21:									
					$updatearr	= array();
					echo $url 		= "https://bittrex.com/api/v1.1/public/getmarketsummary?market=";
					//$res 		= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(1000000)->get();
					$res 		= $this->exchangeMarket($exchange_id,60);
					foreach($res as $val)
					{
						$apiurl	= $url.strtolower($val->market_name);
						echo $apiurl.'  &nbsp; &nbsp; &nbsp;  ++++<br>';
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$i 		= 0;
						//echo '<pre>';print_r($data);exit;
						foreach($data->result as $val)
						{
							if(empty($val)){
								$sourceurl	= $apiurl;
								$availdata 	= $val;
							}
							$this->updatecoindata($exchange_id,$val->MarketName,$val->Last,$val->BaseVolume,$apiurl);						
						}
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					//echo '<pre>';print_r($updatearr);
					break;
			case 22:
					$i = 0;
					$updatearr	= array();
					$url 		= "http://api.coinbene.com/v1/market/ticker?symbol=";
					$res 		= $this->exchangeMarket($exchange_id,60);
					foreach($res as $val)
					{
						$i++;
						if($i%5 == 0){
							sleep(2);
						}
						$apiurl	= $url.strtolower(str_replace('/','',$val->market_name));
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$data 	= $data->ticker;
						$twentyfourhrVol = '24hrVol';
						$twentyfourhrVol = '24hrAmt';
						$price	= $data[0]->last;
						$volume	= $data[0]->$twentyfourhrVol;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$val->market_name,$price,$volume,$apiurl);						
						
					}
					$this->availabledata($exchange_id,$url,$availdata);
					break;
			case 23:
					$apiurl	= "https://oex.com/api/v1/tickers";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					$i 		= 0;
					//echo '<pre>';print_r($data->data);
					$v = '24h_vol';
					foreach($data->data as $val)
					{
						//print_r($val);exit;
						$volume = $val->$v;
						$price	= $val->latest;
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata 	= $val;
						}
						DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>strtoupper($val->name)])->update(['price' => $price,'volume'=>$volume]);
					}					
					break;
			case 24:
					$i = 0;
					$updatearr	= array();
					$url 		= "https://api.bibox.com/v1/mdata?cmd=kline&period=day&size=1&pair=";
					$res 		= $this->exchangeMarket($exchange_id,5);
					foreach($res as $val)
					{
						$sql = "UPDATE exchanges_market SET status = 0 WHERE exchange_id = ".$exchange_id." and market_name = '".$val->market_name."'";
						DB::update($sql);						
						$i++;
						if($i%5 == 0){
							sleep(2);
						}
						$apiurl	= $url.$val->market_name;
						echo $apiurl.'<br>';
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$data	= $data->result;
						$data	= $data[0];
						$price	= $data->close;
						$volume	= $data->vol;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$val->market_name,$price,$volume,$apiurl);
						$sql = "UPDATE exchanges_market SET status = 1 WHERE exchange_id = ".$exchange_id." and market_name = '".$val->market_name."'";
						//DB::update($sql);						
						//exit;
						$this->updatecoindata($exchange_id,$val->market_name,$data->close,$data->close*$data->vol,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$data);
					break;
			case 25:
					$url 		= "https://public.bitbank.cc/";//"ltc_btc/ticker";
					$res 		= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(10)->get();
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name."/ticker";
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$volume	= $data->data->vol;
						$price 	= $data->data->last;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$val->market_name,$price,$price*$volume,$apiurl);
						
					}
					break;
			case 26:
				$url 	= "https://openapi.ooobtc.com/public/v1/getallticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->data);
				foreach($data->data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->tickername,$val->lastprice,$val->lastprice*$val->volume,$url);
				}
				//prd($arr);
				break;	
			case 27:
				$url 	= "https://api.coinone.co.kr/ticker/?currency=all";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					if(!in_array($key,array('errorCode','result','timestamp'))){
						//echo '<h3>'.$key.'</h3>';
						//echo $key.'/krw'.'++++'.$val->last.'****'.$val->last*$val->volume.'<br>';;
						//pr($val);
						//echo '<hr>';					
						$this->updatecoindata($exchange_id,$key.'/krw',$val->last,$val->last*$val->volume,$url);
					}/**/
				}
				//prd($arr);
				break;				
			case 30:
				$availdata = array();
				$url	= "https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=";
				$res 	= $this->exchangeMarket($exchange_id,10);
				foreach($res as $val)
				{
					$apiurl		= $url.$val->market_name;
					//echo $apiurl.'<br>';
					$data	 	= file_get_contents($apiurl);
					$data		= json_decode($data);
					//prd($data);
					$availdata 	= $data;
					$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);
				}
				//$this->availabledata($exchange_id,$apiurl,$availdata);
				break;
			case 31:
					$url 	= "https://upticks.io/apiupticks/curl.php?exchangeid=".$exchange_id;//."&coin=LTC&market=BTC";
					$urlorg	= 'http://topbtc.com/market/market.php';
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$arr		= explode("/",$val->market_name);
						$apiurl		= $url."&coin=".$arr[0]."&market=".$arr[1];
						$urlorg1	= $urlorg."?coin=".$arr[0]."&market=".$arr[1];
						$data 		= file_get_contents($apiurl);
						
						//echo '<br><br><br>'.$data;
						if(strlen($data)<10){
							$sql = "UPDATE `exchanges_market` set status = 0 where id = ".$val->id;
							DB::update($sql);
							exit;
						}
						
						$data 		= json_decode($data);
						$volume		= 0;
						foreach($data->sell as $val1){
							$volume	+= $val1->amount;
						}
						foreach($data->buy as $val1){
							$volume	+= $val1->amount;
						}
						
						$price		= $data->last->price;
						//echo '<br><pre>';print_r($data->last->price);echo '</pre>';//exit;exit;
						$availdata 	= $data;
						$this->updatecoindata($exchange_id,$val->market_name,$price,$volume,$urlorg1);
					}
					//$this->erravailabledata($exchange_id,$apiurl,$availdata);					
					break;
			case 32:
					$this->setzero($exchange_id);
					$url = "https://coinsbank.com/api/bitcoincharts/ticker/";
					$sql = "select * from `exchanges_market` where market_name like '%/%' and exchange_id = 32 order by id desc";
					$res = DB::select($sql);
					foreach($res as $val){
						$market_name = str_replace('/','',$val->market_name);
						$apiurl		= $url.$market_name;
						//myurl($apiurl);
						$data	 	= file_get_contents($apiurl);
						$data		= json_decode($data);
						//pr($data);
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);
					}
					
					/*
					$url 	= "http://api.bitcoincharts.com/v1/markets.json";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data);
					foreach($data as $key=>$val)
					{
						$this->updatecoindata($exchange_id,$val->symbol,$val->close,$val->currency_volume,$url);
					}*/
					break;
			case 33:
					$url 			= "https://www.coinhub.io/en/gateway/coinhub/public/returnTicker";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					//echo '<pre>';print_r($data);
					foreach($data->results as $key=>$val)
					{
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->volume24hr,$url);
					}
					break;
			case 35:
					$availdata = array();
					$url = "https://api.coinegg.im/api/v1/ticker/region/";//"usdt?coin=neo";
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$arr = explode('/',strtolower($val->market_name));
						$apiurl		= $url.$arr[1].'?coin='.$arr[0];
						echo $apiurl.'<br>';
						$data	 	= file_get_contents($apiurl);
						$data		= json_decode($data);
						pr($data);
						$availdata 	= $data;
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->vol,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 36:
					$i = 0;
					$updatearr	= array();
					$url 		= "https://api.itbit.com/v1/markets/";//"XBTUSD/ticker";
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$i++;
						$apiurl	= $url.str_replace('/','',$val->market_name)."/ticker";
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$price	= $data->lastPrice;
						$volume	= $data->volume24h;
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
						//$val->market_name	= str_replace('XBT','BTC',$val->market_name);
						$this->updatecoindata($exchange_id,$val->market_name,$data->lastPrice,$data->lastPrice*$data->volume24h,$apiurl);
					}				
					break;
			case 37:
					exit;
					$url 			= "http://api.zb.cn/data/v1/allTicker";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					//echo '<pre>';print_r($data);
					$arr = array();
					foreach($data as $key=>$val){
						
						/*$updatedata['price'] 		= $val->last;
						$updatedata['volume']	 	= $val->last*$val->vol;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $key;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						
						if($key == 'dogebtc'){
							echo '<pre>';print_r($val);echo '</pre>';
							echo '<pre>';print_r($updatedata);echo '</pre>';
							//exit;
						}*/
						
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->vol,$url);
					}					
					break;
			case 38:
					$url 			= "https://api.exmo.com/v1/ticker/";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					//echo '<pre>';print_r($data);
					$arr = array();
					foreach($data as $key=>$val){
						$market	= $key;
						$price	= $val->last_trade;
						$volume	= $val->vol_curr;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata = $val;
						}						
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$key,$val->last_trade,$val->vol_curr,$url);
					}					
					break;
			case 39:
				$url 	= "https://api.livecoin.net/exchange/ticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->volume,$url);
				}
				//prd($arr);
				break;					
			case 40:
					//$this->setzero($exchange_id);exit;
					$i 		= 0;
					$url 	= "https://api.tidex.com/api/3/ticker/";//"eth_btc-ltc_btc-srn_btc";
					$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(100)->get();
					foreach($res as $val)
					{
						$url .= $val->market_name.'-';
					}
					$url	= substr($url,0,-1);
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					foreach($data as $key=>$val)
					{
						$price 	= $val->last;
						$volume	= $val->vol_cur;
						if(empty($val)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$key])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$key,$val->avg,$val->vol,substr($url,0,10000));
					}
					break;
			case 41:
					//$this->setzero($exchange_id);exit;
					$url 			= "https://openapi.idax.pro/api/v2/ticker";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					//echo '<pre>';print_r($data);
					foreach($data->ticker as $val)
					{
						if(empty($val)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						if($val->vol<=999999999){
							//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->pair])->update(['price' => $val->last,'volume'=>$val->vol]);
						}
						
						$this->updatecoindata($exchange_id,$val->pair,$val->last,$val->last*$val->vol,$url);
					}
					break;
			case 42:
					$url = "https://cex.io/api/tickers/";//"ETH/EUR
					$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(10)->get();
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						foreach($data->data as $val1)
						{
							$market	= str_replace(':','/',$val1->pair);
							$price 	= $val1->last;
							$volume	= $val1->volume;
							if(empty($availdata)){
								$sourceurl	= $apiurl;
								$availdata = $val1;
							}
							//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update(['price' => $price,'volume'=>$volume]);						
							$this->updatecoindata($exchange_id,$market,$val1->last,$val1->last*$val1->volume,$url);					
						}
					}
					break;
			case 43:
					$availdata = array();
					$apiurl = "https://www.cointiger.com/exchange/api/public/market/detail";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					foreach($data as $key=>$val)
					{
						$availdata = $val;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->quoteVolume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);			
					break;
			case 45:
					$url 	= "https://www.rightbtc.com/api/public/tickers";
					myurl($url);
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//pr($data);//exit;
					$i = -1;
					foreach($data->result as $key=>$val){
						//prd($val);
						$i++;
						/*if($val->market == 'BTCUSD'){
							echo '<br>++--++'.$val->market.'----'.$e8*$val->last.'+++++++'.$e8*$val->vol24h;
							echo '++++++++++++<pre>';print_r($val);exit;
						}*/
						$updatedata['price'] 		= $e8*$val->last;
						$vol = $e8*$val->vol;
						if($vol<=999999999){
							$updatedata['volume'] 		= $vol;
						}else{
							$updatedata['bigvolume'] 	= $vol;
						}	
						$wherecond['exchange_id']	=	$exchange_id;
						$wherecond['market_name']	=	$val->market;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						$price 	= $e8*$val->last;
						$volume = $e8*$val->last*$e8*$val->vol24h;
						$this->updatecoindata($exchange_id,$val->market,$price,$volume,$url);
					}			
					break;
			case 46:
					$url 			= "https://www.paribu.com/ticker";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					//echo '<pre>';print_r($data);
					
					/*echo $this->chkexchangemarket($exchange_id);exit;
					if($this->chkexchangemarket($exchange_id))
					{
						$i = -1;
						foreach($data as $key=>$val){
							$i++;
							//echo '<pre>';print_r($val);exit;
							$arr[$i]['exchange_id'] = $exchange_id;
							$arr[$i]['market_name'] = $key;
						}
						echo '<pre>';print_r($arr);exit;
						DB::table('exchanges_market')->insert($arr);
					}
					echo '+++';exit;*/
					
					foreach($data as $key=>$val)
					{
						//echo '<pre>';print_r($val);
						$market	= $key;
						$price 	= $val->last;
						$volume	= $val->avg24hr;
						if(empty($val)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update(['price' => $price,'volume'=>$volume]);						
						$this->updatecoindata($exchange_id,$market,$val->last,$val->last*$val->volume,$url);
						//$this->updatecoindata($exchange_id,$market,$val->last,$val->last*$val->volume,$url);
					}
					break;
			case 48:
				$url 	= "https://data.gateio.io/api2/1/marketlist";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->data);
				foreach($data->data as $key=>$val){
					/*if(!in_array($val->pair,$exchange_market))
					{
						$i++;
						$arr[$i]['exchange_id'] = $exchange_id;
						$arr[$i]['market_name'] = $val->pair;;
						$arr[$i]['symbol1'] 	= $val->curr_a;;
						$arr[$i]['symbol2'] 	= $val->curr_b;;
					}*/
					$price 	= str_replace(',','',$val->rate);
					$volume = str_replace(',','',$val->vol_b);
					$this->updatecoindata($exchange_id,$val->pair,$price,$volume,$url);						
				}
				//prd($arr);				
				break;
			case 49:
				$url 	= "https://app.tradebytrade.com/api/coin-rates";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					//prd($val);
					$this->updatecoindata($exchange_id,$key,$val->price,$val->volume,$url);	
				}
				//prd($arr);
				break;				
			case 50:
					$url 	= "https://www.zebapi.com/api/v1/market/";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $val){
						//pr($val);
						$updatedata['price'] 		= $val->market;
						$updatedata['volume']	 	= $val->volume;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->pair;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						
						DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$val->pair,$val->market,$val->volume,$url);	
					}
					break;
			case 51:
					$url	= "https://www.bitinka.com/api/apinka/ticker?format=json";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data);
					foreach($data as $key=>$val){
						foreach($val as $val_1)
						{
							//pr($val_1);
							$this->updatecoindata($exchange_id,$val_1->symbol,$val_1->lastPrice,$val_1->lastPrice*$val_1->volumen24hours,$url);
						}
					}
					break;
			case 52:
					//$this->setzero($exchange_id);exit;
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->where('symbol2','=','BTC')->where('id','<',8525)->orderBy('lastupdate', 'asc')->take(20)->get();
					//$res	= $this->exchangeMarket($exchange_id,10);
					
					$i = rand(1,3);
					//$i = 2;
					echo '-->>>'.$i.'<<<--';
					if($i == 1){
						$str = "'eth_btc','dash_btc','zec_btc','lsk_btc','etc_btc','ltc_btc','waves_btc','btc_usd','fto_btc','eth_usd','doge_btc','kbc_btc','edr2_btc','ltc_usd','ufr_btc','molk_btc','kbc_eth','bunny_btc','edr2_usd','trx_btc','edrc_btc','noah_btc','fto_usd','trx_eth','trx_usd','echt_btc','doge_usd','gcr_btc','club_btc','nbc_usd','zec_usd','noah_eth','doge_eth','rdd_btc','btg_btc','waves_usd','ltc_eth','kin_eth','dgb_btc','bchabc_btc','xvg_btc','etc_usd','crc_btc','lrc_btc','hur_btc','eos_btc','rntb_btc','bchabc_usd','xhi_btc','rntb_eth','hur_eth','rntb_usd','kin_btc','dgb_eth','btg_usd','xvg_usd','plc_usd','ieth_btc','game_btc','fto_eth','evn_btc','spd_btc','plc_btc','dgb_usd','nyc_eth','pac_btc','mao_btc','eos_usd','exmr_eth','unify_btc','unit_btc','inxt_btc','lana_btc','nlc2_btc','nbc_btc','spd_eth','aur_btc','dash_usd','gnt_btc','nyc_usd','nyc_doge','tokc_btc','xyo_btc','pat_btc','prg_btc','bchabc_eth','molk_eth','lsk_usd','ponzi_btc','exmr_btc','pyn_btc','onx_btc','nexo_btc','btg_eth','ieth_eth','mntp_btc','hand_eth','via_btc','eos_eth','max_btc','zec_eth','xvg_eth','evn_eth','bchsv_btc','xem_usd','nyc_btc','bsty_btc','dcr_btc','xvg_doge','linda_btc','wit_btc','ucash_btc','xem_btc','cnx_btc','wgr_btc','ieth_usd','nanox_btc','flot_btc','kin_usd','dft_btc','pma_btc','elite_btc','pivx_btc','bchsv_eth','prix_btc','plc_eth','ubex_btc','cat_btc','xmg_btc','brat_btc','lcc_btc','nmc_btc','pac_usd','fntb_btc','atl_btc','lcc_usd','tbx_usd','prg_usd','rdd_eth','r_btc','ims_btc','neva_btc','iqn_eth','ubex_eth','bio_btc','tag_btc','comp_eth','ecash_btc','modx_btc','bunny_eth','rbbt_usd','srn_btc','netko_btc','nbc_eth','latx_btc','cxt_btc','rdd_usd','sbtc_btc','axiom_btc','ntk_btc','taj_btc','btg_doge','hkn_btc','crc_usd','ixt_btc','mtc_btc','boli_btc','sbtc_usd','poll_btc','vtc_btc','pyn_eth','sat_btc','ucash_eth','clam_btc','duo_btc','btcred_usd','start_btc','ldoge_doge','wit_eth','snr_btc','xqn_btc','flot_eth','dft_usd','lina_btc','ppc_btc','mco_btc','lbtcx_usd','prg_doge','dlt_btc','icob_btc','socc_btc','cnt_btc','sntr_doge','btcred_btc','tbx_btc','iqn_usd','blue_btc','milo_btc','hpc_btc','cj_btc','arcx_btc','ibank_btc','swing_btc','tbx_eth','pac_doge','pac_eth','wtl_btc','moto_btc','benji_btc','tds_usd','linda_eth','tell_btc','ift_btc','tds_btc','aces_btc','ams_btc','zyd_btc','atb_btc','adz_btc','conx_btc','bern_btc','posw_btc','btcm_btc','chsb_btc','luna_btc','opal_btc','ldoge_usd','occ_eth','buzz_doge','latx_eth','snm_btc','gusd_usd','bouts_btc','fjc_btc','storm_btc','xyo_usd','kick_btc','xem_eth','hmq_btc','tokc_usd','iflt_doge','drm_btc','song_btc','frst_btc','guess_btc','carbon_btc','b2x_usd','acoin_btc','crc_eth','bcd_btc','wbb_btc','smc_btc','mco_usd','iqn_btc','robet_btc','brat_usd','boli_usd','pak_btc','unify_usd','eco_btc','rbt_btc','rbbt_btc','bouts_eth','cmt_btc','cpc_btc','pxi_btc','covx_btc','bits_btc','exp_btc','exmr_usd','trump_btc','argus_btc','cct_btc','lcc_eth','god_btc','cl_btc','bub_btc','dlc_btc','v_btc','dalc_btc','xbtc21_btc','zet_btc','pnd_usd','alis_btc','buzz_eth','dai_btc','sig_btc','creva_btc','cred_btc','ping_btc','frn_btc','cann_btc','wink_btc','uni_btc','stu_btc','vec2_btc','bta_btc','c2_btc','vslice_btc','b2x_btc','time_btc','hvco_btc','zeni_btc','socc_doge','pma_doge','ucash_usd','zeit_usd','storm_usd','bouts_doge','iflt_usd','hkn_eth','ldoge_eth','sntr_eth','crc_doge','dft_eth','guess_doge','horse_eth','pma_eth','sandg_eth','mco_eth','xco_btc','cms_btc','bouts_usd','sib_btc','prg_eth','hkn_usd','ent_btc','plbt_btc','xpy_btc','robet_eth','gb_btc','ely_usd','ixc_btc','cnx_usd','dem_btc','ecob_btc','cf_btc','ubex_doge','rise_btc','neu_btc','loom_btc','xra_btc','socc_usd','xptx_btc','drt_btc','tek_btc','wrc_eth','cjt_eth','dbet_btc','comp_btc','xjo_btc','may_btc','luc_eth','bio_usd','iflt_btc','lbtcx_btc','sent_btc','pnd_btc','modx_usd','blue_eth','plnc_btc','dmt_btc','blazr_btc','pkb_btc','locx_eth','vprc_btc','post_btc','horse_usd','met_btc','nlg_btc','tns_btc','tx_btc','vlt_btc','arco_btc','qbc_btc','frst_eth','cnnc_btc','rec_btc','buzz_usd','kurt_btc','hmc_btc','ely_btc','pma_usd','icon_btc','ing_btc','ctic2_btc','sys_btc','ubex_usd','rbt_usd','talk_btc','btcm_usd','pex_btc','mntp_usd','carbon_usd','abyss_eth','occ_btc','cms_eth','lunyr_btc','snrg_btc','evn_usd','mojo_btc','blu_btc','lina_eth','super_btc','ntk_eth','volt_btc','cjt_btc','wrc_btc','wtl_doge','cab_btc','tit_btc','mntp_eth','zipt_btc','sandg_btc','dai_usd','bitb_btc','ely_doge','scrt_btc','xyo_eth','ltcu_btc','gst_eth','locx_btc','rby_btc','swt_btc','evil_btc','loom_usd','guess_eth','ele_btc','fuzz_btc','2give_btc','rbies_btc','hvco_eth','gusd_doge','women_btc','ccl_btc','prs_btc','euc_btc','atmcha_btc','ldoge_btc','xpd_btc','adc_btc','knc_btc','pmnt_btc','nvc_btc','abyss_btc','sls_btc','all_btc','sigt_btc','inxt_usd','b2b_btc','zur_btc','usc_btc','pak_usd','arct_btc','fntb_eth','guess_usd','rec_usd','bio_eth','frst_usd','gsr_btc','kobo_btc','robet_doge','spr_btc','ely_eth','otn_btc','av_btc','gst_btc','snc_btc','tds_eth','horse_btc','loom_eth','bts_btc','cloak_btc','crw_btc','mst_btc','ert_btc','ubtc_btc','luc_btc','bsc_btc','icon_usd','vidz_btc','tie_btc','cat_usd','chess_btc','ltcr_btc','shdw_btc','arb_btc','lion_btc','met_eth','tns_eth','ind_btc','gcc_btc'";
						$sql = "select * from `exchanges_market` where exchange_id= 52 and market_name in (".$str.") order by lastupdate limit 10";
						$res = DB::select($sql);
					}else{
						$res 	= $this->exchangeMarket($exchange_id,10);
					}
					foreach($res as $val)
					{
						$url 	= "https://yobit.net/api/3/ticker/".$val->market_name;
						$data 	= file_get_contents($url);
						$data	= json_decode($data);
						//echo $url;
						//echo '<pre>';print_r($data);exit;
						$i = -1;
						foreach($data as $key=>$val1){
							//echo '+**+';pr($val1);echo '+**+';//vol_cur
							$this->updatecoindata($exchange_id,$val->market_name,$val1->avg,$val1->vol,$url);
							$i++;
							/*//echo $key.'<pre>';print_r($val);exit;
							$price 	= $val->last;
							$volume	= $val->vol_cur;
							if(empty($val)){
								$sourceurl	= $url;
								$availdata 	= $val;
							}
							//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$key])->update(['price' => $price,'volume'=>$volume]);
							*/
							echo '++++****++++****';
						}
					}
					break;
			case 53:
				$url	= "https://api.lakebtc.com/api_v2/ticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				foreach($data as $key=>$val){
					/*if(!in_array($key,$exchange_market))
					{
						$i++;
						$arr[$i]['exchange_id'] = $exchange_id;
						$arr[$i]['market_name'] = $key;;
						$arr[$i]['symbol1'] 	= substr($key,0,3);
						$arr[$i]['symbol2'] 	= substr($key,3);
					}*/
					//prd($val);
					$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->volume,$url);
				}
				//prd($arr); 
				break;					
			case 55:
					$url 	= 'https://api.gopax.co.kr/trading-pairs/';//ETH-KRW/stats';
					$res	= $this->exchangeMarket($exchange_id,15);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name.'/stats';
						//echo $apiurl;//	exit;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//echo '<pre>';print_r($data);echo '</pre>';//exit;
						$availdata = $data;
						$this->updatecoindata($exchange_id,$val->market_name,$data->close,$data->close*$data->volume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 56:
					$url 	= 'https://bitbay.net/API/Public/';//"DASHBTC/ticker.json';
					$res	= $this->exchangeMarket($exchange_id,15);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name.'/ticker.json';
						//echo $apiurl;//	exit;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//echo '<pre>';print_r($data);echo '</pre>';//exit;
						if(isset($data->message)){
							$this->updatecoindatainactive($exchange_id,$val->market_name);
						}else{
							$availdata = $data;
							$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);
						}
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);					
					break;
			case 59:			
				$url 	= "https://www.fatbtc.com/m/allticker/1/1549126133000";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->data);
				foreach($data->data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->dspName,$val->close,$val->close*$val->volume,$apiurl);
				}
				//prd($arr);
				break;	
			case 60:
				/*$xyz = array();
				$sql = "select * from `exchanges_market` where exchange_id = 60 order by market_name";
				$res = DB::select($sql);
				foreach($res as $val){
					if(in_array($val->market_name,$xyz)){
						//$market_name = $val->market_name;
						//$sql = "update `exchanges_market` set market_name = '".strtolower($val->market_name)."' where exchange_id = 60 and id = ".$val->id;
						//echo '<br><br>'.$sql;
						//DB::update($sql);
						
						$sql = "delete from `exchanges_market` where exchange_id = 60 and id = ".$val->id;
						echo '<br><br>'.$sql;
						DB::delete($sql);
					}else{
						$xyz[] = $val->market_name;
					}
				}
				exit;*/
			
				$url 	= "https://api.exrates.me/openapi/v1/public/ticker?currency_pair=";//"btc_usd";
				$res	= $this->exchangeMarket($exchange_id,15);
				foreach($res as $val)
				{
					$apiurl = $url.strtolower($val->market_name);
					echo "<br><br>".$apiurl;//	exit;
					$data 	= file_get_contents($apiurl);
					$data	= json_decode($data);
					pr($data);
					echo '-------------------';
					//continue;
					if(isset($data[0])){
						$data = $data[0];
						pr($data);
						$availdata = $data;
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->baseVolume,$apiurl);
					}
				}
				break;				
			
					/*$url 	= "https://exrates.me/openapi/v1/public/ticker";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val){
						$i++;
						//echo '<pre>';print_r($val);exit;
						$updatedata['price'] 		= $val->last;
						if($val->quoteVolume<=999999999){
						$updatedata['volume'] 		= $val->quoteVolume;
						}else{
						$updatedata['bigvolume'] 		= $val->quoteVolume;
						}	
						$wherecond['exchange_id']	=	$exchange_id;
						$wherecond['market_name']	=	$val->name;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
					}
					break;*/			
			case 61:
					$url 	= "https://www.btcturk.com/api/ticker";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $val){
						$price 	= $val->last;
						$volume	= $val->volume;						
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->pair])->update(['price' => $price,'volume'=>$volume]);
						
						$this->updatecoindata($exchange_id,$val->pair,$val->last,$val->last*$val->volume,$url);
					}
					break;
			case 62:
				$url	= "https://api.kucoin.com/v1/open/tick";
				$url = "https://api.kucoin.com/api/v1/market/allTickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				foreach($data->data->ticker as $key=>$val){
					//$this->updatecoindata($exchange_id,$val->symbol,$val->lastDealPrice,$val->lastDealPrice*$val->vol,$url);
					$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->vol,$url);
				}
				prd($data);
				break;					
			case 63:
					$this->setzero($exchange_id);exit;
					$url 	= "https://www.cryptopia.co.nz/api/GetMarkets";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data->Data as $val){
						//echo '<pre>';print_r($val);exit;
						$market	= $val->Label;
						$price 	= $val->LastPrice;
						$volume	= $val->Volume;	
						if($volume<=999999999){
							$updatedata['price'] 		= $price;
							$updatedata['volume']		= $volume;
						}else{
							$updatedata['price'] 		= $price;
							$updatedata['bigvolume']	= $volume;
						}
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update($updatedata);
					}
					break;
			case 64:
					//$this->setzero($exchange_id);exit;
					$url 	= "https://bx.in.th/api/";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $val)
					{
						$this->updatecoindata($exchange_id,$val->pairing_id,$val->last_price,$val->last_price*$val->volume_24hours,$url);
					}
					/*
					echo '<pre>';print_r($data);exit;
					
					//https://bx.in.th/api/tradehistory/?pairing=1&date=2014-10-21
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(5)->get();
					$res	= $this->exchangeMarket($exchange_id,15);
					foreach($res as $val)
					{
						$url 	= "https://bx.in.th/api/tradehistory/?date=".date("Y-m-d")."&pairing=".$val->market_name;
						$data 	= file_get_contents($url);
						$data	= json_decode($data);
						//echo '<br><br>'.$url.'<br><pre>';print_r($data->data);exit;
						$i = -1;
						$updatedata['price'] 	= $data->data->avg;
							$updatedata['volume'] 	= $data->data->volume;
							$wherecond['exchange_id']	=	$exchange_id;
							$wherecond['market_name']	=	$val->market_name;
							if(empty($availdata)){
								$sourceurl	= $url;
								$availdata 	= $val;
							}
						//	DB::table('exchanges_market')->where($wherecond)->update($updatedata);///exit;
						//$this->updatecoindata($exchange_id,$val->market_name,1,1,$url);
						$this->updatecoindata($exchange_id,$val->market_name,$data->data->avg,$data->data->avg*$data->data->volume,$url);
					}*/	
					break;
			case 67:
					$updatearr	= array();
					$url 		= "https://broker.negociecoins.com.br/api/v3/";//"btcbrl/ticker";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name."/ticker";
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$availdata 	= $data;
						//prd($data);
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->vol,$apiurl);
					}
					break;
			case 72:
					$url	= "https://api.quadrigacx.com/v2/ticker?book=";
					$res	= $this->exchangeMarket($exchange_id,15);
					foreach($res as $val)
					{
						if(strlen($val->market_name)<3){
							continue;
						}
						$apiurl = $url.$val->market_name;
						echo $apiurl.'<br>';
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//echo '<pre>';print_r($data);exit;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->market_name;						
						$updatedata['price'] 		= $data->last;
						if($data->vwap<=999999999){
							$updatedata['volume']	 	= $data->vwap;
						}else{
							$updatedata['bigvolume']	= $data->vwap;
						}
						if(empty($availdata)){
							$sourceurl	= $apiurl;
							$availdata 	= $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						//$this->updatecoindata($exchange_id,$val->market_name,1,2,$apiurl);
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);
					}	
					//$this->setzero($exchange_id);
					break;

case 76: 
$apiurl	= "https://api.bitso.com/v3/ticker/";
$data 	= file_get_contents($apiurl);
$data	= json_decode($data);
//prd($data->payload);
foreach($data->payload as $key=>$val)
{
	$this->updatecoindata($exchange_id,$val->book,$val->last,$val->last*$val->volume,$apiurl);
}
break;
					
			case 77:
					//Exchange BitMEX :: Already discuss with Timi, and approved ::
					//The BTC/USD market on BitMEX is a derivatives market NOT actually spot trading Bitcoin. As a result, it has been excluded from the price and volume averages of Bitcoin.
					break;
			case 78;
				//Mona/Jpy not working... only BTC/Jpy working
				$url 	= "https://api.fcce.jp/api/1/ticker/btc_jpy";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				$this->updatecoindata($exchange_id,'BTC/JPY',$data->last,$data->last*$data->volume,$url);
				break;			
			case 79:
					//$this->setzero($exchange_id);exit;
					$updatearr	= array();
					$url 		= "https://api.zaif.jp/api/1/ticker/";//"sjcx_btc";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$availdata 	= $data;
						//prd($data);
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);						
					}
					//$this->availabledata($exchange_id,$apiurl,$availdata);
					break;					
			case 86:
				$url 	= "https://sistemkoin.com/api/market/ticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data->data as $key=>$val){
					foreach($val as $val_1){
						$this->updatecoindata($exchange_id,$val_1->short_code.'/'.$val_1->currency,$val_1->current,$val_1->current*$val_1->volume,$url);
					}
				}
				//prd($arr);
				break;
			case 88:
					$this->setzero($exchange_id);exit;
					$url 	= "https://btc-alpha.com/api/v1/exchanges/?format=json";
					$url	= "https://btc-alpha.com/api/charts/BTC_USD/D/chart/";
					$url	= "https://btc-alpha.com/api/charts/BTC_USD/240/chart/";
					$data	= file_get_contents($url);
					$data 	= json_decode($data);
					echo $url.'<br><br>';
					$totalvolume = 0;
					
					foreach($data as $val)
					{
						$totalvolume += $val->volume;						
					}
					//echo $totalvolume.'<br><br>';;
					//echo '+++++++++++++++';pr($data);exit;
					$this->updatecoindata($exchange_id,$val->market_name,$data->data->avg,$data->data->avg*$data->data->volume,$url);
					$this->availabledata($exchange_id,$url,$availdata);					
					break;
			case 89:
					//$this->setzero($exchange_id);exit;
					$updatearr	= array();
					$url 		= "https://api.c2cx.com/v1/ticker/?symbol=";//"DRG_BTC";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$availdata 	= $data;
						//prd($data);
						$this->updatecoindata($exchange_id,$val->market_name,$data->data->last,$data->data->last*$data->data->volume,$apiurl);						
					}
					//$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 93:
					//$this->setzero($exchange_id);exit;
					$updatearr	= array();
					$url 		= "https://api.ethfinex.com/v1/pubticker/";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						if($val->market_name == 'ethusd'){
							continue;
						}
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$availdata 	= $data;
						$this->updatecoindata($exchange_id,$val->market_name,$data->last_price,$data->last_price*$data->volume,$apiurl);						
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 94:
				echo $url 	= "https://upticks.io/apiupticks/curl_cryptonex.php";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				$data 	= $data->result->rates;
				//prd($data);
				foreach($data as $key=>$val){
					/*if(!in_array($val->alias,$exchange_market))
					{
						$i++;
						$x = explode("/",$val->alias);
						$arr[$i]['exchange_id'] = $exchange_id;
						$arr[$i]['market_name'] = $val->alias;;
						$arr[$i]['symbol1'] 	= $x[0];
						$arr[$i]['symbol2'] 	= $x[1];
					}*/
					$this->updatecoindata($exchange_id,$val->alias,$val->last_price,$val->last_price*$val->value_last_24h,$apiurl);						
				}
				//prd($arr);
				break;					
			case 95:
					$url 			= "https://coinsquare.com/api/v1/data/quotes";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					prd($data);
					$this->setzero($exchange_id);exit;
					echo '<br><br>';
					foreach($data->quotes as $key=>$val)
					{
						$market_name	= $val->ticker.'/'.$val->base;// //str_replace('-','/',$key);						
						if(empty($val)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						
						$v = 0;
						$b = 0;
						
						if(!empty($val->volume)){
							$v = $val->volume/100000000;
						}
						if(!empty($val->volbase)){
							$vb = $val->volbase/100000000;
						}
						
						if($market_name == 'CAD/BTC'){
							$this->updatecoindata($exchange_id,$market_name,$val->last,$vb,$url);	
						}
						else{
							if($v>0){
								echo '----'.$market_name.'--- P : '.$val->last.'--- --- V : '.$v.'--- --- --- --- --- --- --- '.$vb.'<br>';
								
								$this->updatecoindata($exchange_id,$market_name,$val->last,$v,$url);	
							}
						}
						
						
					}
					exit;
					break;
			case 96:
				$url = "https://www.chaoex.info/unique/quote/v2/allRealTime";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->attachment);
				foreach($data->attachment as $key=>$val){
					/*if(!in_array($val->tradeCurrencyName.'/'.$val->baseCurrencyName,$exchange_market))
					{
						//prd($val);
						$i++;
						$arr[$i]['exchange_id'] = $exchange_id;
						$arr[$i]['market_name'] = $val->tradeCurrencyName.'/'.$val->baseCurrencyName;
						$arr[$i]['symbol1'] 	= $val->tradeCurrencyName;
						$arr[$i]['symbol2'] 	= $val->baseCurrencyName;
					}
					*/
					$this->updatecoindata($exchange_id,$val->tradeCurrencyName.'/'.$val->baseCurrencyName,$val->last,$val->last*$val->vol,$url);	
				}
				//prd($arr);
				break;					
			case 97:
					$url 	= "https://mercatox.com/public/json24";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data->pairs as $key=>$val){
						$i++;
						//echo '<pre>';print_r($val);exit;
						$updatedata['price'] 	= $val->last;
						$updatedata['volume'] 	= $val->baseVolume;
						$wherecond['exchange_id']	=	$exchange_id;
						$wherecond['market_name']	=	$key;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->baseVolume,$url);	
					}
					break;
			case 99:
					$url 	= "https://us-central1-vebitcoin-market.cloudfunctions.net/app/api/ticker";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val){
						$i++;
						//echo '<pre>';print_r($val);exit;
						$updatedata['price'] 	= $val->Price;
						$updatedata['volume'] 	= $val->Volume;
						$wherecond['exchange_id']	=	$exchange_id;
						$wherecond['market_name']	=	$val->SourceCoinCode.'/'.$val->TargetCoinCode;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						
						$market_name	=	$val->SourceCoinCode.'/'.$val->TargetCoinCode;
						$this->updatecoindata($exchange_id,$market_name,$val->Price,$val->Volume,$url);
					}
					break;

case 100:
$url	= "https://api.cobinhood.com/v1/market/tickers";
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data->result->tickers);
foreach($data->result->tickers as $key=>$val){
	$vol = '24h_volume';
	$this->updatecoindata($exchange_id,$val->trading_pair_id,$val->last_trade_price,$val->last_trade_price*$val->$vol,$url);
}
//prd($arr);
BREAK;
					
			case 103:	
					$url 			= "https://koineks.com/ticker";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					foreach($data as $val){
						$availdata = $val;
						$this->updatecoindata($exchange_id,$val->short_code.'/'.$val->currency,$val->current,$val->current*$val->volume,$url);
						//$this->updatecoindata($exchange_id,$val->short_code.'/'.$val->currency,$val->current,$val->current*$val->volume,$url);
						//$this->updatecoindata($exchange_id,$val->short_code.'/'.$val->currency,1,2,$url);
					}
					$this->availabledata($exchange_id,$url,$availdata);
					break;

case 107:

$url	= 'https://bitbns.com/order/getTickerWithVolume/';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//echo count($data);
//prd($data);
foreach($data as $key=>$val){
	$market = $key;
	if(strlen($key) < 7){
		$market = $key.'/INR';
	}
	else{
		$market = substr($key,0,3).'/'.substr($key,3);
	}
	$this->updatecoindata($exchange_id,$market,$val->last_traded_price,$val->last_traded_price*$val->volume->volume,$url);
}
BREAK;
/*
$sql = "select * from `exchanges_market` where exchange_id = 107 and market_name like '%USDT'";
$res = DB::select($sql);
foreach($res as $val){
	$sql = "update `exchanges_market` set market_name = '".substr($val->market_name,0,3)."/USDT' where exchange_id = 107 and id = ".$val->id;
	echo $sql.';<br>';
}
exit;
$sql = "select * from `exchanges_market` where exchange_id = 107 and market_name like '%INR'";
$res = DB::select($sql);
foreach($res as $val){
	if(strlen($val->market_name) == 6)
	$sql = "update `exchanges_market` set market_name = '".substr($val->market_name,0,3)."/INR' where exchange_id = 107 and id = ".$val->id;

	if(strlen($val->market_name) == 7)
	$sql = "update `exchanges_market` set market_name = '".substr($val->market_name,0,4)."/INR' where exchange_id = 107 and id = ".$val->id;
	if(strlen($val->market_name) == 8)
	$sql = "update `exchanges_market` set market_name = '".substr($val->market_name,0,5)."/INR' where exchange_id = 107 and id = ".$val->id;
	echo $sql.';<br>';
}
exit;
*/
					
case 114:
$url	= 'https://api.ovis.com.tr/public/tickers';
$data 	= file_get_contents($url);
$data	= json_decode($data);
foreach($data->data as $key=>$val){
	$this->updatecoindata($exchange_id,$val->id,$val->last,$val->last*$val->volume,$url);
}
BREAK;
					
					
			case 115:
					$url = "https://bitsane.com/api/public/ticker";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val){
						$i++;
						//echo '<pre>';print_r($val);exit;
						$updatedata['price'] 		= $val->last;
						$updatedata['volume'] 		= $val->quoteVolume;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $key;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//echo '<pre>';print_r($updatedata);exit;
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->quoteVolume,$url);						
						//$this->updatecoindata($exchange_id,$key,$val->last,5,$url);						
					}										
					break;
case 117:
$url	= "https://stellar.api.stellarport.io/Ticker";
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	//prd($val);
	$this->updatecoindata($exchange_id,$key,$val->close,$val->volume,$url);	
}
//prd($arr);
BREAK;					
					
case 121:
$url	= "https://api.therocktrading.com/v1/funds/tickers";
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data->tickers);
foreach($data->tickers as $key=>$val){
	
	$this->updatecoindata($exchange_id,$val->fund_id,$val->last,$val->volume,$url);
}
//prd($arr);
BREAK;				
					
					
			case 123:
					$url 	= "https://www.bitmarket.pl/json/";//"ETHPLN/ticker.json";
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(2)->get();
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name."/ticker.json";;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//echo '<pre>';print_r($data);exit;
						
						$updatedata['price'] 		= $data->last;
						$updatedata['volume'] 		= $data->volume;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->market_name;
						if(empty($val)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//echo '<pre>';print_r($wherecond);exit;
						//echo '</pre><pre>';print_r($updatedata);exit;
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);
					}					
					break;
					
case 125:
	$url = 'https://cryptomate.co.uk/api/all/';
	$data 	= file_get_contents($url);
	$data	= json_decode($data);
	//prd($data);
	foreach($data as $key=>$val){
		$this->updatecoindata($exchange_id,strtoupper($key).'/GBP',$val->price,$val->volume,$url);
	}
	break;					
			case 127:
				$url 	= "https://coinfalcon.com/api/v1/markets";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//$data 	= $data->result->rates;
				//prd($data->data);
				//echo '+++here++++';
				foreach($data->data as $key=>$val){
					//prd($val);
					$this->updatecoindata($exchange_id,$val->name,$val->last_price,$val->volume,$url);
					//exit;
				}
				break;					
			case 130:
					//$this->setzero($exchange_id);exit;
					$url 	= "https://coinbe.net/public/graphs/ticker/ticker.json";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val){
						$updatedata['price'] 		= $val->last;
						if($val->volume<=999999999){
							$updatedata['volume']		= $val->volume;
						}else{
							$updatedata['bigvolume']	= $val->volume;
						}
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $key;
						if(empty($val)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//echo '+++++++++++++++++++++++';exit;
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->volume,$url);
					}			
					break;
			case 131:
					$url 	= "https://bitlish.com/api/v1/tickers";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val){
						//echo $key;
						//prd($val);
						$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->sum,$url);
						//echo '<hr><hr>';
					}			
					break;
case 133:
$res	= $this->exchangeMarket($exchange_id,15);
foreach($res as $key=>$val)
{
	$apiurl	= "https://www.mercadobitcoin.net/api/".$val['symbol1']."/ticker";
	$data 	= file_get_contents($apiurl);
	$data	= json_decode($data);
	$this->updatecoindata($exchange_id,$val['market_name'],$data->ticker->last,$data->ticker->last*$data->ticker->vol,$apiurl);
}				
break;			
					
			case 135:
					$updatearr	= array();
					$url 		= "https://dsx.uk/mapi/ticker/";//"btcusd";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						//$apiurl = "https://dsx.uk/mapi/ticker/btcusd";
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$availdata 	= $data;
						foreach($data as $key=>$val){
							$this->updatecoindata($exchange_id,$key,$val->last,$val->vol_cur,$apiurl);
						}
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 139:
				$url 	= "https://big.one/api/v2/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->data);
				foreach($data->data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->market_id,$val->close,$val->close*$val->volume,$url);
				}
				break;

case 142:
$url	= 'https://tradesatoshi.com/api/public/getmarketsummaries';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data->result);
foreach($data->result as $key=>$val){
	if($val->volume>0){
		$this->updatecoindata($exchange_id,$val->market,$val->last,$val->last*$val->volume,$url);
	}
}
//prd($arr);
BREAK;				
				
case 143:
$url	= 'https://api.crypto-bridge.org/api/v1/ticker';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	$this->updatecoindata($exchange_id,$val->id,$val->last,$val->volume,$url);
}
//prd($arr);
BREAK;				
				
			case 147:
					$url 	= "https://www.southxchange.com/api/prices";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $val){
						$market	= $val->Market;
						$price 	= $val->Last;
						$volume	= $val->Volume24Hr;	
						if(empty($val)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$market,$price,$price*$volume,$url);					
					}
					break;
case 150:

$res 		= $this->exchangeMarket($exchange_id,1);
foreach($res as $val)
{
	$apiurl = "https://bit2c.co.il/Exchanges/".$val->symbol1."/ticker.json";
	$data	= file_get_contents($apiurl);
	$data	= json_decode($data);
	$this->updatecoindata($exchange_id,$val->symbol1.'/ILS',$data->ll,$data->a,$apiurl);
}
break;
/*

/*
$sql = "DELETE FROM exchanges_market where exchange_id = 150";
DB::delete($sql);
$res = App\ExchangeMarket::where('exchange_id','=', $exchange_id)->get();
foreach($res as $val){
	$exchange_market[] = $val;
}
prd($exchange_market);*/

//$url	= 'https://bit2c.co.il/home/api?language=en-US#trades';
$data = array("BtcNis","EthNis","BchabcNis","LtcNis","EtcNis","BtgNis","BchsvNis","GrinNis");
//prd($data);
foreach($data as $key=>$val){
	if(!in_array($val.'/ILS',$exchange_market))
	{
		$i++;
		$x = explode("_",$val);
		$arr[$i]['exchange_id'] = $exchange_id;
		$arr[$i]['market_name'] = $val.'/ILS';
		$arr[$i]['symbol1'] 	= $val;
		$arr[$i]['symbol2'] 	= 'ILS';
	}
}
//prd($arr);
BREAK;									
			case 152:
				exit;
				$url 	= "https://exchange.coss.io/api/getmarketsummaries";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				foreach($data->result as $key=>$val){
					$this->updatecoindata($exchange_id,$val->MarketName,$val->Last,$val->Last*$val->Volume,$url);	
				}
				break;		
case 154:
$url	= 'https://bleutrade.com/api/v2/public/getmarketsummaries';
$data 	= file_get_contents($url);
$data	= json_decode($data);
foreach($data->result as $key=>$val){
	$this->updatecoindata($exchange_id,$val->MarketName,$val->Last,$val->Last*$val->Volume,$url);	
}
BREAK;
case 156:
//$url	= 'https://www.koinim.com/api/v1/market/';
//$data = array("BTC_TRY", "LTC_TRY", "BCH_TRY", "ETH_TRY", "DOGE_TRY", "DASH_TRY");
$res = $this->exchangeMarket($exchange_id,1);
foreach($res as $val){
	echo $url	= "https://www.koinim.com/api/v1/ticker/".$val['market_name'];
	$data 	= file_get_contents($url);
	$data	= json_decode($data);
	//$data 	= $data->result->rates;
	$this->updatecoindata($exchange_id,$val->market_name,$data->wavg,$data->wavg*$data->volume,$url);	
	
}
//prd($arr);
BREAK;

case 161:
$url	= 'https://tradeogre.com/api/v1/markets';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	foreach($val as $key=>$val1)
	{
		$this->updatecoindata($exchange_id,$key,$val1->price,$val1->volume,$url);	
	}
}
BREAK;


case 167:
/*
$sql = "select * from `exchanges_market` where exchange_id = 167";
$res = DB::select($sql);
foreach($res as $val){
	$sql = "update `exchanges_market` set market_name = '".str_replace('/','',$val->market_name)."' where exchange_id = 167 and market_name = '".$val->market_name."'";
	DB::update($sql);
}*/

$url	= 'https://public-api.lykke.com/api/Market';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	if($val->volume24H>0){	
		echo '.......'.$val->assetPair;
		$this->updatecoindata($exchange_id,str_replace('/','',$val->assetPair),$val->lastPrice,$val->volume24H,$url);	
	}
}
//prd($arr);
BREAK;				
			case 175:
					$url 	= "https://www.allcoin.ca/Api_Market/getPriceList";
					$data 	= file_get_contents($url);
					if(strlen($data)<10){
						exit;
					}
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					$i = -1;
					foreach($data as $key=>$val)
					{			
						//echo '++++'.$key.'<br>';//exit;
						foreach($val as $val1)
						{
							$wherecond['exchange_id']	= $exchange_id;
							$wherecond['market_name'] 	= $val1->coin_from.'/'.$val1->coin_to;
							$updatedata['price'] 		= $val1->current;
							$updatedata['volume'] 		= $val1->count;
							if(empty($availdata)){
								$sourceurl	= $url;
								$availdata 	= $val1;
							}
							//echo '</pre><pre>';print_r($updatedata);exit;
							//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
							$this->updatecoindata($exchange_id,$val1->coin_from.'/'.$val1->coin_to,$val1->current,$val1->current*$val1->count,$sourceurl);
						}
					}
					$this->availabledata($exchange_id,$sourceurl,$availdata);				
					break;
case 181:
$url	= 'https://api.ddex.io/v3/markets/tickers';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data->data->tickers);
foreach($data->data->tickers as $key=>$val){
	$this->updatecoindata($exchange_id,$val->marketId,$val->price,$val->volume,$url);
}
//prd($arr);
BREAK;					
case 186:
$url	= 'https://api.switcheo.network/v2/tickers/last_24_hours';
$data 	= file_get_contents($url);
$data	= json_decode($data);
foreach($data as $key=>$val){
	$this->updatecoindata($exchange_id,$val->pair,$val->close,$val->volume,$url);	
}
BREAK;


			case 187:
				//$this->setzero($exchange_id);exit;
				$url 	= "https://www.tidebit.com/api/v2/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				//echo '+++here++++';
				foreach($data as $key=>$arr){
					$price 	= $arr->ticker->last;
					$volume = $price*$arr->ticker->vol;
					$this->updatecoindata($exchange_id,$key,$price,$volume,$url);
				}
				//prd($arr);
				break;	
	case 199:
		$url 	= 'https://api.freiexchange.com/returnTicker';
		$data 	= file_get_contents($url);
		$data	= json_decode($data);
		//prd($data);
		foreach($data as $key=>$val){
			foreach($val as $val1){
				$this->updatecoindata($exchange_id,$key,$val1->last,$val1->volume24h_btc,$url);
			}
		}
		//prd($arr);
		break;					
			case 211:
					/*$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take(100)->get();
					foreach($res as $val)
					{
						$arr 			= explode('-',$val->market_name);
						$market_name	= $arr[1].'-'.$arr[2];
						$symbol1		= $arr[1];
						$symbol2		= $arr[2];
						DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'id'=>$val->id])->update(['market_name' => $market_name,'symbol1'=>$symbol1,'symbol2'=>$symbol2]);
					}echoexit; 	*/					
					
					
					
					$url 	= "https://api.bitforex.com/api/v1/market/ticker/?symbol=coin-";//usdt-btc
					$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take($coinlimit)->get();
					$res 	= $this->exchangeMarket($exchange_id,40);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						$updatedata['price']	= $data->data->last;
						$updatedata['volume']	= $data->data->vol;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->market_name;						
						pr($data);//exit;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);
						$this->updatecoindata($exchange_id,$val->market_name,$data->data->last,$data->data->last*$data->data->vol,$apiurl);	
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
				
case 212:
$url	= 'https://tracker.kyber.network/api/tokens/pairs';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	$this->updatecoindata($exchange_id,$key,$val->lastPrice,$val->lastPrice*$val->baseVolume,$url);		
}
//prd($arr);
BREAK;					
			case 290:			
					$url 	= "https://openapi.bitmart.com/v2/ticker";//"btc_usd
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//echo '<pre>';print_r($data);exit;
					foreach($data as $val)
					{//echo '<pre>';print_r($val);exit;
						$updatedata['price'] 		= $val->current_price;
						if($val->volume<=999999999){
							$updatedata['volume']		= $val->volume;
						}else{
							$updatedata['bigvolume']	= $val->volume;
						}
						$wherecond['exchange_id']	= $exchange_id;
						$key = str_replace('https://www.bitmart.com/trade?symbol=','',$val->url);
						$wherecond['market_name']	= $key;
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$key,$val->current_price,$val->current_price*$val->volume,$apiurl);							
					}
					break;

case 312:
$url	= 'https://coinmarketcap.coindeal.com/api/v1/ticker?_ga=2.63668655.1623861622.1553576420-1333532394.1553576420';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	//prd($val);
	$this->updatecoindata($exchange_id,$key,$val->last,$val->quoteVolume,$url);
	
	
}
//prd($arr);
break;
					
			case 342:			
					$url 	= "https://openapi.digifinex.com/v2/ticker?apiKey=15c52a3c545544";			
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data->ticker);
					$i = -1;
					$arr = array();
					foreach($data->ticker as $key=>$val){
							/*$i++;
							$x = explode("_",$key);
							$arr[$i]['exchange_id'] = $exchange_id;
							$arr[$i]['market_name'] = $key;
							$arr[$i]['symbol1'] 	= $x[0];
							$arr[$i]['symbol2'] 	= $x[1];					
							$arr[$i]['marketname'] = strtoupper(str_replace('_','/',$key));*/
							$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->vol,$url);
					}	


					/*$url 	= "https://www.southxchange.com/api/prices";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data as $val){
						$market	= $val->Market;
						$price 	= $val->Last;
						$volume	= $val->Volume24Hr;	
						if(empty($val)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$market])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$market,$price,$price*$volume);					
					}*/					
					break;
			case 347:			
					$url 	= "https://www.okcoin.com/api/v1/ticker.do?symbol=";//"btc_usd
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->where('status','=',1)->orderBy('timestamp', 'asc')->take($coinlimit)->get();
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						$sql11 = "update `exchanges_market` SET market_name = '".str_replace('-','_',$val->market_name)."' where id = ".$val->id;
						//echo $sql11.';<br><br>';continue;
						//echo $apiurl.'<br><br>';
						$data 	= file_get_contents($apiurl);
						if(strlen($data)<20){
							continue;
						}
						$data	= json_decode($data);
						$price	= $data->ticker->last;
						$volume	= $data->ticker->vol;
						//echo '<pre>';print_r($data);exit;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market_name])->update(['price' => $price,'volume'=>$volume]);
						$this->updatecoindata($exchange_id,$val->market_name,$price,$price*$volume,$apiurl);
						////$this->setzero($exchange_id);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 394:
					$url 	= "https://api.dobitrade.com/market/quote?market=";//"mcc_btc";
					$url 	= "https://api.dobiexchange.com/market/quote?market=";//"mcc_btc";
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->orderBy('timestamp', 'asc')->take($coinlimit)->get();
					$res 	= $this->exchangeMarket($exchange_id,7);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						myurl($apiurl);
						$data 	= file_get_contents($apiurl);
						if(strlen($data)<20){
							continue;
						}
						$data	= json_decode($data);
						$updatedata['price']	= $data->data->last;
						$updatedata['volume']	= $data->data->volume;
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->market_name;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$val->market_name,$data->data->last,$data->data->last*$data->data->volume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 454:
					$url 	= "https://api.fcoin.com/v2/market/ticker/";
					
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$apiurl = $url.$val->market_name;
						myurl($apiurl);
						$data 	= file_get_contents($apiurl);
						if(strlen($data)<20){
							continue;
						}
						$data	= json_decode($data);
						$data 	= $data->data->ticker;
						
						echo 'Price :: '.$data[0].'<br>';
						echo 'Volume :: '.$data[10].'<br><br>';
						$this->updatecoindata($exchange_id,$val->market_name,$data[0],$data[10],$apiurl);
					}						
					break;

case 463:
$url	= "https://exchange.tokenomy.com/api/summaries";
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data->tickers);
foreach($data->tickers as $key=>$val){
	$x = explode('_',$key);
	$x = 'vol_'.$x[0];
	$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->$x,$url);
}
//prd($arr);
BREAK;

			case 477:
				$url 	= "https://api.pro.coinbase.com/products";
				$res 	= $this->exchangeMarket($exchange_id,5);
				foreach($res as $key=>$val){
					$apiurl = $url.'/'.$val->market_name.'/stats';
					//$apiurl = $url.'/'.$val->market_name.'/ticker';
					$sql = "update `exchanges_market` set status = 1 where id = ".$val->id;
					echo '<br>'.$sql;
					DB::update($sql);
					echo '<br><br>'.$apiurl.'<br><br>';
					$data 	= file_get_contents($apiurl);
					$data	= json_decode($data);
					//prd($data);
					$this->updatecoindata($exchange_id,$val->market_name,$data->last,$data->last*$data->volume,$apiurl);					
					//$this->updatecoindata($exchange_id,$val->market_name,$data->price,$data->price*$data->volume,$apiurl);					
				}
				//prd($arr);
				break;
			case 531:
					$url 			= "https://bitebtc.com/api/v1/market";
					$data 			= file_get_contents($url);
					$data			= json_decode($data);
					$arr = array();
					foreach($data->result as $val){
						if(empty($val)){
							$sourceurl	= $url;
							$availdata = $val;
						}
						//DB::table('exchanges_market')->where(['exchange_id'=>$exchange_id,'market_name'=>$val->market])->update(['price' => $val->price,'volume'=>$val->volume]);
						$this->updatecoindata($exchange_id,$val->market,$val->price,$val->price*$val->volume,$url);
					}
					break;
			case 616:
				$url = "https://api.bilaxy.com/v1/tickers";
				$data 			= file_get_contents($url);
				$data			= json_decode($data);
				//prd($data);
				foreach($data->data as $val){
					$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->vol,$url);
				}
				break;	
			case 625:
				$url 	= "https://api.kryptono.exchange/v1/getmarketsummaries";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->result);
				foreach($data->result as $key=>$val){					
					$this->updatecoindata($exchange_id,$val->MarketName,$val->Last,$val->Last*$val->Volume,$url);
				}
				//prd($arr);
				break;					
			case 664:
					$url 	= "https://api.cpdax.com/v1/tickers/";//"BTC-KRW/detailed";
					//$res	= App\ExchangeMarket::where('exchange_id','=', $exchange_id)->where('status','=',1)->orderBy('timestamp', 'asc')->take($coinlimit1)->get();
					$res 	= $this->exchangeMarket($exchange_id,10);
					foreach($res as $val)
					{
						$sql = "UPDATE exchanges_market SET status = 0 WHERE exchange_id = ".$exchange_id." and market_name = '".$val->market_name."'";
						DB::update($sql);
						
						$apiurl = $url.$val->market_name."/detailed";;
						$data 	= file_get_contents($apiurl);
						$data	= json_decode($data);
						if(!isset($data->last) or !isset($data->volume) or !isset($data->currency_pair)){
							continue;
						}
						
						$updatedata['price'] 	= $data->last;
						$volume 				= $data->volume;
						if($volume<=999999999){
							$updatedata['volume']	= $volume;
						}else{
							$updatedata['bigvolume']= $volume;
						}
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $data->currency_pair;						
						
						//echo '<pre>';print_r($data);exit;
						if(empty($data)){
							$sourceurl	= $apiurl;
							$availdata = $data;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$sql = "UPDATE exchanges_market SET status = 1 WHERE exchange_id = ".$exchange_id." and market_name = '".$val->market_name."'";
						//DB::update($sql);						
						$this->updatecoindata($exchange_id,$data->currency_pair,$data->last,$data->volume,$apiurl);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;		
			case 716:
				$url 	= "https://api.instantbitex.com/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				///prd($data->combinations);
				foreach($data->combinations as $key=>$val){
					//prd($val);
					$this->updatecoindata($exchange_id,$key,$val->last,$val->volume24hr,$apiurl);
				}
				//prd($arr);
				break;					
			case 775:
				$url 	= "https://api.liquid.com/products";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->currency_pair_code,$val->last_traded_price,$val->last_traded_price*$val->volume_24h,$url);
				}
				break;	
case 847:
$url	= 'https://api.wazirx.com/api/v2/tickers';
$data 	= file_get_contents($url);
$data	= json_decode($data);
//prd($data);
foreach($data as $key=>$val){
	$this->updatecoindata($exchange_id,$val->name,$val->last,$val->last*$val->volume,$url);
}
//prd($arr);
BREAK;		

			case 11311:
				$url = "https://app.stex.com/api2/ticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->market_name,$val->last,$val->last*$val->vol,$url);
				}
				//prd($arr);	
				break;	
				
			case 14580:
					$url 	= "https://api.cryptaldash.com/v1/pubticker/all";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					foreach($data->market_summary as $key=>$val){
						$wherecond['exchange_id']	= $exchange_id;
						$wherecond['market_name']	= $val->symbol;						
						$updatedata['price'] 		= $val->last_price;
						$updatedata['volume']		= $val->volume24h;	
						if(empty($availdata)){
							$sourceurl	= $url;
							$availdata 	= $val;
						}
						//DB::table('exchanges_market')->where($wherecond)->update($updatedata);//exit;
						$this->updatecoindata($exchange_id,$val->symbol,$val->last_price,$val->last_price*$val->volume24h,$url);
					}
					break;
			case 48510:
					$availdata = array();
					$apiurl = "https://www.bitrue.com/kline-api/public.json?command=returnTicker";
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					foreach($data->data as $key=>$val)
					{
						$availdata = $val;
						$this->updatecoindata($exchange_id,$key,$val->last,$val->quoteVolume);
					}
					$this->availabledata($exchange_id,$apiurl,$availdata);
					break;
			case 48512:
					$url 	= "https://p2pb2b.io/api/v2/tickers";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data->datas);
					foreach($data as $key=>$val){
						//echo '<br><br>'.$key;pr($val->ticker->last);prd($val->ticker->last*$val->ticker->vol);
						$this->updatecoindata($exchange_id,$key,$val->ticker->last,$val->ticker->last*$val->ticker->vol,$url);
					}			
					break;
			case 48514:
					$url 	= "https://kline.zbg.com/api/data/v1/tickers?isUseMarketName=true";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data->datas);
					foreach($data->datas as $key=>$val){
						$price 	= $val[1];
						$volume = $val[1]*$val[4]; 						
						$this->updatecoindata($exchange_id,$key,$val[1],$val[1]*$val[4],$url);
					}			
					break;
			case 48515:
					$url 	= "https://bitmax.io/api/v1/ticker/24hr";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data);
					foreach($data as $key=>$val){
						$this->updatecoindata($exchange_id,$val->symbol,$val->closePrice,$val->closePrice*$val->volume,$url);
					}			
					break;
			case 48516:
					$url 	= "https://www.bw.com/api/data/v1/tickers?isUseMarketName=true";
					$data 	= file_get_contents($url);
					$data	= json_decode($data);
					//prd($data->datas);
					foreach($data->datas as $key=>$val){
						$price 	= $val[1];
						$volume = $val[1]*$val[4]; 						
						$this->updatecoindata($exchange_id,$key,$val[1],$val[1]*$val[4],$url);
					}			
					break;
			case 48517:
				//Exchange :: Coineal
				$url 	= "https://exchange-open-api.coineal.com/open/api/symbollist";				
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					/*if(!in_array($key,$exchange_market))
					{
						$i++;
						$x = explode("_",$key);
						$arr[$i]['exchange_id'] = $exchange_id;
						$arr[$i]['market_name'] = $key;
						$arr[$i]['symbol1'] 	= $x[0];
						$arr[$i]['symbol2'] 	= $x[1];
					}*/
					$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->baseVolume,$url);
				}
				break;					
			case 48518:
				$url 	= "https://www.tokok.com/api/v1/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data->ticker);
				$i = -1;
				$arr = array();			
				foreach($data->ticker as $key=>$val){
					$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->vol,$url);
				}
				break;
			case 48519:
			
				$url 	= "https://www.coinmex.com/api/v1/spot/public/products/ETH_BTC/ticker";
				$url 	= "https://www.coinmex.com/api/v1/spot/public/products/";
				$res 	= $this->exchangeMarket($exchange_id,10);
				foreach($res as $val)
				{
					/*echo '<br>';
					$xx = $val->symbol1.'_'.$val->symbol2;
					$sql = "update  `exchanges_market` set market_name = '".$xx."'  where id = ".$val->id;
					echo $sql.'<br><br>';	
					DB::update($sql);*/
					
					
					
					$apiurl = $url.$val->market_name."/ticker";;
					$data 	= file_get_contents($apiurl);
					if(strlen($data)<20){
						continue;
					}
					$data	= json_decode($data);
					//pr($data);
					$data 	= $data;
					$this->updatecoindata($exchange_id,$val->market_name,$data[3],$data[3]*$data[4],$apiurl);
				}				
				break;
			case 48520:
				$url 	= "https://rest.cashierest.com/public/tickerall";
				$data 	= file_get_contents($url);
				$data 	= str_replace('﻿{"Cashierest":','',$data);
				$data	= str_replace('}}}','}}',$data);
				$data 	= '['.$data.']';
				$data	= json_decode($data);
				$data	= $data[0];
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$key,$val->last,$val->last*$val->baseVolume,$apiurl);
				}		
				break;
			case 48522:
			exit;
				// file_get_contents : not working :: Need to try again :: 11-Feb-2019
				$url = "https://api.coinzest.co.kr/api/public/current_price/KRW-BTC";
				echo '<br><br>'.$url.'<br><br>';
				echo 'file Get content Not Working<br><br><br>';
				echo file_get_contents($url);
				exit;
				
				/**/
				$url	= "https://api.coinzest.co.kr/api/public/market_summary";
				$url = "https://api.coinzest.co.kr/api/public/current_price/KRW-ETH";
				$url	= "http://api.upticks.io/coinzest_market_summary.json";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data->result as $key=>$val){
					//prd($val);
					$this->updatecoindata($exchange_id,$val->MarketName,$val->Last,$val->Volume,$url);				
				}
				break;				
			case 48523:
				$url 		= "https://bgogo.com/api/snapshot/ETH/BTC";
				$data 		= file_get_contents($url);
				$data		= json_decode($data);
				$tickers	= array();
				//prd($data);
				foreach($data->all_symbols as $key=>$val){
					$tickers[$key]['symbol']	= $val;
				}
				foreach($data->last_prices as $key=>$val){
					$tickers[$key]['price']	= $val;
				}
				foreach($data->past_24hrs_volumes as $key=>$val){
					$tickers[$key]['past_24hrs_volume']	= $val;
				}
				foreach($tickers as $val){
					$this->updatecoindata($exchange_id,$val['symbol'],$val['price'],$val['price']*$val['past_24hrs_volume'],$url);
				}
				break;		
			case 48524:	
				$url 		= "https://xena.exchange/api/market-data/market-watch";
				$data 		= file_get_contents($url);
				$data		= json_decode($data);
				foreach($data as $key=>$val){
					if($val->symbol == 'XBTUSD'){
						continue;
					}
					$this->updatecoindata($exchange_id,$val->symbol,$val->lastPx,$val->lastPx*($val->buyVolume+$val->sellVolume),$url);
				}
				break;				
			case 48526;
				$url	= "https://coincheck.com/api/ticker";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				$market_name	= 'BTC/JPY';
				//prd($data);
				$this->updatecoindata($exchange_id,$market_name,$data->last,$data->last*$data->volume,$url);
				break;
			case 48527:
				$url 	= "http://coinsbit.io/api/v1/public/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				foreach($data->result as $key=>$val){
					$this->updatecoindata($exchange_id,$key,$val->ticker->last,$val->ticker->last*$val->ticker->vol,$url);
				}
				break;	
			case 48528:
				/*$sql = "select * from `exchanges_market` where exchange_id = 48528 order by market_name desc limit 200";
				$res = DB::select($sql);
				foreach($res as $val){
					if(strlen($val->market_name) == 11 &&   substr($val->market_name,7) == 'USDT'){
						$sql = "UPDATE `exchanges_market` SET symbol1 = '".substr($val->market_name,0,7)."', symbol2 = 'USDT' where exchange_id = 48528 and market_name = '".$val->market_name."'";
						DB::update($sql);
					}
				}
				exit;*/
			
				$url 	= "https://www.55.com/api/quote-realtime/quote/realTime.last";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->symbol,$val->last,$val->last*$val->volume,$url);					
				}
				break;	

			case 48529:
				$url = "https://extstock.com//api/v1/tickers.json";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$key,$val->ticker->last,$val->ticker->last*$val->ticker->vol,$url);					
				}
				//prd($arr);
				break;
			case 48530:
					$i = 0;
					while($i<1){
						$i++;
						$url = "https://api.bbx.com/v1/ifcontract/tickers?contractID=".$i;
						echo '>> '.$url.'<br><br>';
						$data 	= file_get_contents($url);
						$data	= json_decode($data);
						pr($data);/**/
					}
					exit;
					
			case 48531:
				$url 	= "https://partner.gdac.com/v0.4/public/tickers";
				$url 	= "http://api.upticks.io/gdac.json";
				$url 	= "https://marketapi.gdac.co.kr/v1/public/marketsummaries";			
			
			
// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
 
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); 



 
$data = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);			
//echo '+++++****++++++++++';exit;		
			
			
			

				$data	= json_decode($data);
				
				
				//prd($data);
				foreach($data->result as $key=>$val){
					$this->updatecoindata($exchange_id,$val->marketName,$val->last,$val->last*$val->volume,$url);					
				}
				break;					
					
			case 48532:
					$updatearr	= array();
					$url 		= "https://a.yunex.io/api/market/trade/info?symbol=";//"btc_usdt";
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						$apiurl	= $url.$val->market_name;
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						//prd($data);
						$this->updatecoindata($exchange_id,$val->market_name,$data->data->cur_price,$data->data->cur_price*$data->data->volume,$apiurl);						
					}
					break;
			
			case 48534:
				$url = "https://api.chaince.com/tickers";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$val->code,$val->price,$val->volume,$url);
				}
				//prd($arr);
				break;
			case 48535:
				$updatearr	= array();
				$url 		= "https://api.cryptology.com/v1/public/get-24hrs-stat?trade_pair=";//"XLM_BTC";
				$res 		= $this->exchangeMarket($exchange_id,1);
				foreach($res as $val)
				{
					$apiurl	= $url.$val->market_name;
					$data	= file_get_contents($apiurl);
					$data	= json_decode($data);
					//prd($data);
					$this->updatecoindata($exchange_id,$val->market_name,$data->data->open,$data->data->open*$data->data->base_volume,$apiurl);						
				}
				
				break;				
				
			
			case 48536:
				$url = "https://bitibu.com//api/v2/tickers.json";
				$data 	= file_get_contents($url);
				$data	= json_decode($data);
				//prd($data);
				foreach($data as $key=>$val){
					$this->updatecoindata($exchange_id,$key,$val->ticker->last,$val->ticker->last*$val->ticker->vol,$url);				
				}
				//prd($arr);	
				break;	

			case 48537:
					$updatearr	= array();
					$url = 'https://api.bitker.com/market/history/kline?period=1day&size=4&apikey=e4d6fe30-8319-4c1f-8945-d94644f00a2a&symbol=';//btc_usdt';
					$res 		= $this->exchangeMarket($exchange_id,1);
					foreach($res as $val)
					{
						echo $apiurl	= $url.strtolower($val->market_name);
						$data	= file_get_contents($apiurl);
						$data	= json_decode($data);
						pr($data);
						$data	= $data->tick;
						$this->updatecoindata($exchange_id,$val->market_name,$data[0]->close,$data[0]->close*$data[0]->amount,$apiurl);						
					}
					break;					
			default:break;
		}
		
		
		$sql = "UPDATE exchanges SET time = ".$t.", update_status = 1 where id = ".$exchange_id;
		DB::update($sql);		
		echo $exchange_id.'.....';
		/*if(!empty($sourceurl)){
			echo $sourceurl;
		}
		
		if(!empty($availdata)){
			echo '<pre>';print_r($availdata);echo '</pre>';
		}*/
		
		$convertcurrency = 'http://api.upticks.io/exchange/currencyconvert/btc?exchange_id='.$exchange_id;
		//$convertcurrency = 'exchange/currencyconvert/btc?exchange_id='.$exchange_id;
		//include($convertcurrency);
		echo '++HERE++';
	}

	
	function exchangeexport($exchange_id = 0)
	{
		$i = -1;
		$arr = array();
		$res = DB::table('exchanges_market')->where('exchange_id','=',$exchange_id)->orderBy('lastupdate', 'ASC')->take(10000)->get();
		foreach($res as $val){
			$i++;
			$arr[$i]['id'] 			= $val->id;
			$arr[$i]['price']		= $val->price;
			$arr[$i]['volume']		= $val->volume;
			$arr[$i]['bigvolume'] 	= $val->bigvolume;
		}
		//prd($arr);
		echo json_encode($arr);
	}
	
	function exchange_export($exchange_id = 0)
	{
		$i = -1;
		$arr	= array();
		$idarr 	= array();
		$res = DB::table('exchanges_market')->where('exchange_id','=',$exchange_id)->orderBy('lastupdate', 'DESC')->take(500)->get();
		foreach($res as $val){
			$i++;
			$arr[$i]['id'] 			= $val->id;
			$arr[$i]['price']		= $val->price;
			$arr[$i]['volume']		= $val->volume;
			$arr[$i]['bigvolume'] 	= $val->bigvolume;
			$idarr[] = $val->id;
		}
		
		$res = DB::table('exchanges_market')->orderBy('lastupdate', 'DESC')->take(500)->get();
		foreach($res as $val){
			if($i>500){
				break;
			}
			if(in_array($val->id,$idarr)){
				continue;
			}
			$idarr[] = $val->id;
			$i++;
			$arr[$i]['id'] 			= $val->id;
			$arr[$i]['price']		= $val->price;
			$arr[$i]['volume']		= $val->volume;
			$arr[$i]['bigvolume'] 	= $val->bigvolume;
		}
		
		
		//prd($arr);
		echo json_encode($arr);
	}
	
	
	
	function exchangeimport($exchange_id = 0)
	{
		//exit;
		if(in_array($exchange_id,array(211,22,24,1,5))){
			$url = "http://api02.upticks.io/exchange_export/".$exchange_id;
			//exit;
		}
		elseif(in_array($exchange_id,array(477,454,60,11,7,48519))){
			$url = "http://api01.upticks.io/exchange_export/".$exchange_id;
			exit;
		}
		elseif(in_array($exchange_id,array(35,21,19,36,30,79,56,55,89,42,93,347,67,664,135,72))){
			$url = "https://api05.upticks.io/exchange_export/".$exchange_id;
			//exit;
		}
		elseif(in_array($exchange_id,array(26,86,48524,48,625,25,616,48510,32,6,38,20,48523,48520,33,131,49,27,10,51,39,48526,187,53,48522,115,62,97,152,48527,61,46,40,14580,99,64,139,103,716,123,127,147))){
			$url = "http://api05.upticks.io/exchange_export/".$exchange_id;
			exit;
		}
		
		echo $url.' ++++++ <br><br>';
		$str = file_get_contents($url);
		$arr = json_decode($str);
		$t	 = date('Y-m-d H:i:s');
		$updatedata['lastupdate']	= date('Y-m-d H:i:s');
		foreach($arr as $val){
			$wherecond['id'] 			= $val->id;
			$updatedata['price'] 		= $val->price;
			$updatedata['volume'] 		= $val->volume;
			$updatedata['bigvolume'] 	= $val->bigvolume;
			//pr($wherecond);
			//pr($updatedata);
			//echo '<br>------->>>>>---<br>';
			DB::table('exchanges_market')->where($wherecond)->update($updatedata);	
		}
		
		//$url = "http://api.upticks.io/exchange/currencyconvert/btc?exchange_id=".$exchange_id;;
		//file_get_contents($url);
	}
}